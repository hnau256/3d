#!/usr/bin/env python3
# coding: utf-8

from screw import *
from involute import *
from screw import *
from math import *
from grip import *
from mechanism import *

wall = 3.2
tech = 0.001
toy_friction = 0.8
tech_friction = 0.4
toy_screw_depth = 3.6
toy_screw_angle = pi / 1.2
tech_screw_depth = 1.2
trunk_screw_radius = wall * 4
line_thickness = wall * 3
tech_screw_length = wall * 2 + line_thickness
trunk_length = 120
line_chamfer = line_thickness / 7
gear_cutout_min_length = wall * 12

trunk_radius = trunk_screw_radius + toy_screw_depth

small_gear_screw_enters_count = 3
big_gear_tooth_count_factor = 2

involute_style = InvoluteStyle()

small_gear_context = GearContext.find_min(
    style=involute_style,
    predicate=lambda context: context.root_radius > trunk_radius + wall
)

big_gear_context = GearContext(
    style=involute_style,
    tooth_count=small_gear_context.tooth_count * big_gear_tooth_count_factor
)

trunks_distance = (small_gear_context.pitch_radius + toy_friction + big_gear_context.pitch_radius) / 2

small_gear_screw_style = ScrewStyle(
    depth=toy_screw_depth,
    enters=small_gear_screw_enters_count,
    angle=toy_screw_angle,
    deepening=tech
)

tech_screw_style = ScrewStyle(
    depth=tech_screw_depth,
    deepening=tech
)

big_gear_screw_style = ScrewStyle(
    depth=toy_screw_depth,
    enters=small_gear_screw_enters_count * big_gear_tooth_count_factor,
    angle=toy_screw_angle,
    deepening=tech
)


def build_gear(
        context: GearContext,
        screw_style: ScrewStyle
):
    result = context.create_profile().fill().extrude(line_thickness)
    screw_radius = trunk_screw_radius + toy_friction
    result -= screw_style.create_shape(screw_radius, line_thickness)

    def apply_sbtn():
        nonlocal result
        sbtn_wall = wall + line_chamfer
        sbtn_min_radius = screw_radius + toy_screw_depth + sbtn_wall
        sbtn_max_radius = context.root_radius - sbtn_wall
        sbtn_radius_delta = sbtn_max_radius - sbtn_min_radius
        if sbtn_radius_delta < sbtn_wall:
            return

        min_radius_circle_length = sbtn_min_radius * 2 * pi
        cutsout_count = int(floor(min_radius_circle_length / (gear_cutout_min_length + wall)))
        cutout_angle = pi * 2 / cutsout_count
        cutout_amplitude_angle = (cutout_angle - (sbtn_wall / min_radius_circle_length) * pi * 2) / 2
        sbtn = cylinder(sbtn_max_radius, line_thickness)
        sbtn -= cylinder(sbtn_min_radius, line_thickness)
        sbtn ^= halfspace().rotateX(-pi / 2).rotateZ(cutout_amplitude_angle)
        sbtn ^= halfspace().rotateX(pi / 2).rotateZ(-cutout_amplitude_angle)
        fillet_length = (min_radius_circle_length / cutsout_count - sbtn_wall - tech) / 2
        fillet_length = min(fillet_length, (sbtn_radius_delta - tech) / 2)
        sbtn = sbtn.fillet(fillet_length, [
            point3(sbtn_max_radius, 0, line_thickness / 2).rotateZ(cutout_amplitude_angle),
            point3(sbtn_max_radius, 0, line_thickness / 2).rotateZ(-cutout_amplitude_angle),
            point3(sbtn_min_radius, 0, line_thickness / 2).rotateZ(cutout_amplitude_angle),
            point3(sbtn_min_radius, 0, line_thickness / 2).rotateZ(-cutout_amplitude_angle)
        ])
        sbtn = union([sbtn.rotateZ(cutout_angle * i) for i in range(cutsout_count)])
        result -= sbtn
        result = chamfer(
            result,
            line_chamfer,
            [point3(sbtn_min_radius, 0, 0).rotateZ(cutout_angle * i) for i in range(cutsout_count)]
        )
        result = chamfer(
            result,
            line_chamfer,
            [point3(sbtn_min_radius, 0, line_thickness).rotateZ(cutout_angle * i) for i in range(cutsout_count)]
        )

    apply_sbtn()

    max_radius = context.outside_radius
    chamfer_radius = max_radius - line_chamfer
    istn = cone(chamfer_radius, chamfer_radius + line_thickness / 2, line_thickness / 2)
    istn += istn.mirrorXY().up(line_thickness)
    result ^= istn
    return result


def build_small_gear():
    result = build_gear(small_gear_context, small_gear_screw_style)
    result = result.rotateZ(pi + small_gear_context.tooth_angle / 2)
    result = result.right(trunks_distance)
    return result


small_gear = build_small_gear()


def build_big_gear():
    result = build_gear(big_gear_context, big_gear_screw_style)
    result = result.down(line_thickness).mirrorXY()
    result = result.left(trunks_distance)
    return result


big_gear = build_big_gear()

tech_screw_radius = trunk_screw_radius - wall - tech_friction - tech_screw_depth - tech_friction
tech_screp_hole_radius = tech_screw_radius + tech_screw_depth
tech_screp_cap_radius = tech_screp_hole_radius + wall * sqrt(2)


def build_trunk():
    result = small_gear_screw_style.create_shape(trunk_screw_radius, trunk_length)
    screw_istn = big_gear_screw_style.create_shape(trunk_screw_radius, trunk_length)
    screw_istn = screw_istn.down(trunk_length)
    screw_istn = screw_istn.mirrorXY()
    result ^= screw_istn

    result = result.right(trunks_distance)
    result = result.up(line_thickness)

    base_radius = trunk_radius + line_chamfer

    def base():
        mount_point_radius = trunk_radius + line_chamfer
        radius_max = trunks_distance + mount_point_radius
        radius_min = radius_max - line_thickness
        result = cylinder(radius_max, line_thickness)
        result -= cylinder(radius_min, line_thickness)
        mount_point = cylinder(mount_point_radius, line_thickness)
        result += mount_point.right(trunks_distance)
        result += mount_point.left(trunks_distance)

        def apply_fillet():

            nonlocal result

            def calc_fillet_point(radius: float):
                x = (mount_point_radius ** 2 - radius ** 2 - trunks_distance ** 2) / (2 * trunks_distance)
                y = sqrt(radius ** 2 - x ** 2)
                return point3(x, y, line_thickness / 2)

            max_fillet_point = calc_fillet_point(radius_max)
            min_fillet_point = calc_fillet_point(radius_min)

            fillet_length = mount_point_radius

            result = result.fillet(
                fillet_length,
                [
                    #max_fillet_point,
                    min_fillet_point,
                    #max_fillet_point.mirrorXZ(),
                    min_fillet_point.mirrorXZ(),
                    #max_fillet_point.mirrorYZ(),
                    min_fillet_point.mirrorYZ(),
                    #max_fillet_point.rotateZ(pi),
                    min_fillet_point.rotateZ(pi)
                ]
            )

        apply_fillet()

        result = chamfer(
            result,
            line_chamfer,
            [
                (0, radius_min, 0),
                (0, radius_min, line_thickness),
                #(0, radius_max, 0),
                #(0, radius_max, line_thickness)
            ]
        )
        out_chamfer_radius = radius_max-line_chamfer
        out_chamfer_height = line_thickness / 2
        out_chamfer_istn = cone(out_chamfer_radius, out_chamfer_radius+out_chamfer_height, out_chamfer_height)
        out_chamfer_istn += out_chamfer_istn.mirrorXY().up(line_thickness)
        result ^= out_chamfer_istn

        return result

    result += base()

    def adapter(compression: float = 0, line: bool = False):
        height = wall - compression
        radius = trunk_screw_radius - wall + height - compression * (1 + sqrt(2))
        result = cone(radius, radius - height, height)
        if line:
            line_length = base_radius + wall
            line = box(line_length, radius * 2, height)
            line = line.back(radius)
            line -= halfspace().rotateX(3 * pi / 4).forw(radius)
            line -= halfspace().rotateX(-3 * pi / 4).back(radius)
            result += line
        return result

    add_adapter = adapter(compression=tech_friction)
    add_adapter = add_adapter.mirrorXY()
    add_adapter = add_adapter.right(trunks_distance)
    add_adapter = add_adapter.up(trunk_length + line_thickness + wall - tech_friction)
    result += add_adapter

    sbtn_adapter = adapter(line=True)
    sbtn_adapter = sbtn_adapter.rotateZ(pi)
    sbtn_adapter = sbtn_adapter.left(trunks_distance)
    sbtn_adapter = sbtn_adapter.up(line_thickness - wall)
    result -= sbtn_adapter

    def tech_screw_sbtn():
        length = tech_screw_length - line_thickness + wall + tech_friction
        result = tech_screw_style.create_shape(tech_screw_radius, length)
        result = result.right(trunks_distance)
        result = result.up(trunk_length + line_thickness + wall - length)
        hole = cylinder(tech_screp_hole_radius + tech_friction, line_thickness)
        max_radius = tech_screp_cap_radius + tech_friction * sqrt(2)
        hole += cone(max_radius, 0, max_radius)
        hole = hole.left(trunks_distance)
        result += hole
        return result

    result -= tech_screw_sbtn()

    result = result.down(trunk_length / 2 + line_thickness)

    return result


trunk = build_trunk()


def build_trunk_screw():
    length = tech_screw_length - tech_friction
    result = tech_screw_style.create_shape(tech_screw_radius - tech_friction, length - tech)
    result = result.up(tech)
    result += cone(tech_screp_cap_radius, 0, tech_screp_cap_radius)
    sbtn_radius = tech_screp_cap_radius - wall * sqrt(2)
    sbtn = cone(sbtn_radius, 0, sbtn_radius)
    sbtn ^= box(sbtn_radius * 2, wall, sbtn_radius).translate(-sbtn_radius, -wall / 2)
    result -= sbtn
    result = result.rotateY(pi)
    result = result.right(trunks_distance)
    result = result.up(trunk_length / 2 + line_thickness)
    return result


trunk_screw = build_trunk_screw()

mechanism(
    export_to_stl=True,
    use_mask=False,
    # mask=halfspace().rotateY(pi/2).right(trunks_distance),
    objects=[
        MechanismObject(
            model=small_gear,
            color=color(1, 0, 1),
            to_stl_name="small_gear"
        ),
        MechanismObject(
            model=big_gear,
            color=color(0, 1, 1),
            to_stl_name="big_gear"
        ),
        MechanismObject(
            model=trunk,
            color=color(1, 1, 0),
            to_stl_name="trunk"
        ),
        MechanismObject(
            model=trunk_screw,
            color=color(1, 1, 1),
            to_stl_name="trunk_screw"
        ),
        MechanismObject(
            model=small_gear.down(line_thickness).rotateZ(pi),
            color=color(1, 0, 0)
        ),
        MechanismObject(
            model=big_gear.down(line_thickness).rotateZ(pi),
            color=color(0, 1, 0)
        ),
        MechanismObject(
            model=trunk.rotateY(pi),
            color=color(0, 0, 1)
        ),
        MechanismObject(
            model=trunk_screw.rotateY(pi),
            color=color(1, 1, 1)
        )
    ]
)
