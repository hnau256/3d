#!/usr/bin/env python3
# coding: utf-8

from involute import *
from screw import *
from mechanism import *

wall = 2.4
friction = 0.2
screw_radius = 3.6
screw_length = 12
gear_tooth_count = 10
gear_height = 7.2

screw_style = ScrewStyle()
screw = screw_style.create_shape(screw_radius, screw_length)

gear = InvoluteStyle()\
    .create_context(gear_tooth_count)\
    .create_profile()\
    .fill()\
    .extrude(gear_height)

gear -= screw_style.create_shape(
    radius=screw_radius+friction,
    height=screw_length
)

mechanism(
    objects=[
        MechanismObject(
            model=screw,
            color=color(1, 0, 1),
            to_stl_name="screw"
        ),
        MechanismObject(
            model=gear,
            color=color(0, 1, 1),
            to_stl_name="gear"
        )
    ]
)