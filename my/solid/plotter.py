from math import *
from solid import *
from utils import export

clearence = 0.4


def create_shape():
    return circle(1)


if __name__ == '__main__':
    shape = create_shape()
    export.export_scad(shape, __file__)
