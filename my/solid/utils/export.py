import os
from solid import *


def export_scad(
        scad_object,
        script_file
):
    script_path = os.path.realpath(script_file)
    scad_path = os.path.splitext(script_path)[0] + ".scad"
    print(f"Exporting shape to file {scad_path}")
    scad_render_to_file(
        scad_object,
        scad_path,
        include_orig_code=False,
        file_header='$fs=$preview?10:0.1; $fa=$preview?10:0.1;'
    )
