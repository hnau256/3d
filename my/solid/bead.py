from math import *
from solid import *
from utils import export

clearence = 0.4
radius = 10
height = radius * 1.5
hole_radius = 2.5
fillet = radius * 0.25

Profile = Sequence[float]


def create_linear_extrude_bead(
        projection: OpenSCADObject,
        profile: Profile
) -> OpenSCADObject:
    if len(profile) == 0:
        raise "Profile is empty"

    if len(profile) == 1:
        radius_factor = profile[0]
        return create_linear_extrude_bead(projection, [radius_factor, radius_factor])

    profile_steps = len(profile) - 1

    base_radius = radius - fillet
    base_height = height - fillet * 2
    y_step = base_height / profile_steps

    result = None
    for i in range(profile_steps):
        from_factor = profile[i]
        to_factor = profile[i + 1]
        from_scale = from_factor * base_radius
        from_projection = scale((from_scale, from_scale, 1))(projection)
        level = linear_extrude(
            height=y_step,
            scale=to_factor / from_factor
        )(from_projection)
        level = translate((0, 0, i * y_step))(level)
        result = level if not result else result + level

    result = minkowski()(result, sphere(fillet))
    result = translate((0, 0, -base_height/2))(result)
    return result


def create_linear_extrude_beads() -> Sequence[OpenSCADObject]:
    def create_polygon(angles: int):
        step_angle = pi * 2 / angles
        return polygon([(cos(i * step_angle), sin(i * step_angle)) for i in range(angles)])

    projections = [
        create_polygon(3),
        create_polygon(4),
        create_polygon(6),
        create_polygon(8),
        circle(0.9)
    ]
    profilies = [
        [1],
        [1, 0.6],
        [0.6, 1, 0.6],
        [1, 0.75, 0.75, 0.75, 1]
    ]
    result = []
    for projection in projections:
        for profile in profilies:
            result.append(create_linear_extrude_bead(projection, profile))

    return result


def create_shape() -> OpenSCADObject:
    beads = create_linear_extrude_beads()

    beads_count = len(beads)
    x_count = ceil(sqrt(beads_count))
    y_count = ceil(beads_count / x_count)

    result = None
    cell_size = radius * 2 + clearence
    hole_sbtn = cylinder(
        r=hole_radius,
        h=radius * 10,
        center=True
    )
    for i in range(beads_count):
        (y, x) = divmod(i, x_count)
        bead = beads[i]
        bead -= hole_sbtn
        bead = translate((x * cell_size, y * cell_size, 0))(bead)
        result = bead if not result else result + bead

    result = translate((-(x_count - 1) * cell_size / 2, -(y_count - 1) * cell_size / 2, 0))(result)
    return result


if __name__ == '__main__':
    shape = create_shape()
    export.export_scad(shape, __file__)
