from math import *
from solid import *
from utils import export

tech = 0.01
clearence = 0.4
wall = 1.2

teebag_width = 70
teebag_length = 80
teebags_height = 32
teebag_gap_height = 6
view_gap_width = 5
view_gap_vertical_margin = 3.6

finger_width = 20
finger_length = 30

front_gap_fillet = 20


def create_shape():
    total_width = teebag_width + (clearence + wall) * 2
    total_length = teebag_length + (clearence + wall) * 2
    total_height = teebags_height + clearence + wall

    result = cube((total_width, total_length, total_height))
    result = translate((-total_width / 2, 0, 0))(result)

    teebags_sbtn = cube((total_width - wall * 2, total_length - wall * 2, total_height - wall + tech))
    teebags_sbtn = translate((-total_width / 2 + wall, wall, wall))(teebags_sbtn)
    result -= teebags_sbtn

    bottom_finger_sbtn = square((finger_width, finger_length - finger_width / 2 + tech))
    bottom_finger_sbtn = translate((-finger_width / 2, -tech, 0))(bottom_finger_sbtn)
    bottom_finger_sbtn_circle = circle(finger_width / 2)
    bottom_finger_sbtn_circle = translate((0, finger_length - finger_width / 2, 0))(bottom_finger_sbtn_circle)
    bottom_finger_sbtn += bottom_finger_sbtn_circle
    bottom_finger_sbtn = linear_extrude(wall + tech * 2)(bottom_finger_sbtn)
    bottom_finger_sbtn = translate((0, 0, -tech))(bottom_finger_sbtn)
    result -= bottom_finger_sbtn

    def front_sbtn():
        view_gap_min_y = wall + teebag_gap_height
        tee_gap_width = total_width - wall * 2
        view_gap_dx = view_gap_width / 2
        result = square((tee_gap_width, teebag_gap_height))
        result = translate((-tee_gap_width / 2, wall, 0))(result)
        fillet_square_width = view_gap_width + front_gap_fillet * 2
        fillet_square = square((fillet_square_width, front_gap_fillet))
        fillet_square = translate((-fillet_square_width / 2, view_gap_min_y, 0))(fillet_square)
        result += fillet_square
        view_saquare_min_y = view_gap_min_y + front_gap_fillet
        view_square_height = total_height - view_gap_width / 2 - view_gap_vertical_margin - view_saquare_min_y
        view_square = square((view_gap_width, view_square_height))
        view_square = translate((-view_gap_width / 2, view_saquare_min_y, 0))(view_square)
        view_square += translate((0, view_saquare_min_y + view_square_height, 0))(circle(view_gap_width / 2))
        result += view_square
        fillet_cirlce = circle(front_gap_fillet)
        result -= translate((fillet_square_width / 2, view_saquare_min_y, 0))(fillet_cirlce)
        result -= translate((-fillet_square_width / 2, view_saquare_min_y, 0))(fillet_cirlce)
        result = linear_extrude(wall + tech * 2)(result)
        result = rotate((0, 0, 180))(result)
        result = rotate((-90, 0, 0))(result)
        result = translate((0, -tech, 0))(result)
        return result

    result -= front_sbtn()

    teebag_gap_sbtn = cube((total_width - wall * 2, wall + tech * 2, teebag_gap_height))
    teebag_gap_sbtn = translate((-total_width / 2 + wall, -tech, wall))(teebag_gap_sbtn)
    result -= teebag_gap_sbtn

    return result


if __name__ == '__main__':
    shape = create_shape()
    export.export_scad(shape, __file__)
