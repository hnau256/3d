#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from mechanism import *
from screw import *

tech = 0.001
friction = 0.5
grip_sbtn_radius = 1.6
grip_sbtns_distance = grip_sbtn_radius * 4
toy_friction = 1.0
toy_radius = 18
toy_height = toy_radius * 1.4
toys_z_count = 2
toys_separation = toy_radius * 2.2
wall = 1.6
screw_length = wall * 6
screw_cleaning_length = screw_length / 4

screw_style = ScrewStyle(
    depth=2.4,
    enters=8,
    angle=pi / 1.5
)


def shape_circle():
    result = circle(0.8)
    return result


def shape_square():
    radius = 0.7
    result = rectangle(radius * 2, center=True)
    result = result.fillet2d(
        radius * 0.2,
        [
            (-radius, -radius),
            (-radius, radius),
            (radius, -radius),
            (radius, radius)
        ]
    )
    return result


def shape_polygon(r=1, n=3, f=0.1):
    point = point3(0, -r, 0)
    point_angle = pi * 2 / n
    points = [point.rotateZ(point_angle * i) for i in range(n)]
    result = polygon(points)
    result = result.fillet2d(f, points)
    return result


def shape_star(ro=1, ri=0.6, n=5, f=0.1):
    ray_angle = pi * 2 / n
    point_o = point3(0, -ro, 0)
    point_i = point3(0, -ri, 0).rotateZ(ray_angle / 2)
    points = []
    for i in range(n):
        points.append(point_o.rotateZ(ray_angle * i))
        points.append(point_i.rotateZ(ray_angle * i))
    result = polygon(points)
    result = result.fillet2d(f, points)
    return result


shapes_projections = [
    shape_circle(),
    shape_polygon(),
    shape_square(),
    shape_star(),
]

side_shapes_count = int(sqrt(len(shapes_projections)))


def get_shape_projection_index(x, y):
    return x + y * side_shapes_count


def get_shape_projection(x, y):
    return shapes_projections[get_shape_projection_index(x, y)]


def get_shape_offset(i):
    return (i - float(side_shapes_count - 1) / 2) * toys_separation


def configure_shapes_projections():
    for ix in range(side_shapes_count):
        for iy in range(side_shapes_count):
            i = get_shape_projection_index(ix, iy)
            shape = shapes_projections[i]
            shape = shape.scale(toy_radius)
            shape = shape.translate(
                get_shape_offset(ix),
                get_shape_offset(iy)
            )
            shapes_projections[i] = shape


configure_shapes_projections()

content_radius = toys_separation * sqrt(2) * float(side_shapes_count - 1) / 2 + toys_separation / 2
content_height = toys_z_count * (toy_height + toy_friction) + toy_friction
cup_radius = content_radius + wall
cup_height = content_height + wall + wall


def create_cup():
    result = cylinder(cup_radius, cup_height)
    result = result.fillet(wall - tech, [(0, cup_radius, cup_height)])

    def create_screw():
        screw_height = screw_length - screw_cleaning_length
        result = screw_style.create_shape(cup_radius, screw_height)
        istn_height = cup_radius + screw_height
        istn = cone(istn_height, 0, istn_height)
        istn ^= istn.mirrorXY().up(screw_height)
        result ^= istn
        result = result.up(cup_height - screw_length)
        return result

    result += create_screw()
    result -= cylinder(content_radius, cup_height - wall).up(wall)

    def create_toys_borders():
        result = []
        for ix in range(side_shapes_count):
            for iy in range(side_shapes_count):
                toy_border = get_shape_projection(ix, iy)
                toy_border = toy_border.extrude(wall + toy_friction)
                toy_border = offset(toy_border, toy_friction + wall)
                toy_border = toy_border.down(toy_friction + toy_friction)
                toy_border -= halfspace().up(wall - tech)
                result.append(toy_border)
        result = union(result)
        return result

    #result += create_toys_borders()

    def create_toys_sbtn():
        result = []
        for ix in range(side_shapes_count):
            for iy in range(side_shapes_count):
                toy_sbtn = get_shape_projection(ix, iy)
                toy_sbtn = toy_sbtn.extrude(tech + wall + tech + wall + tech)
                toy_sbtn = offset(toy_sbtn, toy_friction)
                toy_sbtn = toy_sbtn.down(tech)
                result.append(toy_sbtn)
        result = union(result)
        return result

    result -= create_toys_sbtn()

    def create_separators():
        radius = content_radius + tech + tech
        result = []
        for i in range(side_shapes_count - 1):
            offset = get_shape_offset(i) + toys_separation / 2
            separator = box(wall, radius * 2, content_height)
            separator = separator.translate(-wall / 2, -radius)
            separator = separator.right(offset)
            result.append(separator)
            result.append(separator.rotateZ(pi / 2))
        result = union(result)
        result ^= cylinder(content_radius + tech, content_height)
        result = result.up(wall)
        return result

    result += create_separators()

    return result


def create_grip(radius, height):
    radius += grip_sbtn_radius
    result = cylinder(radius, height)
    result = result.chamfer(
        grip_sbtn_radius,
        [(0, radius, 0), (0, radius, height)]
    )
    fillet_radius = grip_sbtn_radius * sqrt(2) * 2
    fillet_radius = min(fillet_radius, height / 2 - grip_sbtn_radius - tech)
    result = result.fillet(
        fillet_radius,
        [(0, radius, grip_sbtn_radius), (0, radius, height - grip_sbtn_radius)]
    )
    sbtns_count = int(radius * 2 * pi / grip_sbtns_distance)
    sbtn_angle = pi * 2 / sbtns_count
    sbtn = cylinder(grip_sbtn_radius, height).right(radius)
    sbtns = union([sbtn.rotateZ(sbtn_angle * i) for i in range(sbtns_count)])
    result -= sbtns
    return result


def create_cap():
    height = screw_length + friction + wall
    sbtn_radius = cup_radius + screw_style.depth + friction * sqrt(2)
    result = create_grip(sbtn_radius + wall, height)
    sbtn = cylinder(sbtn_radius, screw_cleaning_length)
    sbtn += cone(sbtn_radius, sbtn_radius - screw_style.depth, screw_style.depth).up(screw_cleaning_length)
    result -= sbtn
    result = result.fillet(wall - tech, [(0, sbtn_radius, 0)])
    screw_sbtn_height = screw_length + friction - screw_cleaning_length
    screw_sbtn = screw_style.create_shape(cup_radius + friction * sqrt(2), screw_sbtn_height)
    screw_sbtn = screw_sbtn.up(screw_cleaning_length)
    result -= screw_sbtn
    result = result.up(cup_height - screw_length)
    return result


def create_shapes():
    result = []
    for ix in range(side_shapes_count):
        for iy in range(side_shapes_count):
            shape = get_shape_projection(ix, iy)
            shape = shape.extrude(toy_height)
            result.append(shape)
    return result


objects = [
    MechanismObject(
        model=create_cup().up(50),
        to_stl_name="cup",
        color=Color(1, 0, 1)
    ),
    MechanismObject(
        model=create_cap().up(90),
        to_stl_name="cap",
        color=Color(0, 1, 1)
    )
]

shapes = create_shapes()
for i in range(len(shapes)):
    objects.append(
        MechanismObject(
            model=shapes[i],
            to_stl_name="shape_" + str(i + 1),
            color=Color(1, 1, 0)
        )
    )

mechanism(
    use_mask=True,
    mask=halfspace().rotateX(pi / 2).rotateZ(pi / 4),
    objects=objects
)
