#!/usr/bin/env python3
#coding: utf-8

from zencad import *
from mechanism import *
from screw import *
from screw_bolt import *

tech = 0.0001
wall = 6.4
friction = 0.2

max_offset = 60

jaw_depth = 5
jaw_height = 20
jaw_width = 40
jaw_chamfer = jaw_height * 0.2

jaw_length = jaw_depth + jaw_chamfer
diagonal_friction = friction * sqrt(2)

static_screw = ScrewBolt(
    screw_style=ScrewStyle(1.2),
    wall=wall,
    screw_length=wall*2,
    screw_radius=wall,
    friction=friction
)

dynamic_screw_style = ScrewStyle(depth=2.0)


def create_jaw():
    result = box(jaw_width, jaw_length, jaw_height)
    result = result.left(jaw_width/2)
    depth_sbtn = circle_arc(
        (-jaw_width/2, jaw_length),
        (0, jaw_length-jaw_depth),
        (jaw_width/2, jaw_length)
    )
    depth_sbtn = sew([depth_sbtn])
    depth_sbtn = depth_sbtn.fill()
    depth_sbtn = depth_sbtn.extrude(jaw_height)
    result -= depth_sbtn
    result = result.chamfer(
        jaw_chamfer-tech,
        [
            (0, jaw_length-jaw_depth, 0),
            (0, jaw_length-jaw_depth, jaw_height)
        ]
    )
    return result

jaw = create_jaw()

disp(jaw)

show()