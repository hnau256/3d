#!/usr/bin/env python3
# coding: utf-8

from screw import *
from mechanism import *

friction = 0.45
tech = 0.01
wall = 2.4
nut_depth = 1.2
nut_height = 4.8
chamfer_size = wall / 2

radius = 20

nut_top_radius = sqrt(radius ** 2 - (nut_height + friction) ** 2)

screw_style = ScrewStyle(
    depth=nut_depth
)

def half():
    result = sphere(radius)
    result -= halfspace()

    sbtn_nut_height = nut_height + friction
    outer_nut_radius = nut_top_radius - wall - nut_depth
    sbtn = screw_style.create_shape(outer_nut_radius, sbtn_nut_height)
    inner_nut_radius = outer_nut_radius - friction - wall - friction
    sbtn -= cylinder(inner_nut_radius, sbtn_nut_height)

    vault_max_radius = outer_nut_radius + nut_depth
    vault_min_radius = inner_nut_radius
    vault_top_radius = (vault_max_radius + vault_min_radius) / 2
    vault_height = (vault_max_radius - vault_min_radius) / 2
    vault = cone(vault_max_radius, vault_top_radius, vault_height)
    vault -= cone(vault_min_radius, vault_top_radius, vault_height)
    vault = vault.up(sbtn_nut_height)
    sbtn += vault

    nut_chamfer = cone(vault_max_radius, vault_max_radius-nut_depth, nut_depth)
    nut_chamfer -= cylinder(inner_nut_radius+tech, nut_depth+tech)
    sbtn += nut_chamfer

    result -= sbtn

    result = result.chamfer(chamfer_size, [(0, -inner_nut_radius, 0)])

    return result


def connector():
    outer_nut_radius = nut_top_radius - wall - nut_depth - friction
    result = screw_style.create_shape(outer_nut_radius, nut_height*2)
    inner_nut_radius = outer_nut_radius - wall
    result -= cylinder(inner_nut_radius, nut_height*2)
    result = result.down(nut_height)
    result = result.chamfer(chamfer_size, [(0, -inner_nut_radius, -nut_height), (0, -inner_nut_radius, nut_height)])
    istn_height = outer_nut_radius + nut_height + tech
    istn = cone(istn_height, 0, istn_height)
    istn += istn.mirrorXY()
    result ^= istn
    return result


half = half()

mechanism(
    mask=halfspace().rotateX(pi / 2).rotateZ(pi/3),
    objects=[
        MechanismObject(
            model=half,
            to_stl_name="half",
            color=color(1, 1, 0)
        ),
        MechanismObject(
            model=half.rotateX(pi),
            color=color(0, 1, 1)
        ),
        MechanismObject(
            model=connector().rotateZ(pi/2),
            to_stl_name="connector",
            color=color(1, 0, 1)
        )
    ]
)
