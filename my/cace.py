#!/usr/bin/env python3
# coding: utf-8

#from mechanism import *

from zencad import *
pi = 3.1415

wall = 6.4
pattern_height = 7.2
cutter_height = 12
star_height = 16
level_wall = 12

pattern_radius = 190 / 2
cutter_radius = pattern_radius - 5

star_ray_count = 5
star_max_radius = cutter_radius * (4/5)
star_min_radius = star_max_radius * 0.55

def pattern():
	result = cylinder(pattern_radius+wall, pattern_height)
	result -= cylinder(pattern_radius, pattern_height)
	return result


def cutter():
	result = cone(cutter_radius+cutter_height, cutter_radius, cutter_height)
	result -= cylinder(cutter_radius, cutter_height)
	return result

def star():
	points = []
	ray_angle = pi*2/star_ray_count
	for ray in range(star_ray_count):
		points.append(point3(0, star_min_radius).rotateZ(ray*ray_angle))
		points.append(point3(0, star_max_radius).rotateZ((ray+0.5)*ray_angle))
	result_prototype = polygon(pnts=points)
	result_prototype = result_prototype.extrude(star_height)
	result = offset(result_prototype, wall)
	result -= halfspace()
	result -= halfspace().mirrorXY().up(star_height)
	result -= result_prototype
	return result

def level():
	level_radius = cutter_radius + cutter_height
	result = box(level_radius*2, level_wall, level_wall)
	result = result.translate(-level_radius, -level_wall/2)
	return result

disp(star())
show()

