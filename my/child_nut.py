#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from screw import *
from mechanism import *

wall = 6.4
tech = 0.01
screw_depth = 3.6
friction = 1.2

screw_length = wall * 10
screw_sleek_length = wall * 2
cap_radius = wall * 4
cap_handle_length = wall * 2.5

screw_style = ScrewStyle(
    depth=screw_depth,
    enters=4,
    angle=pi / 1.2
)


def cap():
    cap_handles_count = int(floor(cap_radius * 2 * pi / cap_handle_length / 2)) * 2
    cap_handle_angle = pi * 2 / cap_handles_count
    handler_radius = sin(cap_handle_angle / 2) * cap_radius
    handler = cylinder(handler_radius, wall)
    handler = handler.right(cap_radius)
    result = ngon(cap_radius, cap_handles_count)
    result = result.extrude(wall)
    for i in range(cap_handles_count):
        i_handler = handler.rotateZ(i * cap_handle_angle)
        if i % 2 == 0:
            result += i_handler
        else:
            result -= i_handler
    max_radius =cap_radius + handler_radius
    result = unify(result)
    chamfer_length = wall / 3
    result = chamfer(result, chamfer_length, [(max_radius, 0, 0), (max_radius, 0, wall)])
    result = fillet(result, (wall / 2 - chamfer_length)*2 - tech, [(max_radius, 0, chamfer_length), (max_radius, 0, wall-chamfer_length)])
    return result


cap = cap()


def screw():
    screw_main_height = screw_length - screw_sleek_length
    result = screw_style.create_shape(
        radius=wall,
        height=screw_main_height
    )
    istn = cone(wall + screw_depth, 0, wall + screw_depth)
    screw_istn_y = screw_main_height - screw_depth
    istn = istn.up(screw_istn_y)
    istn += cylinder(wall + screw_depth, screw_istn_y)
    result ^= istn
    screw_cap = sphere(wall)
    screw_cap = screw_cap.up(screw_sleek_length - wall)
    screw_cap += cylinder(wall, screw_sleek_length - wall)
    screw_cap = screw_cap.up(screw_main_height)
    result += screw_cap
    result = result.up(wall)
    result += cap
    return result


def nut():
    result = cap
    result -= screw_style.create_shape(wall + friction, wall)
    result = result.up(wall + (screw_length - wall) / 2)
    return result


mechanism(
    export_to_stl=True,
    use_mask=False,
    objects=[
        MechanismObject(
            model=screw(),
            color=color(1, 1, 0),
            to_stl_name="screw"
        ),
        MechanismObject(
            model=nut(),
            color=color(0, 1, 1),
            to_stl_name="nut"
        )
    ]
)
