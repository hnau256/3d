#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from mechanism import *

wall = 1.2
gap = 0.2
tech = 0.001

base_width = 30.7
base_length = 36.4
wemos_pins_width = 2.5
wemos_pins_length = 21
wemos_pins_y_offset = 7.25
wemos_pins_x_offset = 2.6
wemos_pins_height = 7.8
side_sign_radius = wall * 2

content_height = 25
net_rod_width = wall * 3
net_rods_separation = wall * 2


def holder():
    result = box(base_width, base_length, wall)
    result = result.left(base_width / 2)
    result += sphere(side_sign_radius + wall) - halfspace().up(wall) - halfspace().rotateX(-pi/2)
    pins_sbtn_width = wemos_pins_width + gap * 2
    pins_sbtn_length = wemos_pins_length + gap * 2
    pins_wall_width = pins_sbtn_width + wall * 2
    pins_wall_length = pins_sbtn_length + wall * 2
    pins_wall = box(pins_wall_width, pins_wall_length, wemos_pins_height - wall)
    pins_wall = pins_wall.fillet(
        wall,
        [
            (0, 0, wemos_pins_height / 2),
            (pins_wall_width, 0, wemos_pins_height / 2),
            (0, pins_wall_length, wemos_pins_height / 2),
            (pins_wall_width, pins_wall_length, wemos_pins_height / 2)
        ]
    )

    pins_wall = pins_wall.up(wall)
    pins_wall = pins_wall.left(base_width / 2 - wemos_pins_x_offset + gap + wall)
    pins_wall = pins_wall.forw(wemos_pins_y_offset - gap - wall)
    pins_wall += pins_wall.mirrorYZ()
    result += pins_wall

    def walls():
        net_height = content_height + gap - wall

        def net():
            rod = box(net_rod_width, wall, net_height)

            def side_info(length):
                rods_count = int(floor(
                    length / (net_rod_width + net_rods_separation)
                ))
                step = length / rods_count
                result = union([rod.right(step * i) for i in range(rods_count)])
                return result

            x_side = side_info(base_width - wall).left(base_width / 2)
            result = x_side
            result += x_side.back(base_length / 2).rotateZ(pi).forw(base_length / 2)

            y_side = side_info(base_length - wall).rotateZ(pi / 2).right(base_width / 2)
            result += y_side
            result += y_side.back(base_length / 2).rotateZ(pi).forw(base_length / 2)
            return result

        result = net()

        def top():
            top_height = wall + wall + gap
            result = box(base_width, base_length, top_height)
            result = result.left(base_width / 2)
            hole_sbtn_width = base_width - wall * 2 * 2
            hole_sbtn = box(hole_sbtn_width, base_length - wall * 2, wall)
            hole_sbtn = hole_sbtn.left(hole_sbtn_width / 2)
            hole_sbtn = hole_sbtn.forw(wall)
            result -= hole_sbtn
            cap_sbtn_width = base_width - wall * 2
            cap_sbtn_height = wall + gap
            cap_sbtn = box(cap_sbtn_width, base_length, cap_sbtn_height)
            cap_sbtn = cap_sbtn.left(cap_sbtn_width / 2)
            cap_sbtn = cap_sbtn.chamfer(
                wall + gap - tech,
                [
                    (-cap_sbtn_width / 2, base_length / 2, cap_sbtn_height),
                    (cap_sbtn_width / 2, base_length / 2, cap_sbtn_height)
                ]
            )
            cap_sbtn = cap_sbtn.up(wall)
            result -= cap_sbtn
            result = result.up(net_height)
            return result

        result += top()
        result = result.up(wall)
        return result

    result += walls()

    pins_sbtn = box(pins_sbtn_width, pins_sbtn_length, wemos_pins_height)
    pins_sbtn = pins_sbtn.left(base_width / 2 - wemos_pins_x_offset + gap)
    pins_sbtn = pins_sbtn.forw(wemos_pins_y_offset - gap)
    pins_sbtn += pins_sbtn.mirrorYZ()
    result -= pins_sbtn
    result = result.chamfer(
        gap * 2,
        [
            point3(base_width / 2 - wemos_pins_x_offset, wemos_pins_y_offset + pins_wall_length / 2, 0),
            point3(-base_width / 2 + wemos_pins_x_offset, wemos_pins_y_offset + pins_wall_length / 2, 0),
            point3(base_width / 2 - wemos_pins_x_offset - wemos_pins_width - gap * 2,
                   wemos_pins_y_offset + pins_wall_length / 2, 0),
            point3(-base_width / 2 + wemos_pins_x_offset + wemos_pins_width + gap * 2,
                   wemos_pins_y_offset + pins_wall_length / 2, 0),
            point3(base_width / 2 - wemos_pins_x_offset - wemos_pins_width / 2 - gap,
                   wemos_pins_y_offset + wemos_pins_length, 0),
            point3(base_width / 2 - wemos_pins_x_offset - wemos_pins_width / 2 - gap, wemos_pins_y_offset, 0),
            point3(-base_width / 2 + wemos_pins_x_offset + wemos_pins_width / 2 + gap,
                   wemos_pins_y_offset + wemos_pins_length, 0),
            point3(-base_width / 2 + wemos_pins_x_offset + wemos_pins_width / 2 + gap, wemos_pins_y_offset, 0)
        ]
    )
    result -= sphere(side_sign_radius)

    return result


def cap():
    width = base_width - (wall + gap * sqrt(2) * 2) * 2
    result = box(width, base_length, wall)
    result = result.left(width / 2)
    result = result.chamfer(
        wall - tech,
        [
            (-width / 2, base_length / 2, wall),
            (width / 2, base_length / 2, wall)
        ]
    )
    result = result.up(wall + gap + content_height + gap)
    return result


mechanism(
    use_mask=True,
    objects=[
        MechanismObject(
            model=holder(),
            color=Color(1, 1, 0),
            to_stl_name="holder",
            use=True
        ),
        MechanismObject(
            model=cap(),
            color=Color(1, 0, 1),
            to_stl_name="cap",
            use=True
        )
    ]
)
