#!/usr/bin/env python3
# coding: utf-8

from zencad import *

tile_depth = 7
wall = 1.2
tape_width = 11
tape_height = 6
holder_length = 4.8
back_side_length = 16

holder = polysegment(
    pnts=[
        (0, 0),
        (tile_depth, 0),
        (tile_depth, wall + back_side_length),
        (-wall, wall),
        (-wall, wall - tape_height),
        (-wall - tape_width, wall - tape_height),
        (-wall - tape_width, wall),
        (-wall - tape_width - wall, wall),
        (-wall - tape_width - wall, -tape_height),
        (0, -tape_height)
    ],
    closed=True
)

holder = holder.fill()
holder = holder.extrude(holder_length)

disp(holder)

show()
