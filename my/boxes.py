#!/usr/bin/env python3
# coding: utf-8

from mechanism import *
from spring import *

tech = 0.001
friction_big = 0.2
friction_small = 0.1
wall = 2.0

cell_size = 70
cells_x = 0.75
cells_y = 1
cells_z = 0.2

finger_width = 22.0
finger_length = 12.0
finger_corner_radius = finger_width / 2 - tech

spring_wall = wall
spring_segments = 3
spring_segment_length = spring_wall * 5

finger_hook_wall = 0.6

hook_angle_ratio = 2.0
hook_length = wall
bolt_height = hook_length * hook_angle_ratio
bolt_scroll_length = hook_length
bolt_finger_ledge_height = wall + friction_big
spring_compression_factor = 0.9

cap_min_y = wall + friction_big
bolt_min_y = cap_min_y - bolt_scroll_length
cap_finger_min_y = cap_min_y + wall
bolt_finger_min_y = cap_finger_min_y + friction_big
bolt_finger_hook_length = wall
bolt_finger_length = wall + finger_length + bolt_finger_hook_length + wall
bolt_finger_max_y = bolt_finger_min_y + bolt_finger_length
cap_finger_max_y = bolt_finger_max_y + bolt_scroll_length + friction_small
bolt_max_y = cap_finger_max_y + bolt_finger_ledge_height + friction_big * sqrt(2)
spring_min_y = bolt_max_y + friction_small
spring_length = spring_segment_length * spring_segments + spring_wall
spring_max_y = spring_min_y + spring_length * spring_compression_factor
bolt_cap_holder_min_y = spring_max_y + friction_small
bolt_cap_holder_max_y = bolt_cap_holder_min_y + wall + wall

bolt_max_x = finger_width / 2 + wall
cap_finger_max_x = bolt_max_x + friction_big
bolt_cap_holder_min_x = cap_finger_max_x
bolt_cap_holder_max_x = bolt_cap_holder_min_x + wall + wall

cap_max_z = wall
bolt_min_z = cap_max_z + friction_big
bolt_max_z = bolt_min_z + bolt_height
bolt_cap_min_z = bolt_max_z + friction_big
bolt_cap_max_z = bolt_cap_min_z + wall


def create_spring():
    result = spring(
        segments_count=spring_segments,
        wall=spring_wall,
        height=bolt_height,
        width=bolt_max_x * 2,
        segment_length=spring_segment_length
    )
    result = result.forw(spring_min_y)
    result = result.up(bolt_min_z)
    return result


def create_bolt():
    length = bolt_max_y - bolt_min_y
    result = box(bolt_max_x * 2, length, bolt_height)
    result = result.left(bolt_max_x)
    result -= halfspace().rotateX(-pi / 2 - atan(1 / hook_angle_ratio))
    hook_sbtn_width = bolt_max_x * 2 - wall * 2
    hook_sbtn = box(hook_sbtn_width, bolt_scroll_length, bolt_height)
    hook_sbtn = hook_sbtn.left(hook_sbtn_width / 2)
    result -= hook_sbtn
    result = result.forw(bolt_min_y)
    result = result.up(bolt_min_z)
    bolt_finger_max_z = bolt_max_z - wall
    result += box(
        bolt_max_x * 2,
        bolt_finger_max_y - bolt_finger_min_y,
        bolt_min_z
    ).translate(
        -bolt_max_x,
        bolt_finger_min_y
    ).fillet(
        finger_corner_radius + wall,
        [
            (-bolt_max_x, bolt_finger_min_y, wall / 2),
            (bolt_max_x, bolt_finger_min_y, wall / 2)
        ]
    )
    result = chamfer(result, bolt_finger_ledge_height - tech, [(0, bolt_finger_max_y, bolt_finger_ledge_height)])
    result -= box(
        (bolt_max_x - wall) * 2,
        bolt_finger_length - wall - wall,
        bolt_finger_max_z
    ).translate(
        -bolt_max_x + wall,
        bolt_finger_min_y + wall
    ).fillet(
        finger_corner_radius,
        [
            (-bolt_max_x + wall, bolt_finger_min_y, wall / 2),
            (bolt_max_x - wall, bolt_finger_min_y, wall / 2)
        ]
    )
    result = result.fillet(
        bolt_finger_max_z - tech,
        [(0, bolt_finger_min_y + wall, bolt_finger_max_z)]
    )
    finger_hook = box(bolt_max_x * 2, wall, finger_hook_wall + wall)
    finger_hook = finger_hook.left(bolt_max_x)
    finger_hook = chamfer(finger_hook, wall - tech, [(0, 0, finger_hook_wall + wall)])
    finger_hook = finger_hook.forw(bolt_finger_max_y - wall - bolt_finger_hook_length)
    result += finger_hook

    return result


def create_bolt_cap_shape(
        width, length, height
):
    result = box(width, length, height)
    result = result.left(width / 2)
    result = result.chamfer(
        height - tech,
        [
            (-width / 2, length / 2, height),
            (width / 2, length / 2, height),
            (0, length, height)
        ]
    )
    return result


def create_cap():
    width = cell_size * cells_x - (wall + friction_big) * 2
    cap_max_y = cell_size * cells_y - wall - friction_big - wall
    length = cap_max_y - cap_min_y
    result = box(width, length, wall)
    result = result.forw(cap_min_y)
    result = result.left(width / 2)

    def finger_sbtn():
        result = box(
            cap_finger_max_x * 2,
            cap_finger_max_y - cap_finger_min_y + wall,
            wall
        )
        result = result.left(cap_finger_max_x)
        result = result.forw(cap_finger_min_y)
        result = result.fillet(
            finger_corner_radius + wall + friction_big,
            [
                (-cap_finger_max_x, cap_finger_min_y, wall / 2),
                (cap_finger_max_x, cap_finger_min_y, wall / 2)
            ]
        )
        result = chamfer(result, wall - tech, [(0, cap_finger_max_y + wall, 0)])
        return result

    result -= finger_sbtn()

    bolt_cap_holder_height = bolt_cap_max_z - cap_max_z

    def bolt_cap_holder():
        length = bolt_cap_holder_max_y - cap_min_y
        result = box(bolt_cap_holder_max_x * 2, length, bolt_cap_holder_height)
        result = result.left(bolt_cap_holder_max_x)
        result = result.forw(cap_min_y)
        result = result.fillet(
            wall * 2, [
                (bolt_cap_holder_max_x, bolt_cap_holder_max_y, bolt_cap_holder_height / 2),
                (-bolt_cap_holder_max_x, bolt_cap_holder_max_y, bolt_cap_holder_height / 2)
            ]
        )
        result = result.fillet(wall, [(0, bolt_cap_holder_max_y, bolt_cap_holder_height)])
        sbtn = box(bolt_cap_holder_min_x * 2, bolt_cap_holder_min_y - cap_min_y, bolt_cap_holder_height)
        sbtn = sbtn.left(bolt_cap_holder_min_x)
        sbtn = sbtn.forw(cap_min_y)
        result -= sbtn
        result = result.up(cap_max_z)
        sbtn = create_bolt_cap_shape(
            (bolt_cap_holder_max_x - wall) * 2 + friction_small * sqrt(2),
            length - wall + friction_small * sqrt(2),
            wall + friction_small
        )
        sbtn = sbtn.up(bolt_cap_min_z-friction_small)
        sbtn = sbtn.forw(cap_min_y)
        result -= sbtn
        return result

    result += bolt_cap_holder()
    result = result.fillet(
        bolt_cap_holder_height - wall - tech,
        [(0, bolt_cap_holder_max_y, cap_max_z)]
    )

    def back_hook():
        result = box(width, wall, wall)
        result = result.left(width / 2)
        result -= halfspace().rotateX(pi / 4)
        result = result.forw(cap_max_y)
        return result

    result += back_hook()

    return result


def create_bolt_cap():
    compression = friction_small * sqrt(2)
    result = create_bolt_cap_shape(
        (bolt_cap_holder_max_x - wall - compression) * 2,
        (bolt_cap_holder_max_y - cap_min_y) - wall - compression,
        wall
    )
    result = result.up(bolt_cap_min_z)
    result = result.forw(cap_min_y)
    return result


def create_cell():
    width = cell_size * cells_x
    length = cell_size * cells_y
    height = cell_size * cells_z
    result = box(width, length, height)
    result = result.left(width / 2)
    return result


def create_box():
    width = cell_size * cells_x
    length = cell_size * cells_y
    height = cell_size * cells_z
    result = box(width, length, height)
    result = result.left(width / 2)
    sbtn = box(width - wall * 2, length - wall * 2, height-wall)
    sbtn = sbtn.left(width / 2 - wall)
    sbtn = sbtn.forw(wall)
    result -= sbtn
    tier_min_z = wall+friction_big
    tier = box(width - wall * 2, length - wall * 2, wall)
    tier = tier.left(width / 2 - wall)
    tier = tier.forw(wall)
    tier_sbtn = box(width - wall * 4, length - wall * 3, wall)
    tier_sbtn = tier_sbtn.left(width / 2 - wall * 2)
    tier_sbtn = tier_sbtn.forw(wall)
    tier -= tier_sbtn
    tier = tier.chamfer(
        wall-tech,
        [
            (0, length - wall * 2, wall),
            (width / 2 - wall * 2, length/2, wall),
            (-width / 2 + wall * 2, length/2, wall)
        ]
    )
    tier = tier.up(tier_min_z)
    result += tier
    return result

mechanism(
    use_mask=True,
    mask=halfspace().rotateY(-pi / 2),
    objects=[
        MechanismObject(
            model=create_cell(),
            color=Color(1, 1, 1, 0.75)
        ),
        MechanismObject(
            model=create_box(),
            to_stl_name="box",
            color=Color(0, 0, 1)
        ),
        MechanismObject(
            model=create_spring(),
            to_stl_name="spring",
            color=Color(1, 1, 0)
        ),
        MechanismObject(
            model=create_bolt(),
            to_stl_name="bolt",
            color=Color(1, 0, 1)
        ),
        MechanismObject(
            model=create_cap(),
            to_stl_name="cap",
            color=Color(0, 1, 1)
        ),
        MechanismObject(
            model=create_bolt_cap(),
            to_stl_name="bolt_cap",
            color=Color(0, 1, 0)
        )
    ]
)
