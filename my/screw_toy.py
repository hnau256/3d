from screw import *
from math import *
from grip import *
from mechanism import *

wall = 2.4
tech = 0.001
toy_friction = 0.8
tech_friction = 0.4
toy_screw_depth = 3.6
tech_screw_depth = 1.2

cap_thickness = 9.6
cap_radius = cap_thickness * 3
cap_handle_length = cap_radius / 2

trunk_main_length = 150
trunk_radius = (wall + tech_friction + tech_screw_depth + wall) * sqrt(2)

top_screw_length = 9.6

toy_screw_style = ScrewStyle(
    depth=toy_screw_depth,
    enters=4,
    angle=pi / 1.2,
    deepening=tech
)

tech_screw_style = ScrewStyle(
    depth=tech_screw_depth,
    deepening=tech
)


def build_cap():
    cap_handles_count = int(floor(cap_radius * 2 * pi / cap_handle_length / 2)) * 2
    cap_handle_angle = pi * 2 / cap_handles_count
    handler_radius = sin(cap_handle_angle / 2) * cap_radius
    handler = cylinder(handler_radius, cap_thickness)
    handler = handler.right(cap_radius)
    result = ngon(cap_radius, cap_handles_count)
    result = result.extrude(cap_thickness)
    for i in range(cap_handles_count):
        i_handler = handler.rotateZ(i * cap_handle_angle)
        if i % 2 == 0:
            result += i_handler
        else:
            result -= i_handler
    max_radius = cap_radius + handler_radius
    result = unify(result)
    chamfer_length = cap_thickness / 4
    result = chamfer(result, chamfer_length, [(max_radius, 0, 0), (max_radius, 0, cap_thickness)])
    return result


cap = build_cap()

top_box_radius = trunk_radius / sqrt(2)
top_box_length = top_box_radius * 2
top_screw_radius = top_box_radius - wall - tech_friction - tech_screw_depth
top_screw_cap_radius = top_screw_radius + tech_screw_depth + wall


def build_top_screw_sbtn():
    height = top_screw_length + tech_friction
    result = tech_screw_style.create_shape(top_screw_radius + tech_friction, height - tech)
    cap_radius = top_screw_cap_radius + tech_friction
    cap = cone(cap_radius, 0, cap_radius)
    cap = cap.mirrorXY()
    cap = cap.up(height)
    result += cap
    result = result.up(cap_thickness + trunk_main_length + tech_friction + cap_thickness - height)
    return result


top_screw_sbtn = build_top_screw_sbtn()


def trunk():
    result = toy_screw_style.create_shape(trunk_radius, trunk_main_length)
    result ^= result.mirrorXZ()
    top_box = box(top_box_length, top_box_length, cap_thickness - wall - tech_friction)
    top_box = top_box.translate(-top_box_length / 2, -top_box_length / 2)
    top_box = top_box.up(trunk_main_length)
    result += top_box
    result = result.up(cap_thickness)
    result += cap
    result -= top_screw_sbtn
    return result


def top_cap():
    result = cap
    top_box_sbtn_length = top_box_length + tech_friction * 2
    top_box_sbtn = box(top_box_sbtn_length, top_box_sbtn_length, cap_thickness - wall)
    top_box_sbtn = top_box_sbtn.translate(-top_box_sbtn_length / 2, -top_box_sbtn_length / 2)
    result -= top_box_sbtn
    result = result.up(cap_thickness + trunk_main_length + tech_friction)
    result -= top_screw_sbtn
    return result


def top_screw():
    height = top_screw_length
    result = tech_screw_style.create_shape(top_screw_radius, height - tech)
    cap_radius = top_screw_cap_radius
    cap = cone(cap_radius, 0, cap_radius)
    cap = cap.mirrorXY()
    cap = cap.up(height)
    result += cap
    cap_sbtn_radius = cap_radius - wall/sqrt(2)
    cap_sbtn = cone(cap_sbtn_radius, 0, cap_sbtn_radius)
    cap_sbtn_istn = box((cap_sbtn_radius + tech) * 2, wall, cap_sbtn_radius + tech)
    cap_sbtn_istn = cap_sbtn_istn.translate(-(cap_sbtn_radius + tech), -wall / 2)
    cap_sbtn ^= cap_sbtn_istn
    cap_sbtn = cap_sbtn.mirrorXY()
    cap_sbtn = cap_sbtn.up(height)
    result -= cap_sbtn
    result = result.up(cap_thickness + trunk_main_length + tech_friction + cap_thickness - height)
    return result


def build_base_nut():
    resut = cap
    resut -= toy_screw_style.create_shape(trunk_radius + toy_friction, cap_thickness)
    return resut


base_nut = build_base_nut()


def nut():
    result = base_nut
    result = result.up(cap_thickness + trunk_main_length * 1 / 3 - cap_thickness / 2)
    return result


def nut_opposite():
    result = base_nut
    result = result.mirrorXZ()
    result = result.up(cap_thickness + trunk_main_length * 2 / 3 - cap_thickness / 2)
    return result


mechanism(
    objects=[
        MechanismObject(
            model=trunk(),
            color=color(1, 1, 0),
            to_stl_name="trunk"
        ),
        MechanismObject(
            model=top_cap(),
            color=color(0, 1, 1),
            to_stl_name="top_cap"
        ),
        MechanismObject(
            model=top_screw(),
            color=color(1, 0, 1),
            to_stl_name="top_screw"
        ),
        MechanismObject(
            model=nut(),
            color=color(1, 1, 1),
            to_stl_name="nut"
        ),
        MechanismObject(
            model=nut_opposite(),
            color=color(1, 1, 1),
            to_stl_name="nut_opposite"
        )
    ]
)
