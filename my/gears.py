#!/usr/bin/env python3
# coding: utf-8

from involute import *
from screw import *
from mechanism import *

wall = 2.4
friction = 0.3
tech = 0.001

gear_thickness = wall * 2
gear_module = 2.4
left_gear_preferred_radius = 25
right_gear_preferred_radius = 40
screw_depth = 1.2
shaft_radius = wall + screw_depth + friction + wall
gear_spokes_separation = wall * 4

involute_style = InvoluteStyle(
    module=gear_module
)


def create_gear_context(gear_preferred_radius: float) -> GearContext:
    return GearContext.find_min(
        style=involute_style,
        predicate=lambda context: context.outside_radius >= gear_preferred_radius
    )


left_gear_context = create_gear_context(left_gear_preferred_radius)
right_gear_context = create_gear_context(right_gear_preferred_radius)


def gear(context: GearContext, rotate: bool = False):
    result = context.create_profile()
    result = result.fill()
    result = result.extrude(gear_thickness)
    if rotate:
        result = result.rotateZ(context.tooth_angle / 2)
    inner_radius = context.root_radius - wall
    result -= cylinder(inner_radius, gear_thickness)
    result += cylinder(shaft_radius + friction + wall, gear_thickness)
    result -= cylinder(shaft_radius + friction, gear_thickness)
    spokes_count = int(floor(inner_radius * 2 * pi / gear_spokes_separation))
    spoke_angle = pi * 2 / spokes_count
    spoke_min_radius = shaft_radius + friction + tech
    spoke_max_radius = inner_radius - tech
    spoke = box(spoke_max_radius - spoke_min_radius, wall, gear_thickness)
    spoke = spoke.translate(spoke_min_radius, -wall / 2)
    spokes = union([spoke.rotateZ(spoke_angle * i) for i in range(spokes_count)])
    result += spokes
    result = result.down(gear_thickness / 2)
    return result


left_gear_offset = left_gear_context.pitch_radius + friction / 2
right_gear_offset = right_gear_context.pitch_radius + friction / 2


def create_base_cap_billet():
    radius = shaft_radius + wall
    result = box(left_gear_offset + right_gear_offset, radius * 2, wall)
    result = result.translate(-left_gear_offset, -radius)
    side = cylinder(radius, wall + friction)
    side = side.down(friction)
    result += side.left(left_gear_offset)
    result += side.right(right_gear_offset)
    result = result.up(gear_thickness / 2 + friction + friction)
    return result


base_cap_billet = create_base_cap_billet()

screw_style = ScrewStyle(
    depth=screw_depth
)

shaft_height = friction + gear_thickness + friction


def base():
    result = base_cap_billet.mirrorXY()
    shaft = cylinder(shaft_radius, shaft_height)
    shaft = shaft.down(shaft_height / 2)
    result += shaft.left(left_gear_offset)
    result += shaft.right(right_gear_offset)
    nut_sbtn = screw_style.create_shape(wall + friction, shaft_height+friction).down(friction)
    nut_sbtn = nut_sbtn.down(shaft_height / 2)
    result -= nut_sbtn.left(left_gear_offset)
    result -= nut_sbtn.right(right_gear_offset)
    return result


def cap():
    result = base_cap_billet
    sbyn_radius = wall + screw_depth + friction
    sbtn = cone(sbyn_radius, sbyn_radius + wall + friction, wall + friction)
    sbtn = sbtn.up(gear_thickness / 2 + friction)
    result -= sbtn.left(left_gear_offset)
    result -= sbtn.right(right_gear_offset)
    return result


def create_screw():
    result = screw_style.create_shape(wall, shaft_height + wall-tech)
    cap_radius = wall + wall + screw_depth
    cap = cone(0, cap_radius, cap_radius)
    cap = cap.up(gear_thickness + friction + friction + wall - cap_radius)
    result += cap
    handle_sbtn_radius = cap_radius - wall
    handle_sbtn = cone(0, handle_sbtn_radius, handle_sbtn_radius)
    handle_sbtn ^= box((handle_sbtn_radius+tech)*2, wall, handle_sbtn_radius+tech).translate(-handle_sbtn_radius-tech, -wall/2)
    handle_sbtn = handle_sbtn.up(gear_thickness + friction + friction + wall - handle_sbtn_radius)
    result -= handle_sbtn
    result = result.down(gear_thickness/2)
    return result


screw = create_screw()

mechanism(
    use_mask=True,
    objects=[
        MechanismObject(
            model=gear(left_gear_context).left(left_gear_offset),
            to_stl_name="left_gear",
            color=color(1, 1, 0)
        ),
        MechanismObject(
            model=gear(right_gear_context, rotate=True).right(right_gear_offset),
            to_stl_name="right_gear",
            color=color(0, 1, 1)
        ),
        MechanismObject(
            model=base(),
            to_stl_name="base",
            color=color(1, 0, 1)
        ),
        MechanismObject(
            model=cap(),
            to_stl_name="cap",
            color=color(1, 0, 0)
        ),
        MechanismObject(
            model=screw.left(left_gear_offset),
            to_stl_name="screw",
            color=color(0, 0, 1)
        ),
        MechanismObject(
            model=screw.right(right_gear_offset),
            color=color(0, 0, 1)
        )
    ]
)
