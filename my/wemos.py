#!/usr/bin/env python3
# coding: utf-8

from mechanism import *

wall = 1.2
gap = 0.2
tech = 0.001

wemos_width = 25.5
wemos_length = 35
wemos_main_height = 1
wemos_ledge_base_width = 16
wemos_ledge_base_length = 24.5
wemos_ledge_base_y_offset = wemos_length - wemos_ledge_base_length
wemos_ledge_base_height = 1
wemos_ledge_main_width = 12
wemos_ledge_main_length = 15
wemos_ledge_main_y_offset = 12
wemos_ledge_main_height = 3.25
wemos_pins_width = 2.5
wemos_pins_length = 21
wemos_pins_y_offset = 7.25
wemos_pins_height = 8.5
wemos_pices_width = wemos_width
wemos_pices_length = wemos_pins_y_offset + wemos_pins_length
wemos_pices_height = 2.25
wemos_led_width = 2
wemos_led_length = 2
wemos_led_y_offset = wemos_ledge_main_y_offset + wemos_ledge_main_length
wemos_led_x_offset = (wemos_width - wemos_ledge_base_width) / 2 + 1
wemos_led_height = wemos_ledge_main_height
wemos_reset_hole_width = wall * 5
wemos_reset_hole_length = 2
wemos_reset_hole_height = 2
wemos_reset_hole_y_offset = 2.75
wemos_reset_hole_z_offset = 1
side_sign_radius = wall * 2

def wemos(
        offset=0.0
):
    boxes = [
        (
            (-wemos_width / 2, 0, 0),
            (wemos_width, wemos_length, wemos_main_height)
        ),
        (
            (-wemos_pices_width / 2, 0, wemos_main_height),
            (wemos_pices_width, wemos_pices_length, wemos_pices_height)
        ),
        (
            (-wemos_ledge_base_width / 2, wemos_ledge_base_y_offset, -wemos_ledge_base_height),
            (wemos_ledge_base_width, wemos_ledge_base_length, wemos_ledge_base_height)
        ),
        (
            (-wemos_ledge_main_width / 2, wemos_ledge_main_y_offset, -wemos_ledge_main_height),
            (wemos_ledge_main_width, wemos_ledge_main_length, wemos_ledge_main_height)
        ),
        (
            (-wemos_width / 2, wemos_pins_y_offset, -wemos_pins_height),
            (wemos_pins_width, wemos_pins_length, wemos_pins_height)
        ),
        (
            (wemos_width / 2 - wemos_pins_width, wemos_pins_y_offset, -wemos_pins_height),
            (wemos_pins_width, wemos_pins_length, wemos_pins_height)
        ),
        (
            (-wemos_width / 2 + wemos_led_x_offset, wemos_led_y_offset, -wemos_led_height),
            (wemos_led_width, wemos_led_length, wemos_led_height)
        ),
        (
            (wemos_width / 2, wemos_reset_hole_y_offset, wemos_reset_hole_z_offset),
            (wemos_reset_hole_width, wemos_reset_hole_length, wemos_reset_hole_height)
        )
    ]
    result = []
    for item in boxes:
        position = item[0]
        size = item[1]
        shape = box(
            size[0] + offset * 2,
            size[1] + offset * 2,
            size[2] + offset * 2
        )
        shape = shape.translate(
            position[0] - offset,
            position[1] - offset,
            position[2] - offset
        )
        result.append(shape)
    result = union(result)
    result = unify(result)
    return result


def holder():
    x_wall = wall * 2
    width = wemos_width + (gap + x_wall) * 2
    length = wemos_length + gap + wall
    height = wemos_main_height + wemos_pices_height + wemos_ledge_main_height + wall + gap
    result = box(width, length, height)
    result = result.left(width / 2)
    result -= sphere(side_sign_radius)
    result = result.down(wemos_ledge_main_height)
    sbtn_width = width - x_wall * 2
    top_sbtn = box(sbtn_width, length, wall + gap)
    top_sbtn = top_sbtn.left(sbtn_width / 2)
    top_sbtn = top_sbtn.up(wemos_main_height + wemos_pices_height)
    result -= top_sbtn
    back_sbtn_length = wemos_length - wemos_pices_length + wall + gap
    back_sbtn = box(sbtn_width, back_sbtn_length, wemos_pices_height)
    back_sbtn = back_sbtn.left(sbtn_width / 2)
    back_sbtn = back_sbtn.up(wemos_main_height)
    back_sbtn = back_sbtn.forw(length - back_sbtn_length)
    result -= back_sbtn
    cap_sbtn_width = width - wall * 2
    cap_sbtn_height = wall + gap
    cap_sbtn = box(cap_sbtn_width, length, cap_sbtn_height)
    cap_sbtn = cap_sbtn.left(cap_sbtn_width / 2)
    cap_sbtn = cap_sbtn.chamfer(
        cap_sbtn_height - tech,
        [
            (-cap_sbtn_width / 2, length / 2, wall),
            (cap_sbtn_width / 2, length / 2, wall)
        ]
    )
    cap_sbtn_istn_width = cap_sbtn_width - gap * sqrt(2) * 2
    cap_sbtn_istn = box(cap_sbtn_istn_width, length, cap_sbtn_height)
    cap_sbtn_istn = cap_sbtn_istn.left(cap_sbtn_istn_width / 2)
    cap_sbtn ^= cap_sbtn_istn
    cap_sbtn = cap_sbtn.up(wemos_main_height + wemos_pices_height)
    result -= cap_sbtn
    cap_back_sbtn_width = width - (wall + gap * sqrt(2)) * 2
    cap_back_sbtn_length = wemos_length - wemos_pices_length + wall + gap
    cap_back_sbtn_height = wemos_pices_height
    cap_back_sbtn = box(cap_back_sbtn_width, cap_back_sbtn_length, cap_back_sbtn_height)
    cap_back_sbtn = cap_back_sbtn.left(cap_back_sbtn_width / 2)
    cap_back_sbtn = cap_back_sbtn.up(wemos_main_height)
    cap_back_sbtn = cap_back_sbtn.forw(length - cap_back_sbtn_length)
    result -= cap_back_sbtn
    result -= wemos(gap)
    result = unify(result)
    return result


def cap():
    width = wemos_width + (wall - gap * sqrt(2)) * 2
    length = wemos_length + gap + wall
    result = box(width, length, wall)
    result = result.left(width / 2)
    result = result.chamfer(
        wall - tech,
        [
            (width / 2, length / 2, wall),
            (-width / 2, length / 2, wall)
        ]
    )
    result = result.up(wemos_main_height + wemos_pices_height + gap)
    back_width = width
    back_length = wemos_length - wemos_pices_length + wall
    back_height = wemos_pices_height
    back = box(back_width, back_length, back_height)
    back = back.left(back_width / 2)
    back = back.up(wemos_main_height + gap)
    back = back.forw(wemos_length + wall + gap - back_length)
    result += back
    return result


mechanism(
    mask=halfspace().rotateY(-pi / 2).right(13),
    use_mask=False,
    objects=[
        MechanismObject(
            model=wemos(),
            color=Color(1, 1, 1),
            use=True
        ),
        MechanismObject(
            model=holder(),
            color=Color(1, 1, 0),
            to_stl_name="holder",
            use=True
        ),
        MechanismObject(
            model=cap(),
            color=Color(1, 0, 1),
            to_stl_name="cap",
            use=True
        )
    ]
)
