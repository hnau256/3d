#!/usr/bin/env python3
# coding: utf-8

from mechanism import *

wall = 1.8
wire_hole_percentage = 0.6

wire_radius = 1.7
length = 10

def handler():
	total_radius = wire_radius+wall
	result = cylinder(total_radius, length)
	result += box(total_radius, total_radius*2, length).back(total_radius)
	hole_radius = wire_radius * wire_hole_percentage
	result -= box(hole_radius*2, total_radius, length).left(hole_radius)
	result -= cylinder(wire_radius, length)
	return result

mechanism(
	use_mask = False,
    objects=[
        MechanismObject(
            model=handler(),
            to_stl_name="handler",
            color=color(1, 1, 0)
        )
    ]
)

