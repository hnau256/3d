#!/usr/bin/env python3
# coding: utf-8

from mechanism import *
from screw import *

tech = 0.001
clearance = 0.2
friction = 0.5
wall = 2.4

bowl_radius = 89 / 2
cap_depth = 9.6
cap_ladge = 2.4
cap_ladge_chamfer = 2.4
cap_dome_radius_factor = 1.5
hole_radius = 12
hole_fillet = 1.2
hole_angle = pi / 5
screw_radius = 3.6
screw_depth = 1.2
aligner_radius = 2.4
aligner_depth = cap_depth - wall - aligner_radius - clearance * sqrt(2)
aligner_offset = bowl_radius - friction - wall - clearance - aligner_radius
handle_radius = 12
handle_z_offset = -2.4
handle_fillet_radius = 6.4
handle_fillet_r = 2.4
handle_fillet_z = 3.6


def base():
    height = cap_depth * 4
    result = cylinder(bowl_radius + wall, height)
    result -= cylinder(bowl_radius, height)
    result = result.down(height + clearance)
    return result


top_radius = bowl_radius + cap_ladge + cap_ladge_chamfer
dome_radius = top_radius * cap_dome_radius_factor
dome_z_offset = sqrt(dome_radius ** 2 - top_radius ** 2)
dome_max_z = dome_radius - dome_z_offset

screw_cap_radius = screw_radius + screw_depth + clearance
screw_max_z = dome_max_z + handle_z_offset + handle_radius
screw_min_z = -(cap_depth - screw_cap_radius - clearance + clearance)
screw_length = screw_max_z - screw_min_z

screw_style = ScrewStyle(
    depth=screw_depth
)


def create_cap():
    bottom_radius = bowl_radius - friction

    screw_sbtn_radius = screw_radius + clearance * sqrt(2)

    def create_screw_sbtn():
        length = screw_length - clearance * 2
        min_z = screw_min_z + clearance
        result = screw_style.create_shape(screw_sbtn_radius, length)
        cap = cone(screw_cap_radius, 0, screw_cap_radius)
        result += cap.mirrorXY()
        result += cap.up(length)
        result = result.up(min_z)
        return result

    screw_sbtn = create_screw_sbtn()

    def create_aligners_sbtn():
        depth = aligner_depth + clearance * sqrt(2)
        radius = aligner_radius + clearance
        aligner = cone(radius, 0, radius)
        aligner = aligner.up(depth)
        aligner += aligner.rotateX(pi)
        aligner += cylinder(radius, depth * 2, center=True)
        sbtn_delta = friction * sqrt(2)
        sbtn = cone(radius+sbtn_delta, radius, sbtn_delta)
        sbtn += sbtn.mirrorXY()
        aligner += sbtn
        aligner = aligner.forw(aligner_offset)
        result = union([aligner.rotateZ(pi / 2 * i) for i in range(3)])
        return result

    aligners_sbtn = create_aligners_sbtn()

    def create_hole_sbtn():
        z = sqrt((dome_radius - hole_fillet) ** 2 - (hole_radius + hole_fillet) ** 2)
        result = cone(0, (hole_radius + hole_fillet) * 2, z * 2).down(z)
        result -= torus(hole_radius + hole_fillet, hole_fillet)
        result = result.up(z)
        result -= cylinder(hole_radius + hole_fillet * 2, dome_radius).up(z - dome_radius)
        result += cylinder(hole_radius, dome_radius).up(z - dome_radius)
        result = result.rotateY(hole_angle)
        result = result.down(dome_z_offset)
        return result

    hole_sbtn = create_hole_sbtn()

    def bottom():
        result = cylinder(bottom_radius, cap_depth)
        result = result.fillet(cap_depth / 2, [(0, bottom_radius, 0)])
        result = result.down(cap_depth)
        result -= hole_sbtn
        result -= aligners_sbtn
        result -= screw_sbtn
        sbtn_height = friction + screw_depth
        result -= cone(screw_sbtn_radius, screw_sbtn_radius + sbtn_height, sbtn_height).down(sbtn_height)
        return result

    def create_top():
        result = sphere(dome_radius)
        result = result.down(dome_z_offset)
        result -= halfspace()
        result = result.chamfer(cap_ladge_chamfer, [(0, top_radius, 0)])
        result -= hole_sbtn
        result -= aligners_sbtn
        handle_min_z = dome_max_z + handle_z_offset
        handle = sphere(handle_radius)
        handle = handle.up(handle_min_z + handle_radius)
        result += handle
        handle_main_z = handle_min_z + handle_fillet_z
        result = result.fillet(handle_fillet_r, [(0, handle_fillet_r, handle_main_z)])
        istn = cylinder(dome_radius, handle_main_z + clearance + tech).down(clearance + tech)
        result_dome = result ^ istn
        result_dome -= cylinder(screw_radius + screw_depth + friction * sqrt(2), handle_main_z)
        result_handle = result - istn.up(clearance)
        result_handle -= screw_sbtn
        sbtn_height = friction + screw_depth
        result_handle -= cone(screw_sbtn_radius + sbtn_height, screw_sbtn_radius, sbtn_height) \
            .up(handle_main_z + clearance)
        return result_dome, result_handle

    top = create_top()

    return bottom(), top[0], top[1]


cap = create_cap()


def create_aligner():
    depth = aligner_depth
    radius = aligner_radius
    cap_height = friction * sqrt(2)
    result = cylinder(radius, (depth-cap_height) * 2, center=True)
    cap = cone(radius, radius-cap_height, cap_height)
    cap = cap.up(depth-cap_height)
    result += cap
    result += cap.mirrorXY()
    result = result.forw(aligner_offset)
    return result


aligner = create_aligner()


def createScrew():
    result = screw_style.create_shape(screw_radius, screw_length)
    istn = cone(screw_radius+screw_depth, screw_radius, screw_depth)
    istn = istn.up(screw_length-screw_depth) + istn.mirrorXY().up(screw_depth)
    istn += cylinder(screw_radius+screw_depth+tech, screw_length-screw_depth*2).up(screw_depth)
    result ^= istn
    result = result.up(screw_min_z)
    return result


mechanism(
    use_mask=True,
    objects=[
        MechanismObject(
            model=base(),
            color=Color(1, 1, 1, 0.75)
        ),
        MechanismObject(
            model=cap[0],
            color=Color(1, 1, 0),
            to_stl_name="bottom"
        ),
        MechanismObject(
            model=cap[1],
            color=Color(0, 1, 1),
            to_stl_name="top"
        ),
        MechanismObject(
            model=cap[2],
            color=Color(0, 0, 1),
            to_stl_name="handle"
        ),
        MechanismObject(
            model=createScrew(),
            color=Color(1, 0, 0),
            to_stl_name="screw"
        ),
        MechanismObject(
            model=aligner,
            color=Color(1, 0, 1),
            to_stl_name="aligner"
        ),
        MechanismObject(
            model=aligner.rotateZ(pi / 2),
            color=Color(1, 0, 1)
        ),
        MechanismObject(
            model=aligner.rotateZ(pi),
            color=Color(1, 0, 1)
        )
    ]
)
