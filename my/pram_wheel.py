#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from mechanism import *

tech = 0.001
correction = 0.2
friction = 1
bearing_radius = 11
bearing_width = 6
bearing_distance = 7
bearing_holder_width = 3
bearing_holder_radius = 7
bearing_min_stable_radius = 7
axis_radius = 5
axis_cap_width = 4
axis_cap_radis = 15.5 / 2
rim_radius = 55
rim_width = 25
rim_tire_adapter_radius = 13
rim_tire_adapter_border_width = 1.6


pram_adapter_radius = 53.7 / 2
pram_adapter_depth = 4
pram_adapter_wall = 1.7
brake_rod_depth = 5
brake_rod_radius = 3
brake_rod_offset = 19
brake_rod_wall = 2.4


bearings_width = bearing_width + bearing_distance + bearing_width + bearing_holder_width


def base():
    axis_length = 100
    result = cylinder(axis_radius, axis_length)
    result += cylinder(axis_cap_radis, axis_cap_width).down(axis_cap_width)
    bearing = cylinder(bearing_radius, bearing_width)
    result += bearing
    result += bearing.up(bearing_width + bearing_distance)
    bearing_holder = cylinder(bearing_holder_radius, bearing_holder_width)
    bearing_holder = bearing_holder.up(bearing_width + bearing_distance + bearing_width)
    result += bearing_holder

    def pram_adapter():
        result = cylinder(pram_adapter_radius + pram_adapter_wall, pram_adapter_depth + pram_adapter_wall)
        result -= cylinder(pram_adapter_radius, pram_adapter_depth)
        result = result.up(bearings_width - pram_adapter_depth)
        return result

    result += pram_adapter()

    brake_rod = cylinder(brake_rod_radius, brake_rod_depth)
    brake_rod = brake_rod.right(brake_rod_offset)
    brake_rod = brake_rod.up(bearings_width - brake_rod_depth)
    result += brake_rod

    result = result.up(rim_width - bearings_width + friction)

    return result


def ledges():
    ledge_max_z = rim_width + friction - bearing_holder_width - bearing_width - correction
    ledge_width = min(bearing_distance - correction * 2, bearing_radius + correction - bearing_min_stable_radius)
    ledge_min_radius = bearing_min_stable_radius
    ledge_max_radius = bearing_radius + correction

    additional_width = bearing_distance - correction * 2 - ledge_width

    def fixed():
        result = cylinder(ledge_max_radius, bearing_distance - correction * 2)
        result -= cone(ledge_max_radius, 0, ledge_max_radius)
        result -= cylinder(ledge_min_radius, bearing_distance - correction * 2)
        result = result.up(ledge_max_z - ledge_width-additional_width)
        return result

    def additional():
        result = cone(ledge_max_radius - correction, ledge_min_radius - correction, ledge_width)
        result -= cylinder(bearing_min_stable_radius, bearing_distance)
        result = result.up(ledge_max_z - ledge_width-additional_width)
        return result

    return fixed(), additional()


ledges = ledges()


def wheel():
    def rim():
        result = cylinder(rim_radius, rim_width)
        depth = rim_tire_adapter_radius - sqrt(
            rim_tire_adapter_radius ** 2 - (rim_width / 2 - rim_tire_adapter_border_width) ** 2)
        tire_sbtn = torus(rim_radius - depth + rim_tire_adapter_radius, rim_tire_adapter_radius)
        tire_sbtn = tire_sbtn.up(rim_width / 2)
        result -= tire_sbtn
        return result

    result = rim()

    def pram_adapter_sbtn():
        wall = friction + pram_adapter_wall + friction
        radius = pram_adapter_radius - friction
        width = pram_adapter_depth
        result = cylinder(radius + wall, width)
        result -= cylinder(radius, width)
        result = result.up(rim_width-width)
        return result

    result -= pram_adapter_sbtn()

    def brake_rod_sbtn():
        radius = brake_rod_radius + friction
        length = radius * 2 + brake_rod_wall
        count = floor(brake_rod_offset * pi * 2 / length)
        angle = pi * 2 / count
        rod = cylinder(radius, brake_rod_depth)
        rod = rod.right(brake_rod_offset)
        result = union([rod.rotateZ(angle*i) for i in range(count)])
        result = result.up(rim_width-brake_rod_depth)
        return result

    result -= brake_rod_sbtn()

    result -= cylinder(bearing_radius+correction, rim_width)

    result += ledges[0]

    return result


mechanism(
    objects=[
        MechanismObject(
            model=base(),
            color=Color(1, 1, 1, 0.5)
        ),
        MechanismObject(
            model=wheel(),
            color=Color(1, 1, 0),
            to_stl_name="wheel"
        ),
        MechanismObject(
            model=ledges[1],
            color=Color(1, 0, 1),
            to_stl_name="ledge"
        )
    ]
)
