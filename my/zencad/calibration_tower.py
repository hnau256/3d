from utils import *
from math import *

register_font('utils/roboto.ttf')


def create_calibration_tower(
        levels: int,
        min_value: int,
        value_step: int,
        level_width_length: float = 10,
        level_height: float = 10,
        separator_depth: float = 0.4,
        text_depth: float = 0.3,
        text_margin: float = 1,
) -> (str, Shape):
    def create_level(
            index: int,
    ) -> Shape:
        z = box(level_width_length, level_width_length, level_height)
        z = z.translate(
            -level_width_length / 2,
            -level_width_length / 2,
        )
        z = chamfer(
            z,
            separator_depth,
            [
                (
                    level_width_length / 2,
                    0,
                    0,
                ),
                (
                    -level_width_length / 2,
                    0,
                    0,
                ),
                (
                    0,
                    level_width_length / 2,
                    0,
                ),
                (
                    0,
                    -level_width_length / 2,
                    0,
                ),
                (
                    level_width_length / 2,
                    0,
                    level_height,
                ),
                (
                    -level_width_length / 2,
                    0,
                    level_height,
                ),
                (
                    0,
                    level_width_length / 2,
                    level_height,
                ),
                (
                    0,
                    -level_width_length / 2,
                    level_height,
                ),
            ]
        )

        def create_value_sbtn() -> Shape:
            value = min_value + index * value_step
            z = textshape(str(value), "roboto", level_width_length)
            z_bbox = z.bbox()
            x_limit = level_width_length - text_margin * 2
            x_scale = x_limit / z_bbox.xlength()
            y_limit = level_height - text_margin * 2
            y_scale = y_limit / z_bbox.ylength()
            scale_factor = min(x_scale, y_scale)
            z = z.scale(scale_factor)
            z_bbox = z.bbox()
            z = z.translate(
                (- z_bbox.xlength()) / 2,
                (level_height - z_bbox.ylength()) / 2,
            )
            z = z.extrude(text_depth)
            z = z.rotateX(pi / 2)
            z = z.back(level_width_length / 2 - text_depth)
            return z

        z -= create_value_sbtn()

        z = z.up(index * level_height)
        return z

    z = union([create_level(i) for i in range(levels)])
    name = f"{min_value}+{value_step}({levels})"
    return name, z


filename, calibration_tower = create_calibration_tower(
    levels=6,
    min_value=250,
    value_step=5,
)

display(calibration_tower)
export_to_stl(
    shapes={
        filename: calibration_tower,
    },
)
show()
