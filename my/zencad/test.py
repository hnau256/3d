from utils import *


polygon_points = [(30, 0), (90, 0), (120, 60), (60, 115), (0, 60)]
height = 5
z = voronoi_sbtn(
        polygon_points=polygon_points,
        height=height,
        separation=3,
        fillet_radius=3
)
polygon_face = polysegment(polygon_points, closed=True).fill()
z = polygon_face.extrude(height) - z


display(z)
show()
