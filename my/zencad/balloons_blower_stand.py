#!/usr/bin/env python3
# coding: utf-8

from utils import *

# measured
top_out_radius: float = 99.0 / 2
fan_to_top_height: float = 70.0
fan_radius: float = 72 / 2
top_hole_height: float = 20.0
drain_height: float = 15.0
drain_width: float = 24.0

# invented
wall: float = 3.2
bottom_out_radius = 140.0 / 2
bottom_to_fan_height: float = 40.0
shape_top_arm_height: float = 60.0
shape_bottom_arm_height: float = 60.0
rays_count: int = 3
hole_height: float = 90.0
hole_width_fraction: float = 0.9

# calculated
ray_angle: float = pi * 2 / rays_count
height: float = bottom_to_fan_height + fan_to_top_height
ray_height: float = height - top_hole_height


def create_stand():
    def create_base(
            shrinkage: float = 0.0
    ) -> Shape:
        z = [
            segment(
                (0, 0, 0),
                (bottom_out_radius - shrinkage, 0, 0),
            ),
            bezier(
                [
                    (bottom_out_radius - shrinkage, 0, 0),
                    (bottom_out_radius - shrinkage, 0, shape_bottom_arm_height),
                    (top_out_radius - shrinkage, 0, height - shape_top_arm_height),
                    (top_out_radius - shrinkage, 0, height),
                ]
            ),
            segment(
                (top_out_radius - shrinkage, 0, height),
                (0, 0, height),
            ),
            segment(
                (0, 0, height),
                (0, 0, 0),
            ),
        ]
        z = sew(z)
        z = z.fill()
        z = revol(z)
        return z

    z = create_base()

    z = thicksolid(
        proto=z,
        t=-wall,
        refs=[
            (0, 0, 0),
            (0, 0, height),
        ]
    )

    def create_holes():
        length = hole_height

        def calc_width():
            (a_x, a_y) = (bottom_out_radius, 0)
            point_b_angle = ray_angle * hole_width_fraction
            (b_x, b_y) = (
                cos(point_b_angle) * bottom_out_radius,
                sin(point_b_angle) * bottom_out_radius,
            )
            dx = b_x - a_x
            dy = b_y - a_y
            distance = sqrt(dx ** 2 + dy ** 2)
            return distance

        width = calc_width()
        dx = width / 2
        top_square_size = width / 2
        top_square_radius = top_square_size * sqrt(2) / 2
        top_square_y = length - top_square_radius
        center_y = top_square_y - top_square_radius
        z = wire_builder(
            start=(-dx, 0, 0),
        ) \
            .line(dx, 0, 0) \
            .line(dx, center_y, 0) \
            .arc(
            c=(0, center_y, 0),
            r=dx,
            angle=pi / 4,
        ) \
            .line(0, length, 0) \
            .line(-top_square_radius, top_square_y, 0) \
            .arc(
            c=(0, center_y, 0),
            r=dx,
            angle=pi / 4,
        ) \
            .close() \
            .doit()
        z = loft(
            arr=[
                z.scaleX(tech),
                z.up(bottom_out_radius)
            ],
        )
        z = z.rotateX(pi / 2)
        z = z.rotateZ(pi / 2)
        z = union([z.rotateZ(i * ray_angle) for i in range(rays_count)])
        return z

    z -= create_holes()

    def create_rays():
        z = box(
            bottom_out_radius,
            wall,
            ray_height,
        )
        z = z.back(wall / 2)
        z = z.rotateZ(ray_angle / 2)
        z ^= create_base(shrinkage=wall / 2)
        z = union([z.rotateZ(i * ray_angle) for i in range(rays_count)])
        hole = cylinder(
            r=fan_radius,
            h=ray_height - bottom_to_fan_height,
        )
        hole = hole.up(bottom_to_fan_height)
        z -= hole
        return z

    z += create_rays()

    def create_drain():
        z = box(
            drain_width,
            bottom_out_radius,
            drain_height,
        )
        z = z.translate(
            -drain_width / 2,
            0,
            height - drain_height,
        )
        z = z.rotateZ(pi/2)
        return z

    z -= create_drain()

    return z


stand = create_stand()

export_to_stl(
    shapes={
        "stand": stand,
    }
)

disp(stand)
show()
