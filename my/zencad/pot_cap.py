#!/usr/bin/env python3
# coding: utf-8

from utils import *

clearance: float = 0.4
pot_inner_radius: float = 109.7 / 2
pot_wall: float = 1.2
border: float = 2.4
base_height: float = 4.8
dome_height: float = 16
dome_bezier_top_control_point_dx: float = 48
dome_bezier_bottom_control_point_dx: float = 4
dome_bezier_bottom_control_point_dy: float = 3


def create_cap():
    base_radius = pot_inner_radius - clearance

    z = cylinder(
        r=base_radius,
        h=base_height,
    )
    z = z.down(base_height)

    def create_dome():
        out_radius = base_radius + border
        z = sew(
            [
                segment(
                    (0, 0, 0),
                    (out_radius, 0, 0),
                ),
                bezier(
                    [
                        (out_radius, 0, 0),
                        (out_radius + dome_bezier_bottom_control_point_dx, 0, dome_bezier_bottom_control_point_dy),
                        (dome_bezier_top_control_point_dx, 0, dome_height),
                        (0, 0, dome_height),
                    ],
                ),
                segment(
                    (0, 0, dome_height),
                    (0, 0, 0),
                ),
            ]
        )
        z = z.fill()
        z = revol(
            shp=z,
        )
        return z

    z += create_dome()

    z = z.up(clearance)
    return z


def create_pot() -> Shape:
    wall = pot_wall
    in_radius: float = pot_inner_radius
    out_radius: float = in_radius + wall
    height: float = 120
    z = cylinder(
        r=out_radius,
        h=height,
    )
    z -= cylinder(
        r=in_radius,
        h=height - wall,
    ).up(wall)
    z = z.down(height)
    z = z.fillet(
        wall / 2 - tech,
        [
            (0, in_radius, 0),
            (0, out_radius, 0),
        ]
    )
    return z


sbtn = halfspace().rotateX(pi / 2)

pot = create_pot()
disp(pot - sbtn, Color(1, 1, 1, 0.25))

cap = create_cap()
disp(cap - sbtn, Color(1, 0, 1))

export_to_stl(
    shapes={
        "pot_cap": cap,
    },
)
show()
