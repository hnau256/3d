#!/usr/bin/env python3
# coding: utf-8

from dataclasses import dataclass

from utils import *

# Invented
wall: float = 2.4
clearance: float = 0.1
leg_bearing_distance: float = 16
leg_envelopment_length: float = 56
use_parts_section: bool = True
bolt_cap_height: float = 3.2
bolt_wall: float = 3.2
bolt_cap_depth: float = 3.2

# Measured
bearing_length: float = 14.0
bearing_radius: float = 11.0
bearing_inner_radius: float = 4.0
bearing_border: float = 1.5
leg_length: float = 30.0
leg_width: float = 20.0
base_size: float = 41.6
nut_width: float = 6.8
nut_rays_count: int = 6
nut_height: float = 3.1
bolt_radius: float = 3.88 / 2
bolt_cap_radius: float = 5


def calc_nut_radius(
        expansion: float
) -> float:
    return ((nut_width / 2) + expansion) * 2 / sqrt(3)


# Calculated
width: float = max(bearing_radius * 2, leg_width) + (clearance + wall) * 2
bolt_addition_radius: float = bolt_radius + clearance + bolt_wall


@dataclass
class Unit:
    name: str
    parts: dict[str, Shape]
    landmarks: list[Shape]

    def _part_name(self, name: str) -> str:
        return self.name + "_" + name

    def add_to_export(
            self,
            export_dict: dict[str, Shape],
    ):
        for name, part in self.parts.items():
            part_name = self._part_name(name)
            export_dict[part_name] = part

    def disp(self, x_offset: float):
        for name, part in self.parts.items():
            part_name = self._part_name(name)
            if use_parts_section:
                part -= halfspace().rotateY(pi / 2)
            disp(
                part.right(x_offset),
                choose_color_to_display(part_name.__hash__()),
            )

        for landmark in self.landmarks:
            disp(
                landmark.right(x_offset),
                Color(1, 1, 1),
            )


def create_bearing_landmark():
    z = cylinder(
        r=bearing_radius,
        h=bearing_length,
    )
    z -= cylinder(
        r=bearing_inner_radius,
        h=bearing_length,
    )
    sbtn_length = bearing_length * 0.05
    sbtn = cylinder(
        r=bearing_radius - bearing_border,
        h=sbtn_length,
    )
    sbtn -= cylinder(
        r=bearing_inner_radius + bearing_border,
        h=sbtn_length,
    )
    z -= sbtn
    z -= sbtn.up(bearing_length - sbtn_length)
    z = z.rotateX(-pi / 2)
    return z


bearing_landmark = create_bearing_landmark()


def create_bearing_sbtn() -> Shape:
    z = cylinder(
        r=bearing_radius + clearance,
        h=bearing_length + clearance * 2,
    )
    z = z.down(clearance)
    shaft_additional_length: float = 50
    shaft = cylinder(
        r=bearing_radius - bearing_border + clearance,
        h=bearing_length + shaft_additional_length * 2,
    )
    shaft = shaft.down(shaft_additional_length)
    z += shaft
    z = z.rotateX(-pi / 2)
    return z


bearing_sbtn = create_bearing_sbtn()


def split_by_center_of_bearing(
        shape: Shape,
) -> (Shape, Shape):
    center_of_bearing = halfspace() \
        .rotateX(-pi / 2) \
        .forw(bearing_length / 2)
    back = shape - center_of_bearing
    forw = shape ^ center_of_bearing
    return back, forw


@dataclass
class BoltInfo:
    x: float
    z: float
    min_y: float
    min_sbtn_y: float
    max_sbtn_y: float


@dataclass
class BoltResult:
    sbtn: Shape
    landmark: Shape

    def __add__(self, other):
        return BoltResult(
            sbtn=self.sbtn + other.sbtn,
            landmark=self.landmark + other.landmark,
        )


def create_bolts(
        infos: list[BoltInfo],
) -> BoltResult:
    def create_bolt(
            info: BoltInfo,
    ) -> BoltResult:
        length = info.max_sbtn_y - info.min_y - bolt_cap_depth

        def create_shape(
                expansion: float,
                add_cap: bool,
                nut_cap: bool,
        ):
            def create_nut() -> Shape:
                radius = ((nut_width / 2) + expansion) * 2 / sqrt(3)

                def create_shape(
                        scale: float,
                        z: float,
                ) -> Shape:
                    return ngon(
                        r=radius * scale,
                        n=nut_rays_count,
                        wire=True,
                    ).up(z)

                nut_max_z = info.max_sbtn_y - info.min_sbtn_y
                nut_min_z = 0.0 if nut_cap else (nut_max_z - nut_height)

                arr = [
                    create_shape(
                        scale=1,
                        z=nut_min_z,
                    ),
                    create_shape(
                        scale=1,
                        z=nut_max_z + expansion,
                    ),
                ]

                if nut_cap:
                    arr.append(
                        create_shape(
                            scale=tech,
                            z=nut_max_z + radius + expansion,
                        ),
                    )

                z = loft(
                    arr=arr,
                )
                return z

            z = create_nut()

            z += cylinder(
                r=bolt_radius + expansion,
                h=length,
            )

            if not add_cap:
                z += cylinder(
                    r=bolt_cap_radius + expansion,
                    h=bolt_cap_depth,
                ).up(length)

            def create_cap():
                z = sphere(bolt_addition_radius)
                z = z.scaleZ(bolt_cap_height / bolt_addition_radius)
                z -= halfspace()
                z = z.up(length)
                return z

            if add_cap:
                z += create_cap()

            z = z.rotateX(-pi / 2)
            z = z.translate(
                info.x,
                info.min_y,
                info.z,
            )
            return z

        return BoltResult(
            sbtn=create_shape(
                expansion=clearance,
                add_cap=False,
                nut_cap=True,
            ),
            landmark=create_shape(
                expansion=0,
                add_cap=True,
                nut_cap=False,
            ),
        )

    result = BoltResult(
        nullshape(),
        nullshape(),
    )
    for info in infos:
        result += create_bolt(info)

    return result


def create_leg_unit() -> Unit:
    leg_x = leg_width / 2
    leg_sbtn_x = leg_x + clearance
    main_shape_x = width / 2
    top_bolt_x = leg_sbtn_x + bolt_addition_radius
    top_bolt_shape_x = top_bolt_x + bolt_addition_radius

    bearing_min_z = -bearing_radius
    bearing_sbtn_min_z = bearing_min_z - clearance
    min_z = bearing_sbtn_min_z - wall
    bearing_max_z = bearing_radius
    bearing_sbtn_max_z = bearing_max_z + clearance
    leg_min_z = bearing_max_z + leg_bearing_distance
    leg_sbtn_min_z = leg_min_z - clearance
    max_z = leg_min_z + leg_envelopment_length
    top_bolt_z = max_z - bolt_addition_radius

    sbtn_min_y = -clearance
    min_y = sbtn_min_y - wall
    leg_sbtn_max_y = leg_length + clearance
    max_y = leg_sbtn_max_y + wall

    bottom_bolt_offset = main_shape_x - wall + bolt_addition_radius
    bottom_bolt_angle = -pi / 6
    bottom_bolt_x = cos(bottom_bolt_angle) * bottom_bolt_offset
    bottom_bolt_z = sin(bottom_bolt_angle) * bottom_bolt_offset

    def create_main_shape():
        z = box(
            width,
            max_y - min_y,
            max_z - min_z,
        )
        z = z.translate(
            -main_shape_x,
            min_y,
        )
        z = z.fillet(
            main_shape_x - tech,
            [
                (-main_shape_x, 0, 0),
                (main_shape_x, 0, 0),
            ]
        )
        z = z.up(min_z)
        return z

    z = create_main_shape()

    def create_top_bolts_addition() -> (Shape, float, list[(float, float, float)]):
        z = box(
            bolt_addition_radius * 2,
            max_y - min_y - bolt_cap_depth,
            bolt_addition_radius * 2,
        )
        z = z.translate(
            top_bolt_x - bolt_addition_radius,
            min_y,
            top_bolt_z - bolt_addition_radius,
        )
        z = z.fillet(
            bolt_addition_radius - tech,
            [
                (
                    top_bolt_shape_x,
                    0,
                    top_bolt_z + bolt_addition_radius,
                ),
                (
                    top_bolt_shape_x,
                    0,
                    top_bolt_z - bolt_addition_radius,
                ),
            ],
        )
        z += z.mirrorYZ()
        return (
            z,
            top_bolt_x - main_shape_x,
            [
                (
                    main_shape_x,
                    0,
                    top_bolt_z - bolt_addition_radius,
                ),
                (
                    -main_shape_x,
                    0,
                    top_bolt_z - bolt_addition_radius,
                ),
            ]
        )

    (
        top_bolts_addition,
        top_bolts_addition_fillet_radius,
        top_bolts_addition_fillet_points,
    ) = create_top_bolts_addition()

    z += top_bolts_addition

    z = z.fillet(
        top_bolts_addition_fillet_radius,
        top_bolts_addition_fillet_points,
    )

    def create_bottom_bolts_addition() -> (Shape, float, list[(float, float, float)]):
        z = box(
            bottom_bolt_offset,
            max_y - min_y - bolt_cap_depth,
            bolt_addition_radius * 2,
        )
        z = z.translate(
            0,
            min_y,
            - bolt_addition_radius,
        )
        cap = cylinder(
            r=bolt_addition_radius,
            h=max_y - min_y - bolt_cap_depth,
        )
        cap = cap.rotateX(-pi / 2)
        cap = cap.translate(
            bottom_bolt_offset,
            min_y,
            0,
        )
        z += cap
        z = z.rotateY(-bottom_bolt_angle)
        z += z.mirrorYZ()

        def create_point(
                angle: float,
                z_offset: float,
        ) -> (float, float, float):
            z = point3(
                main_shape_x,
                (min_y + max_y) / 2,
                z_offset,
            )
            z = z.rotateY(-angle)
            return z

        return (
            z,
            bottom_bolt_offset - main_shape_x,
            [
                create_point(
                    angle=bottom_bolt_angle,
                    z_offset=bolt_addition_radius,
                ),
                create_point(
                    angle=bottom_bolt_angle,
                    z_offset=-bolt_addition_radius,
                ),
                create_point(
                    angle=pi - bottom_bolt_angle,
                    z_offset=bolt_addition_radius,
                ),
                create_point(
                    angle=pi - bottom_bolt_angle,
                    z_offset=-bolt_addition_radius,
                ),
            ]
        )

    (
        bottom_bolts_addition,
        bottom_bolts_addition_fillet_radius,
        bottom_bolts_addition_fillet_points,
    ) = create_bottom_bolts_addition()

    z += bottom_bolts_addition
    z = z.fillet(
        bottom_bolts_addition_fillet_radius,
        bottom_bolts_addition_fillet_points,
    )

    z -= bearing_sbtn

    def create_leg_sbtn():
        width = leg_width + clearance * 2
        length = leg_length + clearance * 2
        height = leg_envelopment_length + clearance
        z = box(width, length, height)
        z = z.translate(
            -width / 2,
            -clearance,
            bearing_radius + leg_bearing_distance - clearance,
        )
        return z

    z -= create_leg_sbtn()

    bolt_max_sbtn_y = max_y
    bolt_min_sbtn_y = max_y-bearing_length/2

    bolts = create_bolts(
        infos=[
            BoltInfo(
                x=top_bolt_x,
                z=top_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=-top_bolt_x,
                z=top_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=0,
                z=(bearing_sbtn_max_z + leg_sbtn_min_z) / 2,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=bottom_bolt_x,
                z=bottom_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=-bottom_bolt_x,
                z=bottom_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
        ]
    )

    z -= bolts.sbtn
    z = unify(z)

    back, forw = split_by_center_of_bearing(z)

    def create_leg_landmark() -> Shape:
        height = leg_envelopment_length + wall
        min_z = bearing_radius + leg_bearing_distance
        z = box(
            leg_width,
            leg_length,
            height,
        )
        z = z.translate(
            -leg_width / 2,
            0,
            min_z,
        )
        return z

    return Unit(
        name="leg_unit",
        parts={
            "back": back,
            "forward": forw,
        },
        landmarks=[
            bearing_landmark,
            create_leg_landmark(),
            bolts.landmark,
        ],
    )


def create_base_unit():
    bearing_max_z = bearing_radius
    bearing_sbtn_max_z = bearing_max_z + clearance
    max_z = bearing_sbtn_max_z + wall
    bearing_min_z = -bearing_radius
    bearing_sbtn_min_z = bearing_min_z - clearance
    base_sbtn_max_z = bearing_sbtn_min_z - wall
    base_max_z = base_sbtn_max_z - clearance
    base_min_z = base_max_z - base_size
    base_sbtn_min_z = base_min_z - clearance
    min_z = base_sbtn_min_z - wall
    bottom_bolt_z = base_sbtn_min_z - bolt_addition_radius
    top_bolt_z = 0

    base_min_y = 0
    base_sbtn_min_y = base_min_y - clearance
    min_y = base_sbtn_min_y - wall
    base_max_y = base_min_y + base_size
    base_sbtn_max_y = base_max_y + clearance
    max_y = base_sbtn_max_y + wall

    bearing_x = bearing_radius
    bearing_sbtn_x = bearing_x + clearance
    max_x = bearing_sbtn_x + wall
    bottom_bolt_x = width / 2 - bolt_addition_radius
    top_bolt_x = bearing_sbtn_x + bolt_addition_radius

    def create_base(
            expansion: float,
    ) -> Shape:
        landmark_width = width + (wall + expansion) * 2
        z = box(
            landmark_width,
            base_max_y - base_min_y + expansion * 2,
            base_max_z - base_min_z + expansion * 2,
        )
        z = z.translate(
            -landmark_width / 2,
            base_min_y - expansion,
            base_min_z - expansion,
        )
        return z

    def create_main_shape() -> Shape:
        z = box(
            width,
            max_y - min_y,
            max_z - min_z,
        )
        z = z.translate(
            -width / 2,
            min_y,
            min_z,
        )
        z = z.fillet(
            width / 2 - tech,
            [
                (
                    -width / 2,
                    0,
                    max_z,
                ),
                (
                    width / 2,
                    0,
                    max_z,
                ),
            ]
        )
        z -= create_base(
            expansion=clearance,
        )
        return z

    z = create_main_shape()

    def create_top_bolts_addition() -> (Shape, float, list[(float, float, float)]):
        shape_x = top_bolt_x + bolt_addition_radius
        z = box(
            shape_x,
            max_y - min_y - bolt_cap_depth,
            bolt_addition_radius * 2,
        )
        z = z.translate(
            0,
            min_y,
            top_bolt_z - bolt_addition_radius,
        )
        z = z.fillet(
            bolt_addition_radius - tech,
            [
                (
                    shape_x,
                    0,
                    top_bolt_z - bolt_addition_radius,
                ),
                (
                    shape_x,
                    0,
                    top_bolt_z + bolt_addition_radius,
                ),
            ],
        )
        z += z.mirrorYZ()
        return (
            z,
            top_bolt_x - max_x,
            [
                (
                    max_x,
                    (min_y + max_y) / 2,
                    top_bolt_z + bolt_addition_radius,
                ),
                (
                    max_x,
                    (min_y + max_y) / 2,
                    top_bolt_z - bolt_addition_radius,
                ),
                (
                    -max_x,
                    (min_y + max_y) / 2,
                    top_bolt_z + bolt_addition_radius,
                ),
                (
                    -max_x,
                    (min_y + max_y) / 2,
                    top_bolt_z - bolt_addition_radius,
                ),
            ]
        )

    (
        top_bolts_addition,
        top_bolts_addition_fillet_radius,
        top_bolts_addition_fillet_points,
    ) = create_top_bolts_addition()

    z += top_bolts_addition

    z = z.fillet(
        top_bolts_addition_fillet_radius,
        top_bolts_addition_fillet_points,
    )

    def create_bottom_bolts_addition() -> (Shape, float, list[(float, float, float)]):
        z = box(
            bolt_addition_radius * 2,
            max_y - min_y - bolt_cap_depth,
            bolt_addition_radius * 2,
        )
        z = z.translate(
            bottom_bolt_x - bolt_addition_radius,
            min_y,
            bottom_bolt_z - bolt_addition_radius,
        )
        z = z.fillet(
            bolt_addition_radius - tech,
            [
                (
                    bottom_bolt_x - bolt_addition_radius,
                    0,
                    bottom_bolt_z - bolt_addition_radius,
                ),
                (
                    bottom_bolt_x + bolt_addition_radius,
                    0,
                    bottom_bolt_z - bolt_addition_radius,
                ),
            ],
        )
        z += z.mirrorYZ()
        return (
            z,
            min_z - bottom_bolt_z,
            [
                (
                    bottom_bolt_x - bolt_addition_radius,
                    (min_y + max_y) / 2,
                    min_z,
                ),
                (
                    -(bottom_bolt_x - bolt_addition_radius),
                    (min_y + max_y) / 2,
                    min_z,
                ),
            ]
        )

    (
        bottom_bolts_addition,
        bottom_bolts_addition_fillet_radius,
        bottom_bolts_addition_fillet_points,
    ) = create_bottom_bolts_addition()

    z += bottom_bolts_addition

    z = z.fillet(
        bottom_bolts_addition_fillet_radius,
        bottom_bolts_addition_fillet_points,
    )

    z -= bearing_sbtn

    bolt_max_sbtn_y = max_y
    bolt_min_sbtn_y = max_y-bearing_length/2
    bolts = create_bolts(
        infos=[
            BoltInfo(
                x=bottom_bolt_x,
                z=bottom_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=-bottom_bolt_x,
                z=bottom_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=top_bolt_x,
                z=top_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
            BoltInfo(
                x=-top_bolt_x,
                z=top_bolt_z,
                min_y=min_y,
                min_sbtn_y=bolt_min_sbtn_y,
                max_sbtn_y=bolt_max_sbtn_y,
            ),
        ]
    )

    z -= bolts.sbtn
    z = unify(z)

    back, forw = split_by_center_of_bearing(z)

    return Unit(
        name="base_unit",
        parts={
            "back": back,
            "forward": forw,
        },
        landmarks=[
            create_base(
                expansion=0,
            ),
            bearing_landmark,
            bolts.landmark,
        ],
    )


export_dict: dict[str, Shape] = {}

leg_unit = create_leg_unit()
leg_unit.disp(
    x_offset=0.0,
)
leg_unit.add_to_export(export_dict)

base_unit = create_base_unit()
base_unit.disp(
    x_offset=64.0,
)
base_unit.add_to_export(export_dict)

export_to_stl(export_dict)

show()
