from utils import *

hole_radius = 5
row_holes_count = 16
wall = 2.0
holes_distance = hole_radius + wall + hole_radius
text_length = 6
text_depth = 1.2

register_font('utils/roboto.ttf')


def create_holes():
    def create_row(first_number):
        width = holes_distance * (row_holes_count - 1) + (wall + hole_radius) * 2
        length = wall + text_length + wall + hole_radius * 2 + wall
        z = box(width, length, wall)
        z = z.fillet(
                hole_radius + wall,
                [
                    (0, 0, wall / 2),
                    (width, 0, wall / 2)
                ]
        )
        z = z.back(length)

        def create_hole_text_sbtn(number):
            z = cylinder(hole_radius, wall)
            z = z.back(wall + text_length + wall + hole_radius)
            text = textshape(str(number), "roboto", text_length)
            text_bbox = text.bbox()
            text = text.scale(text_length / (text_bbox.ymax - text_bbox.ymin))
            text = text.extrude(text_depth)
            text_bbox = text.bbox()
            text = text.translate(
                    -(text_bbox.xmax - text_bbox.xmin) / 2,
                    -(text_bbox.ymax - text_bbox.ymin) - wall,
                    wall - text_depth
            )
            z += text
            return z

        sbtn_left = wall + hole_radius
        sbtn = union([create_hole_text_sbtn(first_number + i).right(sbtn_left + i * holes_distance) for i in
                      range(row_holes_count)])
        z -= sbtn
        z = z.left(width / 2)
        return z

    z = create_row(1)
    z += create_row(1 + row_holes_count).rotateZ(pi)
    return z


holes = create_holes()

export_to_stl({"holes": holes})
display(holes)
show()
