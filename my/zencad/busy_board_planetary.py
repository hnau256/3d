from utils import *
from involute import *
from time import time
from constructor import *

clearance = 0.1
slip_clearance: float = 0.2
wall = 2.4
planets_count = 3
module = 3.2
branch_width = wall / 2
base_to_gears_separation: float = 1.2
gear_thickness = 9.6
simple = False
planet_screw_radius: float = 4.8
planet_screw_depth: float = 1.2
planet_screw_shrinkage: float = 1.2

# calculated
sun_gear_teeth_count = 24

style = InvoluteStyle(
    module=module,
    pressure_angle=deg2rad(25)
)

sun_gear = style.create_gear(
    teeth_count=sun_gear_teeth_count,
)

planet_gear = style.create_gear(
    teeth_count=12,
)

ring_gear = style.create_gear(
    teeth_count=48,
    addendum_by_module=1.2,
    dedendum_by_module=0.8,
    min_radius_fillet_by_module=0,
)

spiral_radius: float = sun_gear.min_radius - wall / 2
spiral_min_radius: float = 8.0
planet_offset: float = sun_gear.joint_radius + planet_gear.joint_radius
planet_angle: float = pi * 2 / planets_count

planet_screw_cap_radius = planet_screw_radius + clearance + wall
planet_nut_screw_sbtn_radius = planet_screw_radius + clearance * sqrt(2)
planet_nut_radius = planet_nut_screw_sbtn_radius + wall
planet_nut_top_radius = planet_nut_radius + wall + slip_clearance
base_planet_gear_separator_min_radius = planet_nut_radius + clearance
base_planet_gear_separator_max_radius = base_planet_gear_separator_min_radius + wall
base_planet_screw_sbtn_radius = planet_screw_radius + clearance
planet_gear_nut_sbtn_radius = planet_nut_radius + slip_clearance
planet_gear_nut_top_sbtn_radius = planet_nut_top_radius + slip_clearance

base_planet_gear_separator_max_z = 0 - slip_clearance
base_planet_gear_separator_min_z = base_planet_gear_separator_max_z - base_to_gears_separation
base_min_z = base_planet_gear_separator_min_z - wall
planet_screw_cap_max_z = base_min_z - clearance
planet_screw_cap_min_z = planet_screw_cap_max_z - wall
planet_nut_min_z = base_planet_gear_separator_min_z + clearance
planet_nut_top_max_z = gear_thickness
planet_nut_top_min_z = planet_nut_top_max_z - wall
planet_gear_nut_sbtn_min_z = planet_nut_top_min_z - slip_clearance
planet_nut_screw_sbtn_max_z = planet_nut_top_max_z - wall
planet_screw_max_z = planet_nut_screw_sbtn_max_z - slip_clearance - planet_screw_shrinkage


# https://www.thecatalystis.com/gears/
# a - sun gear teeth count
# b - planet gear teeth count
# c - ring gear teeth count
# a + 2*b == c
# (a + c) % planets_count == 0
# a=24, b=12, c=48

def print_variants():
    max_diameter = 200
    max_c = max_diameter / module
    print(max_c)
    for a in range(16, 50):
        for b in range(12, 13):
            for c in range(30, 52):
                if a + 2 * b == c:
                    if (a + c) % 3 == 0:
                        print(f"a={a}, b={b}, c={c}")


# print_variants()

def create_chevron_gear(
        gear: Gear,
) -> Shape:
    height = gear_thickness / 2
    if simple:
        return gear.wire.fill().extrude(
            vec=height * 2,
        )
    total_angle = height / gear.joint_radius * 0.5
    z = loft(
        arr=[
            gear.wire,
            gear.wire.rotateZ(total_angle).up(height),
        ],
        smooth=False,
    )
    z += z.mirrorXY()
    z = z.up(height)
    return z


def create_spiral(
        height: float,
) -> Shape:
    branches_count: int = sun_gear_teeth_count
    branch_angle = pi * 2 / branches_count

    def create_bezier_ray():
        angle = deg2rad(30)
        center_weight: float = 0.75
        z = (
            cos(angle) * spiral_radius,
            sin(angle) * spiral_radius,
            0,
        )
        z = bezier(
            pnts=[
                (
                    spiral_min_radius,
                    0,
                    0,
                ),
                (
                    spiral_min_radius + (spiral_radius - spiral_min_radius) * center_weight,
                    0,
                    0,
                ),
                z,
                z,
            ]
        )
        cut = rectangle(branch_width, height, wire=True)
        cut = cut.rotateX(pi / 2)
        cut = cut.rotateZ(pi / 2)
        cut = cut.right(spiral_min_radius)
        z = pipe_shell(
            arr=[cut],
            spine=z,
        )
        z = z.rotateZ(-angle)
        return z

    def create_simple_ray():
        z = box(
            x=spiral_radius - spiral_min_radius,
            y=branch_width,
            z=height,
        )
        z = z.translate(
            spiral_min_radius,
            -branch_width / 2,
        )
        return z

    z = create_simple_ray() if simple else create_bezier_ray()
    z = union([z.rotateZ(branch_angle * i) for i in range(branches_count)])
    center_circle = circle(spiral_min_radius + wall / 2) - circle(spiral_min_radius - wall / 2)
    center_circle = center_circle.extrude(height)
    z += center_circle
    return z


def create_sun() -> Shape:
    z = create_chevron_gear(sun_gear)
    z -= circle(sun_gear.min_radius - wall).extrude(gear_thickness)
    z += create_spiral(height=gear_thickness)
    z = z.mirrorXZ()
    z = z.rotateZ(sun_gear.tooth_angle / 2)
    return z


def create_base() -> Shape:
    center_out_radius = sun_gear.min_radius
    center_in_radius = center_out_radius - wall
    z = circle(center_out_radius)
    z -= circle(center_in_radius)
    z = z.extrude(wall)
    spiral = create_spiral(wall)
    z += spiral
    z = z.up(base_min_z)

    def create_planet_ray() -> Shape:
        z = build_cylinder(
            min_z=base_min_z,
            max_z=base_planet_gear_separator_max_z,
            radius=base_planet_gear_separator_max_radius,
        )
        z -= build_cylinder(
            min_z=base_planet_gear_separator_min_z,
            max_z=base_planet_gear_separator_max_z,
            radius=base_planet_gear_separator_min_radius,
        )
        z -= build_cylinder(
            min_z=base_min_z,
            max_z=base_planet_gear_separator_min_z,
            radius=base_planet_screw_sbtn_radius,
        )
        z = z.translate(planet_offset)
        return z

    planet_ray = create_planet_ray()
    z += union([planet_ray.rotateZ(i * planet_angle) for i in range(planets_count)])

    return z


def create_planet_screw() -> Shape:
    z = build_screw(
        r=planet_screw_radius,
        min_z=planet_screw_cap_min_z + tech,
        max_z=planet_screw_max_z,
        depth=planet_screw_depth,
        top_side=ScrewSide.CHAMFER,
    )
    z += build_cylinder(
        min_z=planet_screw_cap_min_z,
        max_z=planet_screw_cap_max_z,
        radius=planet_screw_cap_radius,
    )
    z ^= build_box(
        min_x=-planet_screw_cap_radius - tech,
        max_x=planet_screw_cap_radius + tech,
        min_y=-planet_screw_radius + planet_screw_depth + tech,
        max_y=planet_screw_radius - planet_screw_depth - tech,
        min_z=planet_screw_cap_min_z - tech,
        max_z=planet_screw_max_z + tech,
    )
    z = z.right(planet_offset)
    return z


def create_planet_nut() -> Shape:
    z = build_cylinder(
        min_z=planet_nut_min_z,
        max_z=planet_nut_top_max_z - tech,
        radius=planet_nut_radius,
    )
    z += build_cylinder(
        min_z=planet_nut_top_min_z,
        max_z=planet_nut_top_max_z,
        radius=planet_nut_top_radius,
    )
    z -= build_screw(
        r=planet_nut_screw_sbtn_radius,
        max_z=planet_nut_screw_sbtn_max_z,
        min_z=planet_nut_min_z,
        depth=planet_screw_depth,
        bottom_side=ScrewSide.EXTENSION,
    )
    z = z.right(planet_offset)
    return z

def create_planet() -> Shape:
    z = create_chevron_gear(planet_gear)
    z -= build_cylinder(
        min_z=0,
        max_z=gear_thickness,
        radius=planet_gear_nut_sbtn_radius,
    )
    z -= build_cylinder(
        min_z=planet_gear_nut_sbtn_min_z,
        max_z=gear_thickness,
        radius=planet_gear_nut_top_sbtn_radius,
    )
    z = z.right(planet_offset)
    return z


def create_ring() -> Shape:
    z = circle(ring_gear.max_radius + wall).extrude(gear_thickness)
    z -= create_chevron_gear(ring_gear)
    return z


base = create_base()
sun = create_sun()
planet = create_planet()
planet_screw = create_planet_screw()
planet_nut = create_planet_nut()
ring = create_ring()

use_section = True

def disp_use_section(
        shape: Shape,
        color: Color,
):
    z = shape
    bbox = z.boundbox()
    if use_section:
        z ^= halfspace() \
            .rotateX(pi / 2) \
            .translate(
            (bbox.xmin + bbox.xmax) / 2,
            (bbox.ymin + bbox.ymax) / 2,
            (bbox.zmin + bbox.zmax) / 2,
        )
    return disp(z, color)


disp_use_section(
    base,
    choose_color_to_display(0),
)
sun_mediator = disp_use_section(
    sun,
    choose_color_to_display(5),
)

planets = [planet.rotateZ(i * planet_angle) for i in range(planets_count)]
planet_mediators = [disp_use_section(planet, choose_color_to_display(9)) for planet in planets]

for i in range(planets_count):
    disp_use_section(planet_screw.rotateZ(i * planet_angle), choose_color_to_display(2))

for i in range(planets_count):
    disp_use_section(planet_nut.rotateZ(i * planet_angle), choose_color_to_display(1))

ring_mediator = disp_use_section(
    ring,
    choose_color_to_display(13),
)

startTime = time()


def animate(_):
    teeth_rotation = (time() - startTime) * 0.75

    sun_transformation = \
        rotateZ(teeth_rotation * sun_gear.tooth_angle)
    sun_mediator.relocate(sun_transformation)

    for i in range(planets_count):
        planet_transformation = \
            rotateZ(i * planet_angle) \
            * right(planet_offset) \
            * rotateZ(-teeth_rotation * planet_gear.tooth_angle) \
            * left(planet_offset) \
            * rotateZ(-i * planet_angle)
        planet_mediators[i].relocate(planet_transformation)

    ring_transformation = \
        rotateZ(-teeth_rotation * ring_gear.tooth_angle)
    ring_mediator.relocate(ring_transformation)


show(animate=None if use_section else animate)
