from utils import *

# measured
leg_center_height: float = 13.19
leg_side_height: float = 7.66
leg_middle_height: float = 12.38
leg_radius: float = 35.98 / 2
leg_handle_radius: float = 8

# invented
wall: float = 1.2
clearance: float = 0.4
additional_height: float = 16.0
screw_depth: float = 1.2
top_chamfer: float = 7.2
handle_depth: float = 4.8
handle_width: float = 2.4

istn = halfspace().rotateX(pi / 2)


def create_leg() -> Shape:
    leg_length: float = 20.0
    z = sew(
        [
            circle_arc(
                (0, 0, leg_center_height),
                (leg_radius / 2, 0, leg_middle_height),
                (leg_radius, 0, leg_side_height),
            ),
            segment(
                (leg_radius, 0, leg_side_height),
                (leg_radius, 0, 0),
            ),
            segment(
                (leg_radius, 0, 0),
                (0, 0, 0),
            ),
            segment(
                (0, 0, 0),
                (0, 0, leg_center_height),
            )
        ]
    )
    z = fill(z)
    z = revol(z)
    z += cylinder(
        r=leg_handle_radius,
        h=leg_center_height + leg_length,
    ).up(tech)

    return z


leg = create_leg()
disp(leg ^ istn, Color(1, 1, 1, 0.5))


def create_top_with_bottom() -> (Shape, Shape):
    in_radius = leg_radius + clearance
    z_offset = additional_height
    in_height = z_offset + clearance + leg_center_height + clearance
    out_bottom_radius = in_radius + screw_depth
    screw_sbtn_radius = out_bottom_radius + clearance * sqrt(2)
    out_top_radius = screw_sbtn_radius + wall
    out_height = in_height + wall
    top = cylinder(
        r=out_top_radius,
        h=out_height
    )
    top = chamfer(
        top,
        top_chamfer,
        [(0, out_top_radius, out_height)]
    )
    top -= screw(
        r=screw_sbtn_radius,
        h=additional_height,
        depth=screw_depth,
    )
    top -= cone(
        r1=screw_sbtn_radius,
        r2=screw_sbtn_radius - screw_depth,
        h=screw_depth,
    )
    top = top.down(z_offset)
    top -= make_solid(offset(leg, clearance))

    bottom = screw(
        r=out_bottom_radius,
        h=additional_height,
    )
    bottom_istn = cone(
        r1=out_bottom_radius - screw_depth,
        r2=out_bottom_radius - screw_depth+additional_height/2,
        h=additional_height/2,
    )
    bottom_istn += bottom_istn.mirrorXY().up(additional_height)
    bottom ^= bottom_istn
    handle_length = (out_bottom_radius + clearance) * 2
    handle_sbtn = box(
        handle_width,
        handle_length,
        handle_depth,
    )
    handle_sbtn = handle_sbtn.translate(
        -handle_width/2,
        -handle_length/2
    )
    bottom -= handle_sbtn
    bottom = bottom.down(z_offset)
    return top, bottom


top, bottom = create_top_with_bottom()
disp(top ^ istn)
disp(bottom ^ istn)

export_to_stl(
    shapes={
        "top": top.mirrorXY(),
        "bottom": bottom.mirrorXY(),
    }
)

show()
