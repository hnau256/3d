#!/usr/bin/env python3
# coding: utf-8

from opensimplex import *

from utils import *

register_font('utils/roboto.ttf')
seed(2)

clearance: float = 0.2

wall: float = 2.4
batteries_distance: float = 0.8
screw_depth: float = 1.6
screw_length: float = 9.6
height_shrinkage: float = 0.8
battery_hold_percentage: float = 0.75
title_depth: float = 0.8
out_pattern_depth: float = 0.4
out_pattern_segment_length: float = 0.4
out_pattern_smoothing_length = 4.8


def create_cylinder_shape(
        radius: float,
        height: float,
        offset: float,
) -> Shape:
    def calc_pattern(
            angle: float,
            height: float,
    ) -> float:
        scale = 1.5
        x = cos(angle) * scale
        y = sin(angle) * scale
        z = height * scale + offset
        a = noise3(x, y, z)
        waves = 30
        a = sin(a * waves)
        return a

    z_steps: int = int(ceil(height / out_pattern_segment_length))
    angle_steps: int = int(ceil(radius * 2 * pi / out_pattern_segment_length))
    step_offset: float = 2 * pi / angle_steps

    points: list[tuple[float, float, float]] = []

    for iz in range(z_steps):
        z = iz * step_offset
        z_percentage = float(iz) / (z_steps - 1)
        pz = z_percentage * height
        ia_offset: float = 0 if iz % 2 == 0 else 0.5

        radius_factor = 1
        if pz < out_pattern_smoothing_length:
            radius_factor = pz / out_pattern_smoothing_length
        if pz > height - out_pattern_smoothing_length:
            radius_factor = (height - pz) / out_pattern_smoothing_length

        radius_factor = sqrt(1 - (radius_factor - 1) ** 2)

        for ia in range(angle_steps):
            a = (ia + ia_offset) * step_offset
            radius_percentage = calc_pattern(a, z) * radius_factor
            r = radius + radius_percentage * out_pattern_depth
            px = cos(a) * r
            py = sin(a) * r
            points.append((px, py, pz))

    points.append((0, 0, 0))
    bottom_center_index = z_steps * angle_steps
    points.append((0, 0, height))
    top_center_index = bottom_center_index + 1

    faces: list[tuple[int, int, int]] = []

    def calc_point_index(
            iz: int,
            ia: int,
    ) -> int:
        iz = iz % z_steps
        ia = ia % angle_steps
        i = iz * angle_steps
        i += ia
        return i

    for ia in range(angle_steps):
        faces.append(
            (
                bottom_center_index,
                calc_point_index(0, ia + 1),
                calc_point_index(0, ia),
            ),
        )
        faces.append(
            (
                top_center_index,
                calc_point_index(z_steps - 1, ia),
                calc_point_index(z_steps - 1, ia + 1),
            ),
        )

    for iz in range(z_steps - 1):
        if iz % 2 == 0:
            for ia in range(angle_steps):
                faces.append(
                    (
                        calc_point_index(iz, ia),
                        calc_point_index(iz, ia + 1),
                        calc_point_index(iz + 1, ia),
                    ),
                )
                faces.append(
                    (
                        calc_point_index(iz, ia + 1),
                        calc_point_index(iz + 1, ia + 1),
                        calc_point_index(iz + 1, ia),
                    ),
                )
        else:
            for ia in range(angle_steps):
                faces.append(
                    (
                        calc_point_index(iz, ia),
                        calc_point_index(iz + 1, ia + 1),
                        calc_point_index(iz + 1, ia),
                    ),
                )
                faces.append(
                    (
                        calc_point_index(iz, ia),
                        calc_point_index(iz, ia + 1),
                        calc_point_index(iz + 1, ia + 1),
                    ),
                )

    z = polyhedron(points, faces)
    z = unify(z)
    return z


# disp(create_cylinder_shape(
#     radius=40,
#     height=60,
# ))
# show()


def create_holder(
        battery_radius: float,
        battery_height: float,
        layers: int,
        title: str,
        title_size: float = 24,
) -> tuple[Shape, Shape, list[Shape]]:
    batteries_dx: float = battery_radius + clearance + batteries_distance + clearance + battery_radius

    def create_batteries_centers() -> list[tuple[float, float]]:
        batteries_dy: float = batteries_dx * sqrt(3) / 2
        z: list[tuple[float, float]] = []

        for iy in range(layers):
            x_count = layers * 2 - 1 - iy
            dx = (x_count - 1) / 2 * batteries_dx
            y = iy * batteries_dy
            for ix in range(x_count):
                x = ix * batteries_dx - dx
                z.append((x, y))
                if iy > 0:
                    z.append((x, -y))

        return z

    batteries_centers = create_batteries_centers()
    print(f"Batteries count: {len(batteries_centers)}")

    battery_sbtn_min_z = wall
    battery_min_z = battery_sbtn_min_z + clearance
    battery_hold_max_z = battery_min_z + battery_height * battery_hold_percentage
    screw_min_z = battery_hold_max_z - screw_length
    battery_max_z = battery_min_z + battery_height
    battery_sbtn_max_z = battery_max_z + clearance
    title_min_z = battery_sbtn_max_z + wall
    max_z = title_min_z + title_depth

    def create_batteries(
            expansion: float = 0,
    ):
        result = cylinder(battery_radius + expansion, expansion + battery_height + expansion)
        result = result.up(battery_min_z - expansion)
        result = union([result.translate(offset[0], offset[1]) for offset in batteries_centers])
        return result

    batteries = create_batteries()

    batteries_sbtn_radius = batteries_dx * (layers - 1) + battery_radius + clearance
    screw_in_radius = batteries_sbtn_radius + wall + screw_depth
    screw_out_radius = screw_in_radius + clearance * sqrt(2)
    out_radius = screw_out_radius + wall

    def create_out_shape(
            height: float,
            offset: float,
    ) -> Shape:
        z = create_cylinder_shape(out_radius, height, offset)
        return z

    def create_bottom() -> Shape:
        z = create_out_shape(
            height=screw_min_z,
            offset=0,
        )

        def create_in_screw() -> Shape:
            z = screw(
                r=screw_in_radius,
                h=screw_length + 1,
                depth=screw_depth,
            )
            istn_radius = screw_in_radius + screw_length - screw_depth
            z ^= cone(istn_radius, 0, istn_radius)
            z = z.up(screw_min_z - 1)
            return z

        z += create_in_screw()
        z -= create_batteries(expansion=clearance)

        def create_fillet_points() -> list[tuple[float, float, float]]:
            z: list[tuple[float, float, float]] = []
            radius = battery_radius + clearance
            for (x, y) in batteries_centers:
                z.append((x, y + radius, battery_hold_max_z))

            return z

        z = z.fillet(batteries_distance / 3, create_fillet_points())

        return z

    bottom = create_bottom()

    def create_top() -> Shape:
        height = max_z - screw_min_z - height_shrinkage
        z = create_out_shape(
            height=height,
            offset=screw_min_z,
        )

        def create_title() -> Shape:
            z = textshape(title, "roboto", title_size)
            z = z.extrude(title_depth + tech)
            z_bbox = z.boundbox()
            z = z.translate(
                -z_bbox.xlength() / 2,
                -z_bbox.ylength() / 2,
            )
            z = z.up(height - title_depth)
            return z

        z -= create_title()

        def create_out_screw() -> Shape:
            z = screw(
                r=screw_out_radius,
                h=screw_length,
                depth=screw_depth,
            )
            sbtn_radius = screw_out_radius + tech
            sbtn_height = screw_depth + tech
            z += cone(
                sbtn_radius,
                sbtn_radius - sbtn_height,
                sbtn_height
            )
            return z

        z -= create_out_screw()

        z -= cylinder(
            r=batteries_sbtn_radius,
            h=height - wall - title_depth,
        )

        z = z.up(screw_min_z)
        return z

    top = create_top()

    return bottom, top, batteries


holder = create_holder(
    battery_radius=14.5 / 2,
    battery_height=50.5,
    title="A A",
    layers=3,
)


def display_holder(
        holder: tuple[Shape, Shape, list[Shape]],
):
    holder_bottom, holder_top, batteries = holder
    sbtn = halfspace().rotateX(pi / 2)
    disp(holder_bottom - sbtn, Color(1, 1, 0))
    disp(batteries.up(30), Color(1, 1, 1, 0.25))
    disp(holder_top.up(50) - sbtn, Color(1, 0, 1))


display_holder(holder)


def export_holder(
        holder: tuple[Shape, Shape, list[Shape]]
):
    holder_bottom, holder_top, _ = holder
    export_to_stl(
        shapes={
            "bottom": holder_bottom,
            "top": holder_top.rotateX(pi),
        }
    )


export_holder(holder)

show()
