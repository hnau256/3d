#!/usr/bin/env python3
# coding: utf-8

from utils import *

clearance = 0.2

branch_line_width: float = 4.8
branch_line_height: float = 2.4
branches_on_level: int = 3
branch_y_offset: float = 2.4
branch_segment_length: float = 6
branch_reverse_segments: bool = True
branch_handle_wall = 0.4
branch_handle_adapter_wall = 3.2
branch_handle_adapter_shrinkage = -0.2

level_trunk_top_wall: float = 0.2
level_trunk_wall: float = 0.4
level_trunk_radius_wall: float = 0.8
trunk_radius: float = 3.6
trunk_screw_depth: float = 1.2
trunk_screw_length: float = 14
trunk_shrinkage: float = 0.8

star_wall_percentage: float = 0.7
star_rays_count: int = 8
star_radius: float = 15
star_min_radius_factor: float = 0.8
star_skip_radius_factor: float = 0.55
star_z_offset: float = -0.8
star_base_cap_z_offset: float = 9.6

# calculated
branch_handle_min_radius: float = trunk_radius + clearance
branch_handle_max_radius: float = branch_handle_min_radius + branch_handle_wall
level_trunk_inner_radius: float = branch_handle_max_radius + clearance
level_trunk_min_outer_radius: float = level_trunk_inner_radius + level_trunk_wall
branch_width_angle: float = pi * 2 / branches_on_level

trunk_width = trunk_radius * 2 / sqrt(2)
trunk_length = trunk_width - trunk_screw_depth * 2


def create_level_trunk_sbtn(
        level_height: float,
) -> Shape:
    z = box(
        trunk_width + clearance * 2,
        trunk_length + clearance * 2,
        level_height + tech * 2,
    )
    z = z.translate(
        -trunk_width / 2 - clearance,
        -trunk_length / 2 - clearance,
        -tech
    )
    return z


def calc_branch_max_z(
        level_height: float,
        branch_incline_angle: float,
) -> float:
    x1: float = level_trunk_inner_radius
    y1: float = 0
    x2: float = cos(branch_width_angle) * level_trunk_inner_radius
    y2: float = sin(branch_width_angle) * level_trunk_inner_radius
    length: float = sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    incline_z_offset: float = tan(branch_incline_angle) * length / 2
    result: float = level_height - incline_z_offset - level_trunk_top_wall
    return result


def create_branch_handle_base(
        level_height: float,
        adapter_radius: float,
        shrinkage: float,
        branch_incline_angle: float,
) -> Shape:
    height = level_height - level_trunk_top_wall - shrinkage
    z = cylinder(
        r=branch_handle_max_radius - shrinkage,
        h=height,
    )
    adapter_wall = branch_handle_adapter_wall + clearance * 2 - shrinkage * 2
    adapter = box(
        adapter_wall,
        adapter_radius,
        height,
    )
    adapter = adapter.left(adapter_wall / 2)
    z += adapter
    sbtn = halfspace().rotateY(pi / 2).right(shrinkage + level_trunk_radius_wall / 2).rotateZ(branch_width_angle / 2)
    z -= sbtn
    z -= sbtn.mirrorYZ()
    z -= halfspace() \
        .rotateY(branch_incline_angle + pi) \
        .up(calc_branch_max_z(level_height, branch_incline_angle) - shrinkage)
    return z


def create_level_trunk(
        level_height: float,
        z_angle: float,
        max_radius: float,
        min_radius: float,
        branch_incline_angle: float,
) -> Shape:
    z = cone(
        r1=max_radius,
        r2=min_radius,
        h=level_height,
    )

    sbtn = create_branch_handle_base(
        level_height=level_height,
        adapter_radius=max_radius + tech,
        shrinkage=0,
        branch_incline_angle=branch_incline_angle,
    )
    sbtn = union([sbtn.rotateZ(i * branch_width_angle) for i in range(branches_on_level)])
    z -= sbtn
    z = z.rotateZ(z_angle)
    z -= create_level_trunk_sbtn(level_height)
    return z


def create_branch(
        random: Random,
        level_height: float,
        z_angle: float,
        adapter_radius: float,
        length: float,
        branch_incline_angle: float,
) -> Shape:
    branch_max_z = calc_branch_max_z(
        level_height=level_height,
        branch_incline_angle=branch_incline_angle,
    )

    level_trunk_radius = adapter_radius
    adapter_radius -= branch_handle_adapter_shrinkage

    segments_count: int = ceil(length / branch_segment_length)

    def generate_segments():
        result = []
        next_points = []

        available_neighbour_deltas = [
            (1, 0),
            (0, 1),
        ]

        if branch_reverse_segments:
            available_neighbour_deltas.append((-1, 0))
            available_neighbour_deltas.append((0, -1))

        def generate_neighbours(
                point: tuple[int, int],
                reverse: bool = False,
        ) -> list[tuple[int, int]]:

            def check_point(
                    point: tuple[int, int],
            ) -> bool:
                x, y = point
                if x < 0 or y < 0:
                    return False
                if x + y + 1 > segments_count:
                    return False
                return True

            x, y = point
            result = []
            for delta in available_neighbour_deltas:
                neighbour = (
                    x - delta[0],
                    y - delta[1],
                ) if reverse else (
                    x + delta[0],
                    y + delta[1],
                )
                if check_point(neighbour):
                    result.append(neighbour)

            random.shuffle(result)
            return result

        def check_is_point_used(
                point: tuple[int, int],
        ) -> bool:
            if point[0] == 0 and point[1] == 0:
                return True
            for (_, segment_end) in result:
                if segment_end == point:
                    return True
            return False

        def add_point_neighbours_to_next_points(
                point: tuple[int, int],
        ):
            nonlocal next_points
            for neighbour in generate_neighbours(point):
                if not check_is_point_used(neighbour):
                    next_points.append(neighbour)

            next_points = list(set(next_points))
            random.shuffle(next_points)

        add_point_neighbours_to_next_points((0, 0))

        while len(next_points) > 0:
            point = next_points[0]
            next_points.remove(point)

            def add_to_result():
                for neighbour in generate_neighbours(
                        point=point,
                        reverse=True,
                ):
                    if check_is_point_used(neighbour):
                        result.append((neighbour, point))
                        return

            add_to_result()
            add_point_neighbours_to_next_points(point)

        return result

    segments = generate_segments()

    def indexes_to_point(p):
        # return p
        a, b = p
        r = length * (a + b) / (segments_count - 1)
        an = 0
        if a + b > 0:
            an = b / (a + b)
        an = pi / 2 - branch_width_angle + an * branch_width_angle
        x = r * cos(an)
        y = r * sin(an)
        return x, y

    def create_line(
            start: tuple[float, float],
            end: tuple[float, float],
    ):
        radius = branch_line_width / 2
        x1, y1 = start
        x2, y2 = end
        dx = x2 - x1
        dy = y2 - y1
        if dx == 0 and dy == 0:
            return nullshape()
        nx, ny = (-dy / dx, 1) if dx != 0 else (1, -dx / dy)
        length = sqrt(nx * nx + ny * ny)
        nx = nx * radius / length
        ny = ny * radius / length
        z = polysegment(
            [
                (x1 + nx, y1 + ny),
                (x2 + nx, y2 + ny),
                (x2 - nx, y2 - ny),
                (x1 - nx, y1 - ny)
            ],
            closed=True
        )
        z = z.fill()
        return z

    z = [circle(branch_line_width / 2)]
    for s in segments:
        end = indexes_to_point(s[1])
        z.append(
            create_line(
                start=indexes_to_point(s[0]),
                end=end,
            )
        )

        z.append(circle(branch_line_width / 2).translate(end[0], end[1]))

    z = union(z)
    z = z.extrude(branch_line_height)
    z = z.rotateZ(branch_width_angle / 2)
    z = z.down(branch_line_height / 2)
    z = z.rotateY(branch_incline_angle)
    z = z.down(branch_line_height / 2 / cos(branch_incline_angle))
    z = z.up(branch_max_z - clearance)

    z = z.forw(adapter_radius + branch_y_offset)
    z = z.rotateZ(z_angle)

    def create_branch_handle():
        rotate_x_factor = 1 / cos(branch_incline_angle)
        circle_offset: float = sin(branch_incline_angle) * (branch_line_height / 2)

        circle_radius = branch_line_width / 2

        adapter_width = branch_handle_adapter_wall
        handle_to_branch_dx = (adapter_width - branch_line_width) / 2

        def create_branch_wire(
                circle_offset: float,
        ) -> Shape:
            z = wire_builder(
                start=(-adapter_width / 2, adapter_radius - tech),
                defrel=True,
            ) \
                .line((adapter_width, 0)) \
                .line((-handle_to_branch_dx + circle_offset, branch_y_offset + tech)) \
                .arc_by_points(
                a=(-circle_radius, circle_radius),
                b=(-circle_radius * 2, 0),
            ) \
                .close() \
                .doit()
            z = z.scaleX(rotate_x_factor)
            z = z.rotateY(branch_incline_angle)
            return z

        z = loft(
            arr=[
                create_branch_wire(
                    circle_offset=circle_offset,
                ).up(branch_max_z - clearance),
                create_branch_wire(
                    circle_offset=-circle_offset,
                ).up(branch_max_z - branch_line_height / cos(branch_incline_angle) - clearance),
                polysegment(
                    pnts=[
                        (-adapter_width / 2, adapter_radius - tech),
                        (adapter_width / 2, adapter_radius - tech),
                        (adapter_width / 2, adapter_radius),
                        (-adapter_width / 2, adapter_radius),
                    ],
                    closed=True,
                )
            ],
            smooth=True,
        )

        z += create_branch_handle_base(
            level_height=level_height,
            adapter_radius=adapter_radius,
            shrinkage=clearance,
            branch_incline_angle=branch_incline_angle,
        )

        z = z.rotateZ(z_angle)
        z -= create_level_trunk_sbtn(level_height)

        return z

    z += create_branch_handle()

    return z


def create_trunk_screw_sbtn(
        cap: bool = False,
) -> Shape:
    height = trunk_screw_length + clearance
    radius = trunk_width / 2 + clearance * sqrt(2)
    z = screw(
        r=radius,
        h=height,
        depth=trunk_screw_depth,
    )
    z += cone(
        r1=radius,
        r2=0,
        h=radius,
    )
    if cap:
        z += cone(
            r1=radius,
            r2=0,
            h=radius,
        ).up(height)
    return z


def create_tree(
        random: Random,
        count: int = 12,
        level_z_angle: float = -pi / 8,
        trunk_radius_delta: float = 2.4,
        max_radius: float = 45,
        min_radius: float = branch_segment_length + 1,
        max_level_height: float = 12,
        min_level_height: float = 8,
        pedestal_height: float = 16,
        pedestal_radius: float = 32,
        bottom_branch_incline_angle: float = pi / 12,
        top_branch_incline_angle: float = pi / 8,
) -> tuple[Shape, Shape, Shape, list[tuple[Shape, list[tuple[Shape, float, float]]]]]:
    def create_levels() -> list[tuple[Shape, list[tuple[Shape, float, float]]]]:
        max_trunk_radius: float = level_trunk_min_outer_radius + trunk_radius_delta
        trunk_radius_step = trunk_radius_delta / count
        level_height_step = (max_level_height - min_level_height) / (count - 1)
        branch_incline_angle_step = (top_branch_incline_angle - bottom_branch_incline_angle) / (count - 1)

        level_z = 0

        def create_level(
                i: int,
        ) -> tuple[Shape, list[tuple[Shape, float, float]]]:
            nonlocal level_z

            level_height = max_level_height - level_height_step * i
            trunk_radius = max_trunk_radius - trunk_radius_step * i
            z_angle = level_z_angle * i
            branch_incline_angle = bottom_branch_incline_angle + branch_incline_angle_step * i

            level_trunk = create_level_trunk(
                level_height=level_height,
                z_angle=z_angle,
                max_radius=trunk_radius,
                min_radius=trunk_radius - trunk_radius_step,
                branch_incline_angle=branch_incline_angle,
            ).up(level_z)

            level_percentage = 0 if count <= 1 else i / (count - 1)
            radius_percentage = level_percentage ** 1.25
            radius = max_radius + (min_radius - max_radius) * radius_percentage

            def create_level_branch(
                    j: int,
            ) -> tuple[Shape, float, float]:
                angle = z_angle + j * branch_width_angle
                branch = create_branch(
                    level_height=level_height,
                    length=radius,
                    random=random,
                    z_angle=angle,
                    adapter_radius=trunk_radius,
                    branch_incline_angle=branch_incline_angle,
                ).up(level_z)
                return branch, angle, branch_incline_angle

            branches = [create_level_branch(j) for j in range(branches_on_level)]

            level_z += level_height

            return level_trunk, branches

        return [create_level(i) for i in range(count)]

    levels = create_levels()
    levels_height = (max_level_height + min_level_height) / 2 * count

    def create_star_shape(
            length: float,
            size_percentage: float,
    ) -> Shape:
        points_count = star_rays_count * 2

        max_radius = star_radius * size_percentage
        min_radius = star_radius * star_min_radius_factor * size_percentage
        skip_radius = star_radius * star_skip_radius_factor * size_percentage
        point_angle_step = pi * 2 / points_count

        def create_point(
                i: int,
        ) -> tuple[float, float]:
            radius = skip_radius
            if i % 2 == 0:
                ray = i / 2
                radius = max_radius if ray % 2 == 0 else min_radius
            angle = i * point_angle_step
            x = cos(angle) * radius
            y = sin(angle) * radius
            return x, y

        z = [create_point(i) for i in range(points_count)]
        z = polysegment(
            pnts=z,
            closed=True,
        )
        z = z.fill()
        z = z.extrude(length)
        z = z.down(length / 2)
        z = z.rotateX(pi / 2)
        z = z.up(star_radius + star_z_offset)
        z -= halfspace()
        z = z.up(levels_height)
        return z

    star_center_sbtn = create_star_shape(
        length=(level_trunk_min_outer_radius + tech) * 2,
        size_percentage=star_wall_percentage,
    )

    trunk_above_levels = max(0.0, star_z_offset)
    trunk_above_levels += star_radius * (1 - star_wall_percentage)

    def create_trunk() -> Shape:
        screw_length = trunk_screw_length + clearance
        screw_radius = trunk_width / 2
        z = screw(
            r=screw_radius,
            h=screw_length,
            depth=trunk_screw_depth,
        )
        screw_cut_height = screw_length + screw_radius - trunk_screw_depth
        z ^= cone(
            r1=0,
            r2=screw_cut_height,
            h=screw_cut_height + tech,
        ).down(screw_cut_height - screw_length)
        z ^= box(
            trunk_width + tech * 2,
            trunk_length,
            screw_length + tech * 2,
        ).translate(
            -trunk_width / 2 - tech,
            -trunk_length / 2,
            -tech,
        )
        z = z.down(trunk_screw_length - trunk_shrinkage)
        z += box(
            trunk_width,
            trunk_length,
            levels_height - clearance - trunk_shrinkage + trunk_above_levels,
        ).translate(
            -trunk_width / 2,
            -trunk_length / 2,
            clearance + trunk_shrinkage,
        )
        z += create_star_shape(
            length=trunk_length,
            size_percentage=1,
        )
        z -= star_center_sbtn
        return z

    trunk = create_trunk()

    def create_star_base():
        height = star_radius + star_z_offset + star_base_cap_z_offset
        z = cone(level_trunk_min_outer_radius, 0, height)
        z = z.up(levels_height)
        z -= star_center_sbtn
        z -= create_star_shape(
            length=trunk_length + clearance * 2,
            size_percentage=1 + (clearance / star_radius) * sqrt(2),
        )

        def create_trunk_sbtn():
            width = trunk_width + clearance * 2
            length = trunk_length + clearance * 2
            height = trunk_above_levels
            z = box(width, length, height)
            z = z.translate(-width / 2, -length / 2, levels_height)
            return z

        z -= create_trunk_sbtn()
        return z

    star_base = create_star_base()

    def create_pedestal():
        top_radius = level_trunk_min_outer_radius + trunk_radius_delta
        bottom_radius = pedestal_radius
        z = sew(
            [
                segment(
                    (0, 0),
                    (bottom_radius, 0),
                ),
                bezier(
                    [
                        (bottom_radius, 0),
                        (bottom_radius, pedestal_height * 0.5),
                        (top_radius, pedestal_height - pedestal_height * 0.5),
                        (top_radius, pedestal_height),
                    ],
                ),
                segment(
                    (top_radius, pedestal_height),
                    (0, pedestal_height),
                ),
            ]
        )
        z = z.fill()
        z = z.rotateX(pi / 2)
        z = revol(z)
        z = z.down(pedestal_height)
        z -= create_trunk_screw_sbtn().rotateX(pi)
        return z

    pedestal = create_pedestal()

    return pedestal, trunk, star_base, levels


def display_tree(
        tree: tuple[Shape, Shape, Shape, list[tuple[Shape, list[tuple[Shape, float, float]]]]],
):
    pedestal, trunk, star_base, levels = tree
    display(star_base, Color(1, 1, 1))
    display(pedestal - halfspace().rotateX(pi/2), Color(1, 1, 1))
    display(trunk, Color(0.5, 0.5, 0.5))

    first_level = 0
    levels_to_display = len(levels)
    lastLevel = first_level + levels_to_display - 1
    for i in range(first_level, lastLevel + 1):
        level_trunk, branches = levels[i]
        color = choose_color_to_display(i)
        display(level_trunk, color)
        for branch, _, _ in branches:
            display(branch, color)


shapes_to_export: dict[str, Shape] = {}


def export_tree(
        tree: tuple[Shape, Shape, Shape, list[tuple[Shape, list[tuple[Shape, float, float]]]]],
):
    pedestal, trunk, star_base, levels = tree

    shapes_to_export["pedestal"] = pedestal
    shapes_to_export["trunk"] = trunk.rotateX(pi / 2)
    shapes_to_export["star_base"] = star_base

    for i in range(len(levels)):
        prefix = f"level_{i}_"
        level_trunk, branches = levels[i]
        shapes_to_export[f"{prefix}trunk"] = level_trunk.rotateX(pi)

        for j in range(len(branches)):
            branch, z_angle, branch_incline_angle = branches[j]
            branch = branch.rotateZ(-z_angle)
            branch = branch.rotateY(-branch_incline_angle + pi)
            shapes_to_export[f"{prefix}branch_{j}"] = branch


random = Random(1)
tree_parts = create_tree(
    random=random,
)

display_tree(tree_parts)
export_tree(tree_parts)

if len(shapes_to_export) > 0:
    export_to_stl(
        shapes=shapes_to_export,
        delta=0.003,
    )

show()
