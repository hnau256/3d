from utils import *

wall = 5.2
screws_distance = 24
screw_min_radius = 2.5
screw_max_radius = 5


def create_plane():
    size = screws_distance + (screw_max_radius + wall) * 2
    z = box(size, size, wall)
    z = z.translate(-size / 2, -size / 2)
    z = z.fillet(
            screw_max_radius + wall,
            [
                (size / 2, size / 2, wall / 2),
                (size / 2, -size / 2, wall / 2),
                (-size / 2, size / 2, wall / 2),
                (-size / 2, -size / 2, wall / 2)
            ]
    )
    screw_sbtn = cylinder(screw_min_radius, wall)
    screw_sbtn_top = cone(0, screw_max_radius, screw_max_radius)
    screw_sbtn_top = screw_sbtn_top.up(wall-screw_max_radius)
    screw_sbtn += screw_sbtn_top
    screw_sbtn = screw_sbtn.translate(screws_distance / 2, screws_distance / 2)
    screw_sbtn += screw_sbtn.mirrorXZ()
    screw_sbtn += screw_sbtn.mirrorYZ()
    z -= screw_sbtn
    return z


plane = create_plane()

export_to_stl(
        {
            "plane": plane
        }
)
display(plane)
show()
