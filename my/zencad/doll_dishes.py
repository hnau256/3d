from utils import *

plate_1_radius = 7
plate_2_radius = 8.5
plate_3_radius = 10
pot_radius = 15

wall = 0.8
separation = 2


def create_plate(radius: float):
    height = radius * 0.5
    z = cylinder(radius, height)
    z = z.chamfer(height / 2, [(0, radius, 0)])
    z = z.fillet(height, [(0, radius, height / 2)])
    z = thicksolid(z, -wall, [(0, 0, height)])
    return z


def create_pot(radius: float):
    height = radius * 1.2
    z = cylinder(radius, height)
    z = z.chamfer(height / 8, [(0, radius, 0)])
    z = z.fillet(height / 4, [(0, radius, height / 8)])
    z = thicksolid(z, -wall, [(0, 0, height)])
    return z


plate_1 = create_plate(plate_1_radius)
plate_2 = create_plate(plate_2_radius)
plate_3 = create_plate(plate_3_radius)
pot = create_pot(pot_radius)

export_to_stl(
        {
            "plate_1": plate_1,
            "plate_2": plate_2,
            "plate_3": plate_3,
            "pot": pot
        }
)

center_x = 0
display(plate_1)
center_x += plate_1_radius + plate_2_radius + separation
display(plate_2.right(center_x))
center_x += plate_2_radius + plate_3_radius + separation
display(plate_3.right(center_x))
center_x += plate_3_radius + pot_radius + separation
display(pot.right(center_x))
show()
