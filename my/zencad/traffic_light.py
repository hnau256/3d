from utils import *
import svgpathtools as svg

wall = 2.4
clearance = 0.3

main_section_height = 36
main_fillet = 2.4

led_wire_length = 8
led_radius = 3 / 2
led_handle_length = 1.2


def create_main():
    def create_section():
        z = box(main_section_height, main_section_height, main_section_height)
        z = z.fillet(
                wall - tech,
                [
                    (main_section_height, 0, main_section_height/2),
                    (main_section_height, main_section_height, main_section_height/2)
                 ]
        )
        z = z.translate(-main_section_height / 2, -main_section_height / 2)
        z -= halfspace().rotateX(pi / 2).back(clearance / 2).rotateZ(pi / 4)
        z -= halfspace().rotateX(-pi / 2).forw(clearance / 2).rotateZ(-pi / 4)
        wire_sbtn = box(led_wire_length * 2, led_wire_length * 2, main_section_height)
        wire_sbtn = wire_sbtn.translate(-led_wire_length, -led_wire_length)
        z -= wire_sbtn
        cone_sbtn_max_radius = main_section_height / 2 - wall - wall
        cone_sbtn_length = main_section_height / 2 - led_wire_length - led_handle_length
        cone_sbtn = cone(led_radius+tech, cone_sbtn_max_radius, cone_sbtn_length)
        cone_sbtn = cone_sbtn.rotateY(pi/2)
        cone_sbtn = cone_sbtn.up(main_section_height / 2)
        cone_sbtn = cone_sbtn.right(main_section_height/2 - cone_sbtn_length)
        z -= cone_sbtn
        led_sbtn = cylinder(led_radius+tech, led_handle_length)
        led_sbtn = led_sbtn.rotateY(pi/2)
        led_sbtn = led_sbtn.up(main_section_height / 2)
        led_sbtn = led_sbtn.right(led_wire_length)
        z -= led_sbtn
        return z

    z = create_section()
    z = union([z.up(main_section_height * i) for i in range(3)])
    z = union([z.rotateZ(pi / 2 * i) for i in range(4)])
    return z


display(create_main())
show()
