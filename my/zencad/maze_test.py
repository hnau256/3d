from utils import *

def gen_maze_test():
    size = 16
    cells_length = 7
    z = sphere(size / 2)
    z = z.scaleZ(2)
    z -= halfspace().down(size / 2)
    z -= halfspace().mirrorXY().up(size / 2)
    maze_size = size * sqrt(3) + tech
    maze = gen_maze(cells_length, cells_length, cells_length, maze_size / (cells_length - 1), 0.75)
    maze = maze.translate(-maze_size / 2, -maze_size / 2, -maze_size / 2)
    maze = maze.rotateX(pi / 8)
    maze = maze.rotateZ(pi / 8)
    z -= maze
    return z


disp(gen_maze_test())
show()
