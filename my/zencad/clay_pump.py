#!/usr/bin/env python3
# coding: utf-8
import time

from utils import *
from involute import *

# measured
bolt_radius: float = 3.75 / 2
nut_width: float = 6.83
nut_height: float = 3.04

# invented
clearance: float = 0.1
base_wall: float = 2.4
tooth_count: int = 6
gear_context: Gear = InvoluteStyle(
    module=4.5,
    addendum_by_module=0.9,
    dedendum_by_module=1
).create_gear(
    teeth_count=tooth_count,
    min_radius_fillet_by_module=0.25,
)
main_gear_thickness: float = 12
axis_hanging_angle: float = pi / 4
pipe_wall: float = 1.2
pipe_flange: float = 1.2
main_gear_wall: float = 2.4
far_pipe_in_radius: float = 6.0
near_pipe_in_radius: float = 4.0
pipe_length: float = 20.0
nut_wall: float = 0.8
axis_separator_thickness: float = 0.8
axis_separator_additional_radius: float = 2.4
axis_separator_empty_angle: float = pi / 4
axis_separator_groove_depth: float = 1.2
axis_margin: float = 0.8
handle_thickness: float = 2.4
handle_wall: float = 3.2
handle_length: float = 70.0


# calculated

def calc_nut_radius(offset: float) -> float:
    return (nut_width / 2 + offset) / cos(pi / 6)


nut_sbtn_radius = calc_nut_radius(offset=clearance)
gear_radius = gear_context.max_radius
axis_radius = gear_context.min_radius - main_gear_wall
axis_dx = gear_context.joint_radius + clearance / 2
axis_radius_shrinkage = axis_radius - sin(axis_hanging_angle) * axis_radius
base_height = base_wall + clearance + main_gear_thickness / 2 + clearance / 2
base_length = base_wall + clearance + gear_radius * 2 + clearance + base_wall
base_width = (base_wall + clearance + axis_dx + gear_radius) * 2
pipe_adapter_length = nut_sbtn_radius * 2 + nut_wall

pipe_flange_mid_y = base_length / 2 + clearance + pipe_wall / 2


def create_axis_sketch(
        offset: float,
        shrinkage: bool,
) -> Shape:
    radius = axis_radius + offset
    z = circle(radius)
    if shrinkage:
        sbtn = halfspace().rotateX(-pi / 2).back(radius - axis_radius_shrinkage)
        sbtn += sbtn.rotateZ(pi)
        z -= sbtn
    return z


def create_main_gear() -> Shape:
    z = gear_context.wire.fill()
    z -= create_axis_sketch(
        offset=clearance,
        shrinkage=True,
    )
    z = z.extrude(main_gear_thickness)
    z = z.down(main_gear_thickness / 2)
    return z


def create_pipe(
        in_radius: float,
        sbtn: bool,
) -> Shape:
    offset = clearance if sbtn else 0
    out_radius = in_radius + pipe_wall + offset
    z = cylinder(
        r=out_radius,
        h=pipe_length,
    )
    flange = offset + pipe_wall + offset
    z += cylinder(
        r=out_radius + pipe_flange,
        h=flange,
    ).down(flange / 2)
    if sbtn:
        bottom_sbtn_length = tech + base_wall + flange / 2
        z += cylinder(
            r=in_radius,
            h=bottom_sbtn_length,
        ).down(bottom_sbtn_length)
    else:
        z -= cylinder(
            r=in_radius,
            h=tech + flange + pipe_length + tech,
        ).down(flange + tech)
    z = z.rotateX(-pi / 2)
    z = z.forw(pipe_flange_mid_y)
    return z


def create_nut(
        sbtn: bool,
) -> Shape:
    offset = clearance if sbtn else 0
    radius = (nut_width / 2 + offset) / cos(pi / 6)
    angle_step = pi / 3

    def create_shape(
            radius: float,
    ) -> Shape:
        return polygon(
            pnts=[
                (cos(i * angle_step) * radius, sin(i * angle_step) * radius)
                for i in range(6)
            ],
            wire=True,
        )

    levels = [
        create_shape(
            radius=radius,
        ),
        create_shape(
            radius=radius,
        ).up(nut_height),
    ]
    if sbtn:
        levels.append(
            create_shape(
                radius=tech,
            ).up(nut_height + (radius - tech))
        )
    z = loft(
        arr=levels,
    )
    return z


def create_bolt(
        sbtn: bool,
) -> Shape:
    height = base_height * 2
    offset = clearance if sbtn else 0
    z = cylinder(
        r=bolt_radius + offset,
        h=height,
    ).up(tech)
    z += create_nut(
        sbtn=sbtn,
    )
    if not sbtn:
        cap_radius = bolt_radius + offset + base_wall
        z += cylinder(
            r=cap_radius,
            h=base_wall,
        ) \
            .fillet(
            r=base_wall - tech,
            refs=[
                (0, cap_radius, base_wall)
            ]
        ) \
            .up(height)

    z = z.down(base_height)
    return z


bolt_sbtn = create_bolt(
    sbtn=True,
)
bolts_coordinates: [(float, float)] = []


def place_bolt_sbtn(
        x: float,
        y: float,
) -> Shape:
    bolts_coordinates.append((x, y))
    return bolt_sbtn.translate(x, y)
    pass


def create_base() -> (Shape, Shape):
    z = box(
        base_width,
        base_length,
        base_height,
    )
    z = z.translate(
        -base_width / 2,
        -base_length / 2,
    )
    z = z.fillet(
        nut_sbtn_radius + nut_wall,
        [
            (base_width / 2, base_length / 2, base_height / 2),
            (base_width / 2, -base_length / 2, base_height / 2),
            (-base_width / 2, base_length / 2, base_height / 2),
            (-base_width / 2, -base_length / 2, base_height / 2),
        ]
    )
    z -= wire_builder(
        start=(axis_dx, -base_length / 2 + base_wall),
        defrel=False
    ) \
        .arc_by_points(
        a=(axis_dx + base_length / 2 - base_wall, 0),
        b=(axis_dx, base_length / 2 - base_wall)
    ) \
        .l(-axis_dx, base_length / 2 - base_wall) \
        .arc_by_points(
        a=(-axis_dx - base_length / 2 + base_wall, 0),
        b=(-axis_dx, -base_length / 2 + base_wall)
    ) \
        .close() \
        .doit() \
        .fill() \
        .extrude(base_height - base_wall) \
        .up(base_wall)

    def create_axis_sbtn() -> Shape:
        z = create_axis_sketch(
            offset=clearance,
            shrinkage=False,
        )
        z = z.right(axis_dx)
        z += z.mirrorYZ()
        z = z.extrude(base_height)
        return z

    z -= create_axis_sbtn()

    z = z.down(base_height)

    def create_pipe_adapter(
            in_radius: float,
    ) -> (Shape, float, float):
        bolt_dx = in_radius + pipe_wall + clearance + nut_sbtn_radius
        width_div_2 = bolt_dx + nut_sbtn_radius + nut_wall
        z = box(
            width_div_2 * 2,
            pipe_adapter_length,
            base_height
        )
        z = z.translate(
            -width_div_2,
            base_length / 2,
            -base_height,
        )
        z = z.fillet(
            r=nut_sbtn_radius + nut_wall,
            refs=[
                (width_div_2, base_length / 2 + pipe_adapter_length, -base_height / 2),
                (-width_div_2, base_length / 2 + pipe_adapter_length, -base_height / 2),
            ]
        )
        return z, bolt_dx, width_div_2

    far_pipe_adapter, far_pipe_bolt_dx, far_pipe_adapter_dx = create_pipe_adapter(
        in_radius=far_pipe_in_radius,
    )
    z += far_pipe_adapter

    near_pipe_adapter, near_pipe_bolt_dx, near_pipe_adapter_dx = create_pipe_adapter(
        in_radius=near_pipe_in_radius,
    )
    z += near_pipe_adapter.mirrorXZ()

    z = z.fillet(
        pipe_adapter_length - nut_sbtn_radius - nut_wall - tech,
        [
            (near_pipe_adapter_dx, -base_length / 2, -base_height / 2),
            (-near_pipe_adapter_dx, -base_length / 2, -base_height / 2),
            (far_pipe_adapter_dx, base_length / 2, -base_height / 2),
            (-far_pipe_adapter_dx, base_length / 2, -base_height / 2),
        ]
    )

    z = unify(z)

    bottom = z
    top = z.mirrorXY()

    far_pipe_sbtn = create_pipe(
        in_radius=far_pipe_in_radius,
        sbtn=True,
    )

    near_pipe_sbtn = create_pipe(
        in_radius=near_pipe_in_radius,
        sbtn=True,
    ).mirrorXZ()

    bottom -= far_pipe_sbtn
    bottom -= near_pipe_sbtn

    top -= far_pipe_sbtn
    top -= near_pipe_sbtn

    corner_bolt_dx = base_width / 2 - nut_wall - nut_sbtn_radius
    corner_bolt_dy = base_length / 2 - nut_wall - nut_sbtn_radius
    pipe_bolt_dy = base_length / 2 + pipe_adapter_length - nut_wall - nut_sbtn_radius
    bolts_sbtn = union([
        place_bolt_sbtn(corner_bolt_dx, corner_bolt_dy),
        place_bolt_sbtn(corner_bolt_dx, -corner_bolt_dy),
        place_bolt_sbtn(-corner_bolt_dx, corner_bolt_dy),
        place_bolt_sbtn(-corner_bolt_dx, -corner_bolt_dy),
        place_bolt_sbtn(far_pipe_bolt_dx, pipe_bolt_dy),
        place_bolt_sbtn(-far_pipe_bolt_dx, pipe_bolt_dy),
        place_bolt_sbtn(near_pipe_bolt_dx, -pipe_bolt_dy),
        place_bolt_sbtn(-near_pipe_bolt_dx, -pipe_bolt_dy),
    ])

    bottom -= bolts_sbtn
    top -= bolts_sbtn

    return bottom, top


def create_separator(
        sbtn: bool
) -> Shape:
    offset = clearance if sbtn else 0
    in_radius = axis_radius - axis_separator_groove_depth + clearance - offset
    out_radius = axis_radius + axis_separator_additional_radius + offset
    height = offset + axis_separator_thickness + offset
    z = cylinder(
        r=out_radius,
        h=height,
        center=True,
    )
    z -= cylinder(
        r=in_radius,
        h=tech + height + tech,
        center=True,
    )
    if not sbtn:
        istn = cylinder(
            r=out_radius + tech,
            h=tech + height + tech,
            center=True,
        )
        istn -= halfspace().rotateX(-pi / 2).rotateZ(axis_separator_empty_angle/2)
        istn -= halfspace().rotateX(pi / 2).rotateZ(-axis_separator_empty_angle/2)
        z -= istn
    return z


separator = create_separator(
    sbtn=False,
)


def create_axes_and_separators() -> (Shape, [Shape], Shape, [Shape]):
    separator_sbtn = create_separator(
        sbtn=True,
    )

    def create_axis_and_separators(
            separators_zs: [float],
    ) -> (Shape, [Shape]):
        max_separator: float = 0
        min_separator: float = 0
        for separator_z in separators_zs:
            if separator_z < min_separator:
                min_separator = separator_z
            else:
                if separator_z > max_separator:
                    max_separator = separator_z
        margin = axis_separator_thickness / 2 + clearance + axis_margin
        min_z = min_separator - margin
        max_z = max_separator + margin
        height = max_z - min_z
        z = create_axis_sketch(
            offset=0,
            shrinkage=True,
        )
        z = z.extrude(height)
        z = z.up(min_z)
        separators = [separator.up(i) for i in separators_zs]
        z -= union([separator_sbtn.up(i) for i in separators_zs])
        return z, separators

    base_separator_dz = base_height + clearance + axis_separator_thickness / 2
    separators_zs = [
        -base_separator_dz,
        base_separator_dz,
    ]

    left_axis, left_separators = create_axis_and_separators(
        separators_zs=separators_zs
    )

    handle_bottom_separator_dz = base_separator_dz + axis_separator_thickness + axis_margin * 2 + clearance * 2
    separators_zs += [
        handle_bottom_separator_dz,
        handle_bottom_separator_dz + handle_thickness + axis_separator_thickness + clearance * 2
    ]
    right_axis, right_separators = create_axis_and_separators(
        separators_zs=separators_zs
    )

    return left_axis, left_separators, right_axis, right_separators


def create_handle() -> Shape:
    center_radius = axis_radius + clearance + handle_wall
    side_radius = nut_sbtn_radius + handle_wall
    bezier_arm_length_center = handle_length / 5
    bezier_arm_length_side = handle_length / 1.5
    z = sew(
        [
            circle_arc(
                (0, center_radius),
                (-center_radius, 0),
                (0, -center_radius),
            ),
            bezier(
                [
                    (0, -center_radius),
                    (bezier_arm_length_center, -center_radius),
                    (handle_length - bezier_arm_length_side, -side_radius),
                    (handle_length, -side_radius),
                ]
            ),
            circle_arc(
                (handle_length, -side_radius),
                (handle_length + side_radius, 0),
                (handle_length, side_radius),
            ),
            bezier(
                [
                    (handle_length, side_radius),
                    (handle_length - bezier_arm_length_side, side_radius),
                    (bezier_arm_length_center, center_radius),
                    (0, center_radius),
                ]
            ),
        ]
    )
    z = z.fill()
    z -= circle(bolt_radius + clearance).right(handle_length)
    z -= create_axis_sketch(
        offset=clearance,
        shrinkage=True,
    )
    z = z.extrude(handle_thickness)
    z = z.down(handle_thickness / 2)
    return z


def create_bolts() -> Shape:
    z = create_bolt(
        sbtn=False,
    )
    z = union([
        z.translate(x, y) for x, y in bolts_coordinates
    ])
    return z


left_axis_mediators = []
right_axis_mediators = []

main_gear = create_main_gear()
left_axis_mediators.append(disp(main_gear, choose_color_to_display(0)))
right_axis_mediators.append(disp(main_gear, choose_color_to_display(5)))

base_bottom, base_top = create_base()
disp(base_bottom, choose_color_to_display(8))
disp(base_top, Color(1, 1, 1, 0.75))

far_pipe = create_pipe(
    in_radius=far_pipe_in_radius,
    sbtn=False,
)
disp(far_pipe, choose_color_to_display(3))

near_pipe = create_pipe(
    in_radius=near_pipe_in_radius,
    sbtn=False,
).mirrorXZ()
disp(near_pipe, choose_color_to_display(14))

(left_axis, left_separators, right_axis, right_separators) = create_axes_and_separators()
left_axis_mediators.append(disp(left_axis, choose_color_to_display(2)))
for left_separator in left_separators:
    left_axis_mediators.append(disp(left_separator, choose_color_to_display(13)))

right_axis_mediators.append(disp(right_axis, choose_color_to_display(4)))
for right_separator in right_separators:
    right_axis_mediators.append(disp(right_separator, choose_color_to_display(13)))

handle = create_handle()
handle_z = base_height + clearance * 4 + axis_separator_thickness * 2 + axis_margin * 2 + handle_thickness / 2
right_axis_mediators.append(disp(handle.up(handle_z)))

bolts = create_bolts()
disp(bolts, choose_color_to_display(1))

export_to_stl(
    shapes={
        "base_bottom": base_bottom,
        "base_top": base_top.rotateY(pi),
        "far_pipe": far_pipe.rotateX(pi / 2),
        "near_pipe": near_pipe.rotateX(-pi / 2),
        "gear_x2": main_gear,
        "handle": handle,
        "left_axis": left_axis.rotateX(pi / 2),
        "right_axis": right_axis.rotateX(pi / 2),
        f"separator_x{len(left_separators) + len(right_separators)}": separator,
    }
)

zero_time = time.time()


def animate(widget):
    angle = (time.time() - zero_time) / 3
    left_transformation = right(-axis_dx) * rotateZ(angle + pi / tooth_count)
    for mediator in left_axis_mediators:
        mediator.relocate(left_transformation)
    right_transformation = right(axis_dx) * rotateZ(-angle)
    for mediator in right_axis_mediators:
        mediator.relocate(right_transformation)


show(animate=animate)
