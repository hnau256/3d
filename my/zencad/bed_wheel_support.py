from utils import *

height = 4.25
width = 45.5
length = 19.5
hole_radius = 5.25 / 2
hole_offset = width / 2 - 3 - hole_radius
corners_fillet = 3
corners_chamfer = 0.5


def create_bed_support():
    z = box(width, length, height)
    z = z.fillet(
            corners_fillet,
            [
                (0, 0, height / 2),
                (width, 0, height / 2),
                (0, length, height / 2),
                (width, length, height / 2)
            ]
    )
    z = z.chamfer(
            corners_chamfer,
            [
                (width / 2, 0, 0),
                (width / 2, 0, height)
            ]
    )
    z = z.translate(-width / 2, -length / 2)
    hole = cylinder(hole_radius, height)
    hole = hole.right(hole_offset)
    z -= hole
    z -= hole.rotateZ(pi)
    z = z.chamfer(
            corners_chamfer,
            [
                (hole_offset, hole_radius, 0),
                (-hole_offset, hole_radius, 0),
                (hole_offset, hole_radius, height),
                (-hole_offset, hole_radius, height)
            ]
    )
    return z


bed_support = create_bed_support()

export_to_stl(
        shapes={
            "bed_support": bed_support
        }
)

display(bed_support)
show()
