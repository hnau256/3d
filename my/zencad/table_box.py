#!/usr/bin/env python3
# coding: utf-8

from utils import *

# measured
glass_thickness: float = 4.9

# invented
tech: float = 0.1
clearance: float = 0.2
wall: float = 1.8
handle_length: float = 12.0
in_width: float = 60
in_height: float = 60
in_depth: float = 20
in_xy_fillet: float = 5
in_z_fillet: float = 2.0
hanging_angle: float = pi / 2.75


def create_box() -> Shape:
    out_width = wall + in_width + wall
    shape_out_width = out_width + wall
    out_height = wall + in_height
    out_depth = in_depth + wall + wall

    out_radius = out_depth / (1 - sin(pi / 2 - hanging_angle))
    ly = out_radius - out_depth
    lx = sqrt(out_radius ** 2 - ly ** 2)
    dy = sqrt(out_radius ** 2 - (lx / 2) ** 2) - ly

    z = sew([
        segment(
            (-out_width / 2, 0),
            (-out_width / 2, out_depth),
        ),
        segment(
            (-out_width / 2, out_depth),
            (shape_out_width / 2 - lx, out_depth),
        ),
        circle_arc(
            (shape_out_width / 2 - lx, out_depth),
            (shape_out_width / 2 - lx / 2, dy),
            (shape_out_width / 2, 0),
        ),
        segment(
            (shape_out_width / 2, 0),
            (-out_width / 2, 0),
        ),
    ])
    z = z.fill()
    z = z.extrude(out_height)
    z = thicksolid(
        proto=z,
        t=-wall,
        refs=[(0, wall + in_depth / 2, out_height)],
    )

    z = z.fillet(
        r=in_z_fillet,
        refs=[
            (in_width / 2, wall, out_height / 2),
            (-in_width / 2, wall, out_height / 2),
            (-in_width / 2, out_depth - wall, out_height / 2),
        ]
    )
    z = z.fillet(
        r=in_xy_fillet,
        refs=[
            (0, wall, wall),
        ]
    )
    z = z.fillet(
        r=in_xy_fillet+wall,
        refs=[
            (0, out_depth, 0),
        ]
    )

    sbtn = halfspace().rotateY(-pi / 2)
    sbtn = sbtn.right(out_width / 2)
    sbtn += sbtn.mirrorYZ()
    z -= sbtn

    def create_handle() -> Shape:
        depth = wall + clearance + glass_thickness + clearance
        height = handle_length + clearance + wall
        z = box(
            out_width,
            depth,
            height,
        )
        z -= box(
            out_width,
            depth - wall,
            height - wall,
        ).forw(wall)
        z = z.translate(
            -out_width / 2,
            -depth,
            out_height - height
        )
        z = z.fillet(
            r=wall - tech,
            refs=[
                (0, -depth, out_height),
                (0, -depth, out_height - height)
            ]
        )
        return z

    z += create_handle()

    z = z.fillet(
        r=wall - tech,
        refs=[
            (0, out_depth - wall, out_height),
            (0, wall, out_height),
            (-in_width / 2, wall + in_depth / 2, out_height),
        ]
    )
    z = unify(z)
    return z


table_box = create_box()
disp(table_box)

export_to_stl(
    shapes={
        "box": table_box.rotateY(-pi / 2).rotateZ(pi / 2),
    },
)
show()
