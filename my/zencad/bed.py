from zencad import *
from utils import *

matress_width = 1700
matress_length = 800
matress_height = 120
matress_compressed_height = 80
matress_corner_fillet = 100

material_thickness = 16
clearance = 4

matress_top = 500
boardside_height = 250
boardside_opening_width = 500
boardside_fillet = 200

wheel_height = 36

# Calculated
matress_bottom = matress_top - matress_height
matress_base_bottom = matress_bottom - material_thickness
total_height = matress_top + boardside_height


def create_matress():
    z = box(matress_width, matress_length, matress_height)
    z = z.translate(-matress_width / 2, material_thickness, matress_bottom)
    z = z.fillet(
            matress_corner_fillet,
            [
                (-matress_width / 2, 0, matress_bottom + matress_compressed_height / 2),
                (matress_width / 2, 0, matress_bottom + matress_compressed_height / 2),
                (-matress_width / 2, matress_length, matress_bottom + matress_compressed_height / 2),
                (matress_width / 2, matress_length, matress_bottom + matress_compressed_height / 2)
            ]
    )
    return z


def create_matress_base():
    z = box(matress_width, matress_length, material_thickness)
    z = z.translate(-matress_width / 2, material_thickness, matress_base_bottom)
    return z


def create_back_side():
    z = box(matress_width, material_thickness, total_height)
    z = z.translate(-matress_width / 2, matress_length + material_thickness)
    return z


def create_front_side():
    z = box(matress_width, material_thickness, material_thickness + matress_compressed_height)
    z = z.translate(-matress_width / 2, 0, matress_base_bottom)
    return z


def create_left_side():
    z = box(material_thickness, material_thickness + matress_length + material_thickness, total_height)
    z = z.fillet(boardside_fillet, [(material_thickness / 2, 0, total_height)])
    z = z.left(matress_width / 2 + material_thickness)
    return z


def create_right_side():
    z = box(material_thickness, material_thickness + matress_length + material_thickness, total_height)
    z = z.right(matress_width / 2)
    return z


def create_folding_boardside():
    min_x = - matress_width / 2 + boardside_opening_width
    max_x = matress_width / 2 - clearance
    width = max_x - min_x
    min_z = matress_bottom + matress_compressed_height + clearance
    height = total_height - min_z
    z = box(width, material_thickness, height)
    z = z.fillet(boardside_fillet, [(0, material_thickness / 2, height)])
    z = z.translate(min_x, 0, min_z)
    return z


def create_matress_base_support():
    z = box(material_thickness, matress_length - clearance, matress_base_bottom)
    z = z.left(material_thickness / 2)
    z = z.forw(material_thickness + clearance)
    return z


def create_chest_fasade():
    width = (matress_width - clearance) / 2 - clearance
    z = box(width, material_thickness, matress_base_bottom - clearance * 2)
    z = z.right(clearance / 2)
    z = z.up(clearance)
    return z


chest_min_z = wheel_height
chest_max_z = matress_base_bottom - clearance
chest_min_x = material_thickness / 2 + clearance
chest_max_x = matress_width / 2 - clearance
chest_min_y = material_thickness
chest_max_y = matress_length + material_thickness - clearance


def create_chest_base():
    z = box(
            chest_max_x - chest_min_x,
            chest_max_y - chest_min_y,
            material_thickness
    )
    z = z.translate(chest_min_x, chest_min_y, chest_min_z)
    return z


def create_chest_side():
    z = box(
            material_thickness,
            chest_max_y - chest_min_y - material_thickness,
            chest_max_z - chest_min_z - material_thickness
    )
    z = z.translate(chest_min_x, chest_min_y, chest_min_z + material_thickness)
    return z


def create_chest_back():
    z = box(
            chest_max_x - chest_min_x,
            material_thickness,
            chest_max_z - chest_min_z - material_thickness
    )
    z = z.translate(chest_min_x, chest_max_y - material_thickness,
                    chest_min_z + material_thickness)
    return z


chest_fasade = create_chest_fasade()
chest_base = create_chest_base()
chest_left_side = create_chest_side()
chest_right_side = chest_left_side.right(chest_max_x - chest_min_x - material_thickness)
chest_back = create_chest_back()

parts = {
    "matress_base": create_matress_base(),
    "back_side": create_back_side(),
    "front_side": create_front_side(),
    "left_side": create_left_side(),
    "right_side": create_right_side(),
    "folding_boardside": create_folding_boardside(),
    "matress_base_support": create_matress_base_support(),
    "right_chest_fasade": chest_fasade,
    "right_chest_base": chest_base,
    "right_chest_left_side": chest_left_side,
    "right_chest_right_side": chest_right_side,
    "right_chest_back": chest_back,
    "left_chest_fasade": chest_fasade.mirrorYZ(),
    "left_chest_base": chest_base.mirrorYZ(),
    "left_chest_left_side": chest_left_side.mirrorYZ(),
    "left_chest_right_side": chest_right_side.mirrorYZ(),
    "left_chest_back": chest_back.mirrorYZ()
}

for (name, shape) in parts.items():
    bounds = shape.bbox()
    raw_sizes = [
        bounds.xmax-bounds.xmin,
        bounds.ymax-bounds.ymin,
        bounds.zmax-bounds.zmin
    ]
    sizes = ""
    size_to_skip = int(material_thickness)
    for size in raw_sizes:
        value = int(size)
        if value != size_to_skip:
            sizes = "" if len(sizes) <= 0 else sizes + "x"
            sizes = sizes + str(value)
    print(name, sizes)

matress = create_matress()

display(matress, Color(1, 1, 0))
for (name, shape) in parts.items():
    display(shape, Color(1, 1, 1))
show()
