from utils import *

content_width = 9
content_height = 5
nail_max_radius = 7 / 2
nail_min_radius = 3 / 2
wall = 1.6
width = 9
clearance = 0.3
tech = 0.001


def create_bracket():
    nail_offset = content_width / 2 + clearance + wall + nail_min_radius
    height = content_height + clearance + wall
    z = box(nail_offset, width, height)
    z += cylinder(width / 2, height).translate(nail_offset, width / 2)
    content_sbtn = box(content_width / 2 + clearance, width, height - wall)
    content_sbtn = content_sbtn.up(wall)
    z -= content_sbtn
    z = z.back(width / 2)
    z -= cone(nail_max_radius, 0, nail_max_radius).right(nail_offset)
    z -= cylinder(nail_min_radius, height).right(nail_offset)
    z += z.rotateZ(pi)
    return z

bracket = create_bracket()

export_to_stl(
        {
            "bracket": bracket
        }
)

display(bracket)
show()
