from utils import *

hole_radius = 35 / 2
hole_height = 16

tech = 0.001
clearance = 0.3
wall = 1.6
outer_screw_length = 4.8
inner_screw_length = 2.4
screw_depth = 1.2
grip_circle_radius = wall
grip_circles_distance = grip_circle_radius * 4

# Calculated
hole_wall = wall + screw_depth


def create_hole():
    z = cylinder(hole_radius * 2, hole_height)
    z -= cylinder(hole_radius, hole_height)
    return z


def create_base():
    height = outer_screw_length + clearance + hole_height + clearance + wall
    full_screw_length = outer_screw_length + inner_screw_length
    z = cylinder(hole_radius - clearance, height - full_screw_length)
    scr = screw(
            r=hole_radius - clearance,
            h=full_screw_length,
            depth=screw_depth
    )
    scr ^= cylinder(hole_radius - clearance, full_screw_length) \
        .chamfer(wall, [(0, hole_radius - clearance, full_screw_length)])
    z += scr.up(height - full_screw_length)
    z += cylinder(hole_radius + wall, wall)
    z -= cylinder(hole_radius - clearance - hole_wall, height)
    z = z.chamfer(
            wall / 2,
            [
                (0, hole_radius - clearance - hole_wall, 0),
                (0, hole_radius - clearance - hole_wall, height),
                (0, hole_radius + wall, 0)
            ]
    )
    z = z.down(wall + clearance)
    return z


def create_nut():
    screw_radius = hole_radius - clearance + clearance * sqrt(2)
    radius = screw_radius + wall + grip_circle_radius
    z = cylinder(radius, outer_screw_length)
    z = z.chamfer(wall / 2, [(0, radius, outer_screw_length)])
    z -= screw(screw_radius, outer_screw_length, screw_depth)
    fillet_sbtn = cone(screw_radius, screw_radius - screw_depth, screw_depth)
    z -= fillet_sbtn
    z -= fillet_sbtn.rotateX(pi).up(outer_screw_length)

    def create_grip_sbtn():
        count = int(floor(radius * 2 * pi / grip_circles_distance))
        angle = pi * 2 / count
        z = cylinder(grip_circle_radius, outer_screw_length)
        z = z.right(radius)
        z = union([z.rotateZ(i * angle) for i in range(count)])
        return z

    z -= create_grip_sbtn()
    z = z.up(hole_height + clearance)
    return z


base = create_base()
nut = create_nut()

export_to_stl(
        shapes={
            "base": base,
            "nut": nut
        }
)

display(create_hole(), Color(1, 1, 1, 0.5))
display(base ^ halfspace().rotateX(pi / 2), Color(1, 1, 0))
display(nut ^ halfspace().rotateX(pi / 2), Color(0, 1, 1))
show()

