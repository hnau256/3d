from dataclasses import dataclass
from zencad import *
from math import *

from zencad import point3


# https://3dtoday.ru/blogs/3drafter/cog-method-of-construction-for-any-cad-system?page=user&id=709

@dataclass
class Gear:
    wire: Shape
    min_radius: float
    max_radius: float
    joint_radius: float
    tooth_angle: float


@dataclass
class InvoluteStyle:
    module: float
    pressure_angle: float = deg2rad(20)
    involute_interpolate_points_count: int = 64

    def create_gear(
            self,
            teeth_count: int,
            min_radius_fillet_by_module: float = 0.4,
            addendum_by_module: float = 1,
            dedendum_by_module: float = 1.1,
    ) -> Gear:
        joint_radius: float = self.module * teeth_count / 2
        addendum: float = self.module * addendum_by_module
        dedendum: float = self.module * dedendum_by_module
        max_radius: float = joint_radius + addendum
        min_radius: float = joint_radius - dedendum
        tooth_angle: float = pi * 2 / teeth_count
        wire = self._create_gear_wire(
            teeth_count=teeth_count,
            min_radius=min_radius,
            max_radius=max_radius,
            joint_radius=joint_radius,
            tooth_angle=tooth_angle,
            min_radius_fillet_by_module=min_radius_fillet_by_module,
        )
        return Gear(
            wire=wire,
            min_radius=min_radius,
            max_radius=max_radius,
            joint_radius=joint_radius,
            tooth_angle=tooth_angle,
        )

    def _create_gear_wire(
            self,
            min_radius: float,
            max_radius: float,
            joint_radius: float,
            teeth_count: int,
            tooth_angle: float,
            min_radius_fillet_by_module: float,
    ) -> Shape:
        min_radius_fillet: float = self.module * min_radius_fillet_by_module
        build_radius: float = cos(self.pressure_angle) * joint_radius

        max_handle: float = sqrt(max_radius ** 2 - build_radius ** 2)
        handle_step: float = max_handle / (self.involute_interpolate_points_count - 1)
        angle_step: float = handle_step / build_radius
        tooth_angle_on_build_radius: float = pi / teeth_count + 2 * (tan(self.pressure_angle) - self.pressure_angle)

        def calc_tooth() -> (Shape, float):
            def calc_involute_points() -> [point3]:
                def calc_point(
                        point_index: int,
                ) -> point3:
                    root_angle = angle_step * point_index
                    handle_start_x = build_radius * cos(root_angle)
                    handle_start_y = build_radius * sin(root_angle)
                    handle_angle = root_angle - pi / 2
                    handle_length = handle_step * point_index
                    handle_offset_x = handle_length * cos(handle_angle)
                    handle_offset_y = handle_length * sin(handle_angle)
                    return point3(
                        handle_start_x + handle_offset_x,
                        handle_start_y + handle_offset_y,
                    )

                result: [point3] = []
                for i in range(self.involute_interpolate_points_count):
                    point = calc_point(i)
                    if point.x >= min_radius:
                        result.append(point)
                return result

            involute_points = calc_involute_points()
            involute = interpolate(involute_points)
            top_involute_point = involute_points[-1].rotateZ(-tooth_angle_on_build_radius / 2)
            bottom_involute_point = involute_points[0]
            min_radius_below_involute_point = point3(min_radius, bottom_involute_point.y)

            min_radius_arc = circle_arc(
                min_radius_below_involute_point.mirrorXZ().rotateZ(tooth_angle_on_build_radius / 2 - tooth_angle),
                min_radius_below_involute_point.rotateZ(-tooth_angle / 2),
                min_radius_below_involute_point.rotateZ(-tooth_angle_on_build_radius / 2),
            )

            min_radius_to_involute_segment = segment(
                min_radius_below_involute_point,
                bottom_involute_point,
            ).rotateZ(-tooth_angle_on_build_radius / 2)

            involute_min_to_max = involute.rotateZ(-tooth_angle_on_build_radius / 2)

            max_radius_arc = circle_arc(
                top_involute_point,
                point3(max_radius, 0, 0),
                top_involute_point.mirrorXZ(),
            )

            involute_max_to_min = involute.rotateZ(-tooth_angle_on_build_radius / 2).mirrorXZ()

            involute_to_min_radius_segment = segment(
                bottom_involute_point,
                min_radius_below_involute_point,
            ).rotateZ(-tooth_angle_on_build_radius / 2).mirrorXZ()

            z: [Shape] = [
                min_radius_arc,
                min_radius_to_involute_segment,
                involute_min_to_max,
                max_radius_arc,
                involute_max_to_min,
                involute_to_min_radius_segment,
            ]
            z = sew(z)
            return z, bottom_involute_point.x

        z, involute_min_radius = calc_tooth()
        z = sew([z.rotateZ(tooth_angle * i) for i in range(teeth_count)])
        z = z.fill()

        fillet_radius = min(min_radius_fillet, involute_min_radius - min_radius - 0.001)
        if fillet_radius > 0:

            def calc_min_radius_corners_points() -> [point3]:
                angles = tooth_angle_on_build_radius / 2
                angles = [-angles, angles]
                z = []
                for i in range(teeth_count):
                    offset = i * tooth_angle
                    for angle in angles:
                        z.append(offset + angle)
                base_point = point3(min_radius, 0, 0)
                z = [base_point.rotateZ(angle) for angle in z]
                return z

            min_radius_corners_points = calc_min_radius_corners_points()
            z = z.fillet2d(fillet_radius, min_radius_corners_points)
        z = z.wires()[0]
        return z


if __name__ == "__main__":
    from time import time

    wall = 24
    friction = 0

    involute_style = InvoluteStyle(
        module=24,
        pressure_angle=deg2rad(24)
    )

    gear1 = involute_style.create_gear(
        teeth_count=16,
    )
    gear2 = involute_style.create_gear(
        teeth_count=64,
        min_radius_fillet_by_module=0.15,

    )

    gear_1_bind = disp(
        difference(
            [
                gear1.wire.fill(),
                circle(gear1.min_radius - wall)
            ]
        )
        .extrude(wall)
        .rotateZ(gear1.tooth_angle / 2),
        Color(0x03 / 255, 0xA9 / 255, 0xF4 / 255),
    )

    gear_2_bind = disp(
        difference(
            [
                gear2.wire.fill(),
                circle(gear2.min_radius - wall)
            ]
        )
        .extrude(wall),
        Color(0xFF / 255, 0xC1 / 255, 0x07 / 255),
    )

    startTime = time()


    def animate(_):
        teeth_rotation = (time() - startTime) * 0.1

        gear1_transformation = \
            left(gear1.joint_radius + friction / 2) \
            * rotateZ(-teeth_rotation * gear1.tooth_angle)
        gear_1_bind.relocate(gear1_transformation)

        gear2_transformation = \
            right(gear2.joint_radius + friction / 2) \
            * rotateZ(teeth_rotation * gear2.tooth_angle)
        gear_2_bind.relocate(gear2_transformation)


    show(animate=animate)
