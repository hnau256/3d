from zencad import *


def parallelepiped(
        x_min: float,
        x_max: float,
        y_min: float,
        y_max: float,
        z_min: float,
        z_max: float,
) -> Shape:
    z = zencad.box(
        x_max - x_min,
        y_max - y_min,
        z_max - z_min,
    )
    z = z.translate(
        x_min,
        y_min,
        z_min,
    )
    return z
