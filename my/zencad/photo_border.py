#!/usr/bin/env python3
# coding: utf-8

from placers import *
from utils import *

# Measured
glass_size: float = 201
glass_thickness: float = 1.6
substrate_thickness: float = 2.35
image_size: float = 175
image_thickness: float = 1

# Invented
clearance: float = 0.1
wall: float = 1.2
image_border: float = 7
press_border: float = 16
press_thickness: float = 9.6
intersection: bool = True
border: float = 12
screws_side_count: int = 3
screw_depth: float = 1.2
screw_lock_size: float = 1.6
screw_groove_thickness: float = 1.2
screw_groove_border: float = 1.2

# Calculated
intersection_shape: Shape = halfspace().rotateX(-pi / 2)


def display(
        shape: Shape,
        color: Color,
):
    if intersection:
        shape ^= intersection_shape
    disp(shape, color)


class Nicks:

    def __init__(self):
        self.image_xy = image_size / 2
        self.window_max_xy = self.image_xy + image_border
        self.glass_xy = glass_size / 2
        self.glass_sbtn_xy = self.glass_xy + clearance
        self.main_max_xy = self.window_max_xy + border
        self.substrate_xy = self.glass_xy - image_thickness
        self.press_max_xy = self.glass_xy
        self.press_min_xy = self.press_max_xy - press_border

        screw_sbtn_min_xy = self.press_min_xy + wall
        screw_sbtn_max_xy = self.main_max_xy - wall
        self.screw_center_xy = (screw_sbtn_min_xy + screw_sbtn_max_xy) / 2
        self.screw_sbtn_radius = screw_sbtn_max_xy - self.screw_center_xy
        self.screw_lock_sbtn_max_radius = self.screw_sbtn_radius
        self.screw_lock_sbtn_min_radius = self.screw_lock_sbtn_max_radius - screw_lock_size - screw_depth
        self.screw_radius = self.screw_sbtn_radius - clearance * sqrt(2)
        self.screw_lock_max_radius = self.screw_radius - screw_depth
        self.screw_lock_min_radius = self.screw_lock_max_radius - screw_lock_size
        self.screw_groove_y = screw_groove_thickness / 2
        self.screw_groove_radius = self.screw_lock_min_radius - screw_groove_border

        self.glass_min_z = 0
        self.glass_max_z = self.glass_min_z + glass_thickness
        self.main_min_z = self.glass_min_z - wall
        self.image_min_z = self.glass_max_z
        self.image_max_z = self.image_min_z + image_thickness
        self.substrate_min_z = self.image_max_z
        self.substrate_max_z = self.substrate_min_z + substrate_thickness
        self.image_back_min_z = self.substrate_max_z
        self.image_back_max_z = self.image_back_min_z + image_thickness
        self.press_min_z = self.image_back_max_z
        self.press_max_z = self.press_min_z + press_thickness
        self.main_max_z = self.press_max_z
        self.screw_cylinder_sbtn_min_z = self.press_min_z + wall
        self.screw_min_z = self.screw_cylinder_sbtn_min_z + clearance
        self.screw_cylinder_sbtn_lock_max_z = self.press_max_z
        self.screw_cylinder_sbtn_lock_min_z = self.screw_cylinder_sbtn_lock_max_z - screw_lock_size - screw_depth
        self.screw_lock_max_z = self.screw_cylinder_sbtn_lock_max_z - clearance
        self.screw_lock_min_z = self.screw_lock_max_z - screw_lock_size
        self.screw_screw_sbtn_max_z = self.press_max_z
        self.screw_screw_sbtn_min_z = self.glass_min_z
        self.screw_groove_max_z = self.screw_lock_max_z
        self.screw_groove_min_z = self.screw_min_z + wall


nicks = Nicks()


class Landmarks:
    glass: Shape
    substrate: Shape
    image: Shape
    image_background: Shape

    @staticmethod
    def _create_glass() -> Shape:
        return parallelepiped(
            x_min=-nicks.glass_xy,
            x_max=nicks.glass_xy,
            y_min=-nicks.glass_xy,
            y_max=nicks.glass_xy,
            z_min=nicks.glass_min_z,
            z_max=nicks.glass_max_z,
        )

    @staticmethod
    def _create_substrate() -> Shape:
        return parallelepiped(
            x_min=-nicks.substrate_xy,
            x_max=nicks.substrate_xy,
            y_min=-nicks.substrate_xy,
            y_max=nicks.substrate_xy,
            z_min=nicks.substrate_min_z,
            z_max=nicks.substrate_max_z,
        )

    @staticmethod
    def _create_image_background() -> Shape:
        z = parallelepiped(
            x_min=-nicks.glass_xy,
            x_max=nicks.glass_xy,
            y_min=-nicks.glass_xy,
            y_max=nicks.glass_xy,
            z_min=nicks.image_min_z,
            z_max=nicks.image_back_max_z,
        )
        z -= parallelepiped(
            x_min=-nicks.substrate_xy,
            x_max=nicks.substrate_xy,
            y_min=-nicks.substrate_xy,
            y_max=nicks.substrate_xy,
            z_min=nicks.substrate_min_z,
            z_max=nicks.substrate_max_z,
        )
        z -= parallelepiped(
            x_min=-nicks.image_xy,
            x_max=nicks.image_xy,
            y_min=-nicks.image_xy,
            y_max=nicks.image_xy,
            z_min=nicks.image_min_z,
            z_max=nicks.substrate_min_z,
        )
        return z

    @staticmethod
    def _create_image() -> Shape:
        z = parallelepiped(
            x_min=-nicks.image_xy,
            x_max=nicks.image_xy,
            y_min=-nicks.image_xy,
            y_max=nicks.image_xy,
            z_min=nicks.image_min_z,
            z_max=nicks.substrate_min_z,
        )
        return z

    def __init__(self):
        self.glass = Landmarks._create_glass()
        self.substrate = Landmarks._create_substrate()
        self.image = Landmarks._create_image()
        self.image_background = Landmarks._create_image_background()

    def disp(self):
        display(self.glass, Color(1, 1, 1, 0.75))
        display(self.substrate, Color(1, 1, 0))
        display(self.image, Color(1, 0, 1))
        display(self.image_background, Color(0.75, 0.75, 0.75))


landmarks = Landmarks()


class Shapes:
    main: Shape
    press: Shape
    screws: Shape
    screw: Shape
    screws_count: int

    class Screws:
        main_single: Shape
        main: Shape
        screw_sbtn: Shape
        cylinder_sbtn: Shape
        count: int

        @staticmethod
        def _create_main():
            h = nicks.screw_lock_min_z - nicks.screw_min_z
            z = screw(
                r=nicks.screw_radius,
                h=h,
                depth=screw_depth,
            )
            istn_h = h + nicks.screw_radius - screw_depth
            istn = cone(
                r1=istn_h,
                r2=0,
                h=istn_h,
            )
            z ^= istn
            z ^= istn.mirrorXY().up(h)
            z = z.up(nicks.screw_min_z)
            lock = cone(
                r1=nicks.screw_lock_max_radius,
                r2=nicks.screw_lock_min_radius,
                h=nicks.screw_lock_max_z - nicks.screw_lock_min_z,
            )
            lock = lock.up(nicks.screw_lock_min_z)
            z += lock
            z -= parallelepiped(
                x_min=-nicks.screw_groove_radius,
                x_max=nicks.screw_groove_radius,
                y_min=-nicks.screw_groove_y,
                y_max=nicks.screw_groove_y,
                z_min=nicks.screw_groove_min_z,
                z_max=nicks.screw_groove_max_z,
            )
            return z

        @staticmethod
        def _create_screw_sbtn():
            h = nicks.screw_screw_sbtn_max_z - nicks.screw_screw_sbtn_min_z
            z = screw(
                r=nicks.screw_sbtn_radius,
                h=h,
                depth=screw_depth,
            )
            ch = cone(
                r1=nicks.screw_sbtn_radius - screw_depth,
                r2=nicks.screw_sbtn_radius,
                h=screw_depth,
            )
            ch = ch.up(h - screw_depth)
            z += ch
            z = z.up(nicks.screw_screw_sbtn_min_z)
            return z

        @staticmethod
        def _create_cylinder_sbtn():
            z = cylinder(
                r=nicks.screw_sbtn_radius,
                h=nicks.screw_cylinder_sbtn_lock_min_z - nicks.screw_cylinder_sbtn_min_z,
            )
            z = z.up(nicks.screw_cylinder_sbtn_min_z)
            lock = cone(
                r1=nicks.screw_lock_sbtn_max_radius,
                r2=nicks.screw_lock_sbtn_min_radius,
                h=nicks.screw_cylinder_sbtn_lock_max_z - nicks.screw_cylinder_sbtn_lock_min_z,
            )
            lock = lock.up(nicks.screw_cylinder_sbtn_lock_min_z)
            z += lock
            return z

        def __init__(self):
            step = nicks.screw_center_xy * 2 / (screws_side_count - 1)
            places: list[tuple[float, float]] = []
            for i in range(screws_side_count - 1):
                offset = i * step
                places.append(
                    (
                        -nicks.screw_center_xy + offset,
                        -nicks.screw_center_xy,
                    ),
                )
                places.append(
                    (
                        nicks.screw_center_xy - offset,
                        nicks.screw_center_xy,
                    ),
                )
                places.append(
                    (
                        nicks.screw_center_xy,
                        -nicks.screw_center_xy + offset,
                    ),
                )
                places.append(
                    (
                        -nicks.screw_center_xy,
                        nicks.screw_center_xy - offset,
                    ),
                )

            def create_shapes(
                    base: Shape,
            ) -> Shape:
                z = []
                for x, y in places:
                    z.append(base.translate(x, y))
                z = union(z)
                return z

            self.main_single = Shapes.Screws._create_main()
            self.main = create_shapes(self.main_single)
            self.screw_sbtn = create_shapes(Shapes.Screws._create_screw_sbtn())
            self.cylinder_sbtn = create_shapes(Shapes.Screws._create_cylinder_sbtn())
            self.count = (screws_side_count - 1) * 4

    @staticmethod
    def _create_main(
            screws: Screws,
    ) -> Shape:
        z = parallelepiped(
            x_min=-nicks.main_max_xy,
            x_max=nicks.main_max_xy,
            y_min=-nicks.main_max_xy,
            y_max=nicks.main_max_xy,
            z_min=nicks.main_min_z,
            z_max=nicks.main_max_z,
        )
        z -= parallelepiped(
            x_min=-nicks.glass_sbtn_xy,
            x_max=nicks.glass_sbtn_xy,
            y_min=-nicks.glass_sbtn_xy,
            y_max=nicks.glass_sbtn_xy,
            z_min=nicks.glass_min_z,
            z_max=nicks.main_max_z,
        )
        z -= parallelepiped(
            x_min=-nicks.window_max_xy,
            x_max=nicks.window_max_xy,
            y_min=-nicks.window_max_xy,
            y_max=nicks.window_max_xy,
            z_min=nicks.main_min_z,
            z_max=nicks.glass_min_z,
        )
        z -= screws.screw_sbtn
        return z

    @staticmethod
    def _create_press(
            screws: Screws,
    ) -> Shape:
        z = parallelepiped(
            x_min=-nicks.press_max_xy,
            x_max=nicks.press_max_xy,
            y_min=-nicks.press_max_xy,
            y_max=nicks.press_max_xy,
            z_min=nicks.press_min_z,
            z_max=nicks.press_max_z,
        )
        z -= parallelepiped(
            x_min=-nicks.press_min_xy,
            x_max=nicks.press_min_xy,
            y_min=-nicks.press_min_xy,
            y_max=nicks.press_min_xy,
            z_min=nicks.press_min_z,
            z_max=nicks.press_max_z,
        )
        z -= screws.cylinder_sbtn
        return z

    def __init__(self):
        screws = Shapes.Screws()
        self.main = Shapes._create_main(screws)
        self.press = Shapes._create_press(screws)
        self.screws = screws.main
        self.screw = screws.main_single
        self.screws_count = screws.count

    def disp(self):
        display(self.main, Color(1, 1, 1))
        display(self.press, Color(1, 1, 1))
        display(self.screws, Color(1, 0, 1))

    def export(self):
        export_to_stl(
            shapes={
                "main": self.main,
                "press": self.press,
                f"screw (x{self.screws_count})": self.screw,
            },
        )


shapes = Shapes()

landmarks.disp()
shapes.disp()
shapes.export()

show()
