from utils import *

level_height = 5
levels_count = 10
acceleration_width = 8
waves_width = 32
deepening_radius = level_height / 2 * 1.2
deepening_length = 4


def create_test():
    def create_level():
        width = acceleration_width + waves_width
        z = polysegment(
            [
                (0, 0),
                (width, 0),
                (0, width),
            ],
            closed=True,
        )
        z = z.fill()
        z = z.extrude(level_height)
        deepening = cylinder(deepening_radius, deepening_length)
        deepening = deepening.rotateX(-pi/2)
        deepening = deepening.up(level_height / 2)
        deepening = deepening.right(acceleration_width) + deepening.rotateZ(-pi/2).forw(waves_width)
        z -= deepening
        return z

    z = create_level()
    z = union([z.up(level_height * i) for i in range(levels_count)])
    return z


display(create_test())
show()
