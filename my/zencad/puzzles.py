from utils import *
from random import *

register_font('utils/roboto.ttf')  # textshape(str(number), "roboto", text_length)

clearance = 0.3
border = 12
base_sbtn_corner_radius = 16
base_height = 4.8
part_height = 4.8
part_size = 72
common_fillet = 1.6
elefant_leg_chamfer = 0.4


def create_pin_half_besier_points():
    pin_base_x_offset = 8
    pin_length = 14
    raw = [
        (pin_base_x_offset, 0),
        (4, tech),
        (1, 4),
        (1, 5),
        (1, 7),
        (8, 7),
        (8, 9),
        (8, 11),
        (2, pin_length-tech),
        (0, pin_length)  #
    ]

    pin_target_part_size = 40
    pin_scale = part_size / pin_target_part_size

    z = []
    for (x, y) in raw:
        z.append((x * pin_scale, y * pin_scale))

    return pin_scale, pin_base_x_offset * pin_scale, pin_length * pin_scale, z


pin_scale, pin_base_x_offset, pin_length, pin_half_besier_points = create_pin_half_besier_points()

pin_randomize_min = 1
pin_randomize_max = 3


def create_parts(
        seed: int,
        x_count: int,
        y_count: int
):
    height = base_height + clearance + part_height
    part_step = part_size + clearance

    seed_random = Random(seed)

    def create_pin():
        points = []
        for (x, y) in pin_half_besier_points:
            points.append((x, y - tech))
        points2 = reversed(points[:-1])
        for (x, y) in points2:
            points.append((-x, y))
        points3 = []

        def get_offset():
            z = pin_randomize_min + (pin_randomize_max - pin_randomize_min) * seed_random.random()
            if seed_random.random() < 0.5:
                z = -z
            return z * pin_scale

        for i in range(len(points)):
            point = points[i]
            if i < 2 or i > len(points) - 3:
                points3.append(point)
            else:
                (x, y) = point
                points3.append(
                        (
                            x + get_offset(),
                            y + get_offset()
                        )
                )

        z = bezier(points3)
        deeping = tech * 2
        x_offset = pin_base_x_offset
        z = sew(
                [
                    z,
                    segment(
                            (-x_offset, -tech),
                            (-x_offset, -deeping)
                    ),
                    segment(
                            (-x_offset, -deeping),
                            (x_offset, -deeping)
                    ),
                    segment(
                            (x_offset, -deeping),
                            (x_offset, -tech)
                    )
                ]
        )
        z = z.fill()
        return z

    def create_horizontal_pin(x, y):
        z = create_pin()
        if x == 0:
            z = z.rotateZ(-pi / 2).right(-clearance)
        else:
            z = z.rotateZ(pi / 2).right(x * part_step)
        z = z.forw((y + 0.5) * part_step)
        return z

    horizontal_pins = [[create_horizontal_pin(x, y) for y in range(y_count)] for x in range(x_count + 1)]

    def create_vertical_pin(x, y):
        z = create_pin()
        if y == 0:
            z = z.forw(-clearance)
        else:
            z = z.rotateZ(pi).forw(y * part_step)
        z = z.right((x + 0.5) * part_step)
        return z

    vertical_pins = [[create_vertical_pin(x, y) for y in range(y_count + 1)] for x in range(x_count)]

    def create_base():
        base_sbtn_width = x_count * part_step + clearance
        base_sbtn_length = y_count * part_step + clearance
        base_width = base_sbtn_width + border * 2
        base_length = base_sbtn_length + border * 2
        z = box(base_width, base_length, height)
        z = z.fillet(
                base_sbtn_corner_radius + clearance + border,
                [
                    (0, 0, height / 2),
                    (base_width, 0, height / 2),
                    (0, base_length, height / 2),
                    (base_width, base_length, height / 2)
                ]
        )
        z = z.translate(
                -clearance - border,
                -clearance - border
        )
        sbtn_height = height - part_height
        sbtn = box(base_sbtn_width, base_sbtn_length, sbtn_height)
        sbtn = sbtn.fillet(
                base_sbtn_corner_radius + clearance,
                [
                    (0, 0, sbtn_height / 2),
                    (base_sbtn_width, 0, sbtn_height / 2),
                    (0, base_sbtn_length, sbtn_height / 2),
                    (base_sbtn_width, base_sbtn_length, sbtn_height / 2)
                ]
        )
        sbtn = sbtn.translate(
                -clearance,
                -clearance,
                base_height
        )
        z -= sbtn
        pins = []
        for x in range(x_count):
            pins.append(vertical_pins[x][0])
            pins.append(vertical_pins[x][y_count])
        for y in range(y_count):
            pins.append(horizontal_pins[0][y])
            pins.append(horizontal_pins[x_count][y])
        pins = union(pins)
        pins = pins.extrude(height)
        z += pins
        z = unify(z)
        z = z.fillet(
                common_fillet,
                [
                    ((base_sbtn_width - clearance) / 2, -clearance - border, height)
                ]
        )
        z = z.chamfer(
                elefant_leg_chamfer,
                [((base_sbtn_width - clearance) / 2, -clearance - border, 0)]
        )
        return z

    def create_part(
            x: int,
            y: int
    ):
        z = box(part_size, part_size, part_height)

        big_corner_points = []
        if x == 0:
            if y == 0:
                big_corner_points.append((0, 0, part_height / 2))
            if y == y_count - 1:
                big_corner_points.append((0, part_size, part_height / 2))
        if x == x_count - 1:
            if y == 0:
                big_corner_points.append((part_size, 0, part_height / 2))
            if y == y_count - 1:
                big_corner_points.append((part_size, part_size, part_height / 2))

        if len(big_corner_points) > 0:
            z = z.fillet(base_sbtn_corner_radius, big_corner_points)

        z = z.translate(x * part_step, y * part_step)

        sbtn_pins = [
            horizontal_pins[x + 1][y],
            vertical_pins[x][y + 1]
        ]
        if x == 0:
            sbtn_pins.append(horizontal_pins[x][y])
        if y == 0:
            sbtn_pins.append(vertical_pins[x][y])
        sbtn_pins = list(map(lambda pin: offset(pin.extrude(part_height + tech * 2).down(tech), clearance), sbtn_pins))
        sbtn_pins = union(sbtn_pins)
        z -= sbtn_pins

        add_pins = []
        if x > 0:
            add_pins.append(horizontal_pins[x][y])
        if y > 0:
            add_pins.append(vertical_pins[x][y])
        if len(add_pins) > 0:
            add_pins = union(add_pins)
            add_pins = add_pins.extrude(part_height)
            z += add_pins

        z = z.up(height - part_height)

        return z

    parts = [create_part(x, y) for x in range(x_count) for y in range(y_count)]
    return create_base(), parts


(base, parts) = create_parts(0,2 , 2)

parts_dict = {
    "base": base,
    "all_parts": union(parts)
}
for i in range(len(parts)):
    parts_dict[f"part_{i + 1}"] = parts[i]
export_to_stl(parts_dict)

display(base, Color(1, 1, 1))
colors = [
    Color(1, 0, 0),
    Color(0, 1, 0),
    Color(0, 0, 1),
    Color(1, 1, 0),
    Color(0, 1, 1),
    Color(1, 0, 1)
]
for i in range(len(parts)):
    part = parts[i]
    display(part.up(part_height + (i + 1) * 10), colors[i % len(colors)])

show()
