from utils import *

wall = 1.6
clearance = 0.4
content_length = 50
content_height = 30
content_width = 70
screw_radius = 3.6
screw_length = 4.8
screw_depth = 1.2
leg_length = 6.4
hanging_angle = pi / 3
wire_hole_radius = 3

# Calculated
screw_out_radius = screw_radius + clearance * sqrt(2)
nut_radius = screw_out_radius + wall
screw_offset = nut_radius

width = wall + content_width + wall
length = wall + content_length + wall
height = wall + content_height + wall



def create_voronoi_sbtn():
    margin = wall / 2 + wall / 2
    y_offset = length / 2 - margin
    x_offset = width / 2 - margin
    z = voronoi_sbtn(
            seed=1,
            polygon_points=[
                (x_offset, y_offset),
                (-x_offset, y_offset),
                (-x_offset, -y_offset),
                (x_offset, -y_offset)
            ],
            height=wall,
            separation=wall,
            fillet_radius=wall
    )
    return z


voronoi_sbtn = create_voronoi_sbtn()


def create_base():
    base_height = height - clearance * sqrt(2)
    z = box(width, length, base_height)
    z = z.translate(-width / 2, -length / 2)
    z = z.fillet(
            screw_offset,
            [
                (width / 2, length / 2, base_height / 2),
                (-width / 2, length / 2, base_height / 2),
                (width / 2, -length / 2, base_height / 2),
                (-width / 2, -length / 2, base_height / 2)
            ]
    )
    z = thicksolid(z, -wall, [(0, 0, base_height)])
    z = z.chamfer(wall - tech, [(0, -length / 2 + wall, base_height)])
    z -= voronoi_sbtn
    nut_height = screw_length + clearance + wall + nut_radius * sqrt(2)
    nut = cylinder(nut_radius, nut_height)
    nut_handle = box(nut_radius, nut_radius, nut_height)
    nut += nut_handle
    nut += nut_handle.rotateZ(pi)
    screw_sbtn_height = screw_length + clearance
    screw_sbtn = screw(screw_out_radius, screw_sbtn_height, screw_depth)
    screw_sbtn += cone(0, screw_out_radius, screw_out_radius).up(screw_sbtn_height - screw_out_radius)
    screw_sbtn = screw_sbtn.up(nut_height - screw_sbtn_height)
    nut -= screw_sbtn
    nut -= halfspace().rotateX(pi / 4).rotateZ(pi / 4).up(nut_radius)
    nut = nut.up(base_height - nut_height - wall)
    nuts = nut.rotateZ(pi / 2).translate(width / 2 - screw_offset, length / 2 - screw_offset)
    nuts += nut.translate(width / 2 - screw_offset, -length / 2 + screw_offset)
    nuts += nuts.rotateZ(pi)
    z += nuts
    nuts_fillet_offset = screw_offset + nut_radius
    nuts_fillet_z = base_height - wall - tech
    z = z.fillet(
            nut_radius - wall - tech,
            [
                (width / 2 - wall, length / 2 - nuts_fillet_offset, nuts_fillet_z),
                (width / 2 - nuts_fillet_offset, length / 2 - wall, nuts_fillet_z),
                (-width / 2 + wall, length / 2 - nuts_fillet_offset, nuts_fillet_z),
                (-width / 2 + nuts_fillet_offset, length / 2 - wall, nuts_fillet_z),
                (width / 2 - wall, -length / 2 + nuts_fillet_offset, nuts_fillet_z),
                (width / 2 - nuts_fillet_offset, -length / 2 + wall, nuts_fillet_z),
                (-width / 2 + wall, -length / 2 + nuts_fillet_offset, nuts_fillet_z),
                (-width / 2 + nuts_fillet_offset, -length / 2 + wall, nuts_fillet_z)
            ]
    )
    wire_sbtn = cylinder(wire_hole_radius, wall)
    wire_sbtn += box(wall + wire_hole_radius, wire_hole_radius * 2, wall).back(wire_hole_radius)
    wire_sbtn = wire_sbtn.rotateY(pi / 2)
    wire_sbtn = wire_sbtn.rotateX(pi)
    wire_sbtn = wire_sbtn.translate(width / 2 - wall, 0, height - wall - wire_hole_radius - clearance)
    z -= wire_sbtn
    return z


def create_cap():
    cap_width = width
    cap_length = length
    z = box(cap_width, cap_length, wall)
    z = z.translate(-cap_width / 2, -cap_length / 2)
    z = z.fillet(
            screw_offset - clearance * sqrt(2),
            [
                (cap_width / 2, cap_length / 2, wall / 2),
                (-cap_width / 2, cap_length / 2, wall / 2),
                (cap_width / 2, -cap_length / 2, wall / 2),
                (-cap_width / 2, -cap_length / 2, wall / 2)
            ]
    )
    z = z.chamfer(wall - tech, [(0, -cap_length / 2, 0)])
    corner = cylinder(screw_out_radius + wall, wall)
    corner = corner.translate(width / 2 - screw_offset, length / 2 - screw_offset)
    corner ^= z
    corner += corner.mirrorXZ()
    corner += corner.mirrorYZ()
    z -= voronoi_sbtn
    z += corner
    corner_sbtn = cylinder(screw_out_radius, wall)
    corner_sbtn = corner_sbtn.translate(width / 2 - screw_offset, length / 2 - screw_offset)
    corner_sbtn += corner_sbtn.mirrorXZ()
    corner_sbtn += corner_sbtn.mirrorYZ()
    z -= corner_sbtn
    z = unify(z)
    z = z.up(height - wall)
    return z


def create_leg():
    radius = screw_out_radius + wall
    z = cylinder(radius, leg_length)
    z = z.chamfer(wall, [(0, -radius, leg_length)])
    screw_sbtn_height = screw_length + clearance
    screw_sbtn = screw(screw_out_radius, screw_sbtn_height, screw_depth)
    z -= screw_sbtn
    z -= cone(screw_out_radius+tech, 0, screw_out_radius+tech)
    z = z.up(height + clearance)
    return z


def create_screw():
    length = screw_length + wall + screw_length
    z = cylinder(screw_radius, length)
    z = z.chamfer(
            screw_depth + tech,
            [
                (0, -screw_radius, 0),
                (0, -screw_radius, length)
            ]
    )
    z = screw(screw_radius, length, screw_depth, blank=z)
    cut_offset = (screw_radius - screw_depth) * sin(hanging_angle)
    z -= halfspace().rotateX(-pi / 2).back(cut_offset)
    z -= halfspace().rotateX(pi / 2).forw(cut_offset)
    z = z.up(height - wall - screw_length)
    return z


base = create_base()
cap = create_cap()
leg = create_leg()
screw = create_screw()

export_to_stl(
        {
            "base": base,
            "cap": cap.rotateX(pi).rotateZ(pi),
            "leg": leg.rotateX(pi),
            "screw": screw.rotateX(pi / 2),
        }
)


def copy_to_corners(shape):
    z = shape.translate(width / 2 - screw_offset, length / 2 - screw_offset)
    z += shape.translate(-width / 2 + screw_offset, length / 2 - screw_offset)
    z += z.rotateZ(pi)
    return z


istn = halfspace().rotateX(pi / 2).back(length)

display(base ^ istn, Color(1, 1, 0))
display(cap.up(10) ^ istn, Color(0, 1, 1))
display(copy_to_corners(leg).up(20) ^ istn, Color(1, 0, 1))
display(copy_to_corners(screw).up(10) ^ istn, Color(1, 1, 1))
show()
