from utils import *

base_width = 26.0
base_length = 34.0
base_height = 25.0
base_bezier_point_1_dx = 10
base_bezier_point_1_dy = 32
base_bezier_point_2_dx = -5
base_bezier_point_2_dy = 32
base_chamfer = 1.2
base_hole_enter_fillet = 0.8
base_side_angle = pi / 15
hole_dz = 16.0
hole_dy = 11.0
hole_radius = 3.5
hole_back_gap_height = 2.0
hole_back_gap_length = 11.0
hole_front_gap_height = 6.0
legs_height = 7.2
legs_width = 7
near_leg_length = 3
near_leg_dy = 7.5
far_leg_length = 8
legs_separation = 6.5
legs_fillet = 1.2

coupler_width = 5.4
coupler_height = 2
coupler_cap_width = 9
coupler_cap_height = 6.5
coupler_cap_length = 8
coupler_cap_hole_dy = 2.5
coupler_cap_z = 3.2
coupler_exit_radius = 6


def create_car_hook():
    z = sew(
        [
            segment((0, 0), (-base_length, 0)),
            bezier(
                [
                    (-base_length, 0),
                    (-base_length + base_bezier_point_1_dx, base_bezier_point_1_dy),
                    (base_bezier_point_2_dx, base_bezier_point_2_dy),
                    (0, 0),
                ]
            ),
        ]
    )
    z = z.fill()
    z = z.extrude(base_width)
    z = z.rotateX(pi / 2)
    z = z.rotateZ(-pi / 2)
    z = z.right(base_width / 2)
    z -= halfspace() \
        .rotateY(-pi / 2 - base_side_angle) \
        .right(base_width / 2)
    z -= halfspace() \
        .rotateY(pi / 2 + base_side_angle) \
        .left(base_width / 2)

    z = chamfer(
        z,
        base_chamfer,
        [
            (base_width / 2, base_length / 2, base_bezier_point_1_dy),
            (-base_width / 2, base_length / 2, base_bezier_point_1_dy),
        ]
    )

    def create_hole():
        z = cylinder(hole_radius, base_width)
        z = z.down(base_width / 2)
        z = z.rotateY(pi / 2)
        z = z.forw(hole_dy)
        z = z.up(hole_dz)

        def create_back_gap():
            z = box(base_width, hole_back_gap_length, hole_back_gap_height)
            z = z.fillet(
                hole_back_gap_height / 2 - tech,
                [
                    (base_width / 2, hole_back_gap_length, 0),
                    (base_width / 2, hole_back_gap_length, hole_back_gap_height)
                ]
            )
            z = z.translate(
                -base_width / 2,
                hole_dy,
                hole_dz - hole_back_gap_height / 2
            )
            return z

        z += create_back_gap()

        def create_front_gap():
            z = box(base_width, hole_dy, hole_front_gap_height)
            z = z.translate(
                -base_width / 2,
                0,
                hole_dz - hole_front_gap_height / 2
            )
            return z

        z += create_front_gap()
        return z

    z -= create_hole()

    z = z.fillet(
        base_hole_enter_fillet,
        [
            (0, hole_dy - hole_radius, hole_dz + hole_front_gap_height / 2),
            (0, hole_dy - hole_radius, hole_dz - hole_front_gap_height / 2),
            (0, hole_dy / 2, hole_dz + hole_front_gap_height / 2),
            (0, 0, hole_dz - hole_front_gap_height / 2),
        ],
    )

    def create_legs():
        near = rectangle(
            legs_width - near_leg_length,
            near_leg_length,
        )
        near = near.right(near_leg_length / 2)
        near += circle(near_leg_length / 2) \
            .forw(near_leg_length / 2) \
            .right(near_leg_length / 2)
        near += circle(near_leg_length / 2) \
            .forw(near_leg_length / 2) \
            .right(legs_width - near_leg_length / 2)
        far = rectangle(
            legs_width,
            far_leg_length,
        )
        far = far.forw(near_leg_length + legs_separation)
        z = near + far
        z = z.extrude(legs_height)
        z = fillet(
            z,
            legs_fillet,
            [
                (0, near_leg_length + legs_separation, legs_height / 2),
                (0, near_leg_length + legs_separation + far_leg_length, legs_height / 2),
                (legs_width, near_leg_length + legs_separation, legs_height / 2),
                (legs_width, near_leg_length + legs_separation + far_leg_length, legs_height / 2),
            ]
        )
        z = z.translate(
            -legs_width / 2,
            near_leg_dy,
            -legs_height
        )
        return z

    z += create_legs()

    def create_coupler_sbtn():
        coupler_far_leg_enter_y = near_leg_dy + near_leg_length + legs_separation + far_leg_length - coupler_height
        coupler_cap_y = coupler_far_leg_enter_y - coupler_cap_length + coupler_cap_hole_dy
        z = box(
            coupler_cap_width,
            base_length - coupler_cap_y,
            coupler_cap_height,
        )
        z = z.translate(
            -coupler_cap_width / 2,
            coupler_cap_y,
            coupler_cap_z,
        )
        legs_sbtn_y = near_leg_dy + near_leg_length - coupler_height
        legs_sbtn = box(
            coupler_width,
            base_length-legs_sbtn_y,
            legs_height,
        )
        legs_sbtn = legs_sbtn.translate(
            -coupler_width / 2,
            legs_sbtn_y,
            -legs_height
        )
        z += legs_sbtn

        far_leg_enter = box(
            coupler_width,
            coupler_height,
            coupler_cap_z,
        )
        far_leg_enter = far_leg_enter.translate(
            -coupler_width / 2,
            coupler_far_leg_enter_y,
            0
        )
        z += far_leg_enter

        def create_path_to_near_leg():
            near_leg_enter_y = near_leg_dy + near_leg_length - coupler_height
            length = coupler_cap_y - near_leg_enter_y
            width = coupler_width
            height = coupler_cap_z + coupler_height
            z = circle(1)\
                .scaleX(length)\
                .scaleY(height)
            z -= circle(1) \
                .scaleX(length-coupler_height) \
                .scaleY(height-coupler_height)
            z ^= rectangle(length, height).left(length)
            z = z.extrude(width)
            z = z.rotateX(pi/2)
            z = z.forw(coupler_width / 2)
            z = z.rotateZ(pi / 2)
            z = z.forw(near_leg_enter_y+length)

            return z

        z += create_path_to_near_leg()

        def create_exit():
            z = circle(coupler_exit_radius)
            z -= circle(coupler_exit_radius-coupler_height)
            z = z.extrude(coupler_width)
            z = z.down(coupler_width/2)
            z = z.rotateX(pi/2)
            z = z.rotateZ(pi/2)
            z -= halfspace().rotateX(pi/2)
            z -= halfspace()
            z = z.forw(coupler_far_leg_enter_y+coupler_exit_radius)
            z = z.up(coupler_cap_z+coupler_cap_height)
            return z

        z += create_exit()

        return z

    z -= create_coupler_sbtn()

    return z


car_hook = create_car_hook()
#car_hook -= halfspace().rotateY(-pi / 2).left(tech)

display(car_hook)
show()
