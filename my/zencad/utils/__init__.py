import shutil
from enum import Enum
from math import *
from random import *

from foronoi import Voronoi, Polygon, Coordinate
from zencad import *

tech: float = 0.01
fake_infinity: float = 1000.0


class ScrewSide(Enum):
    DEFAULT = 0
    CHAMFER = 1
    EXTENSION = 2


def screw(
        r: float,
        h: float,
        depth: float = 1.2,
        angle: float = pi / 2,
        entries: int = 1,
        top_side: ScrewSide = ScrewSide.DEFAULT,
        bottom_side: ScrewSide = ScrewSide.DEFAULT,
):
    tech = 0.001
    pattern_height_half = depth * tan(angle / 2)
    tech_z = tech * tan(angle / 2)
    pattern = polysegment(
        points([
            (tech, 0, pattern_height_half + tech_z),
            (-depth, 0, 0),
            (tech, 0, -pattern_height_half - tech_z)
        ]),
        closed=True
    )
    pattern = pattern.right(r)
    sbtn = helix(
        r=r,
        h=h + pattern_height_half * 2 + tech * 2,
        step=pattern_height_half * 2 * entries + tech * entries
    )
    sbtn = pipe_shell(
        spine=sbtn,
        arr=[pattern],
        frenet=True,
    )
    sbtn = sbtn.down(pattern_height_half + tech)
    sbtn_rotate_step = pi * 2 / entries
    sbtn = union([sbtn.rotateZ(sbtn_rotate_step * i) for i in range(entries)])
    cyliner_height = h

    if bottom_side == ScrewSide.EXTENSION:
        cyliner_height -= tech

    if top_side == ScrewSide.EXTENSION:
        cyliner_height -= tech

    z = cylinder(r, cyliner_height)

    if bottom_side == ScrewSide.EXTENSION:
        z = z.up(tech)

    if bottom_side == ScrewSide.CHAMFER:
        z = z.chamfer(depth, [(r, 0, 0)])
    if top_side == ScrewSide.CHAMFER:
        z = z.chamfer(depth, [(r, 0, h)])
    z -= sbtn

    if bottom_side == ScrewSide.EXTENSION:
        z += cone(
            r1=r,
            r2=r - depth,
            h=depth,
        )

    if top_side == ScrewSide.EXTENSION:
        z += cone(
            r1=r - depth,
            r2=r,
            h=depth,
        ).up(h - depth)

    return z


def export_to_stl(
        shapes: dict[str, Shape],
        directory="./stl",
        delta=0.01,
):
    print("exporting")
    stl_directory_was_initialized = False

    def initialize_stl_directory_if_need():
        nonlocal stl_directory_was_initialized
        if stl_directory_was_initialized:
            return
        if os.path.exists(directory):
            shutil.rmtree(directory)
        os.mkdir(directory)
        stl_directory_was_initialized = True

    for name, shape in shapes.items():
        initialize_stl_directory_if_need()
        to_stl_name = directory + "/" + name + ".stl"
        to_stl(shape, to_stl_name, delta)
        print(name, "exported as", to_stl_name)


def voronoi_sbtn(
        seed,
        polygon_points: [(float, float)],
        height: float,
        separation: float,
        fillet_radius: float,
        point_area: float = 75,
        smooth_steps: int = 100,
        smooth_step_factor: float = 0.1
):
    random = Random(seed)
    polygon = Polygon(polygon_points)

    min_x = float(polygon.min_x)
    max_x = float(polygon.max_x)
    min_y = float(polygon.min_y)
    max_y = float(polygon.max_y)
    points_count = floor((max_x - min_x) * (max_y - min_y) / point_area)

    def generate_points():
        point_margin = sqrt(point_area) / 2
        point_min_x = min_x + point_margin
        point_max_x = max_x - point_margin
        point_min_y = min_y + point_margin
        point_max_y = max_y - point_margin
        points = []
        for i in range(points_count):
            x = point_min_x + random.random() * (point_max_x - point_min_x)
            y = point_min_y + random.random() * (point_max_y - point_min_y)
            points.append((x, y))

        for i in range(smooth_steps):

            min_x_point_x = None
            max_x_point_x = None
            min_y_point_y = None
            max_y_point_y = None
            for point in points:
                (x, y) = point
                if not min_x_point_x or x < min_x_point_x:
                    min_x_point_x = x
                if not max_x_point_x or x > max_x_point_x:
                    max_x_point_x = x
                if not min_y_point_y or x < min_y_point_y:
                    min_y_point_y = x
                if not max_y_point_y or x > max_y_point_y:
                    max_y_point_y = x

            for i in range(len(points)):
                point = points[i]
                (x, y) = point
                if x <= min_x_point_x or x >= max_x_point_x or y <= min_y_point_y or y >= max_y_point_y:
                    continue

                nearest = None
                nearset_distance = None
                for other_point in points:
                    if other_point != point:
                        (other_x, other_y) = other_point
                        distance = sqrt(pow(x - other_x, 2) + pow(y - other_y, 2))
                        if not nearset_distance or distance < nearset_distance:
                            nearset_distance = distance
                            nearest = other_point

                if nearest:
                    (nearest_x, nearest_y) = nearest
                    new_x = x + (x - nearest_x) * smooth_step_factor
                    new_y = y + (y - nearest_y) * smooth_step_factor
                    if min_x <= new_x <= max_x and min_y <= new_y <= max_y:
                        points[i] = (new_x, new_y)

        result = []

        for point in points:
            (x, y) = point
            if polygon.inside(Coordinate(x, y)):
                result.append(point)

        return result

    v = Voronoi(polygon)
    v.create_diagram(generate_points())

    edges = []
    for edge in v.edges:
        start_x = edge.origin.x
        start_y = edge.origin.y
        end_x = edge.twin.origin.x
        end_y = edge.twin.origin.y
        center_x = (start_x + end_x) / 2
        center_y = (start_y + end_y) / 2
        length = sqrt(pow(start_x - end_x, 2) + pow(start_y - end_y, 2))
        angle = atan2((start_x - end_x), (start_y - end_y))
        z = rectangle(fillet_radius + separation + fillet_radius, length, True)
        z = z.rotateZ(-angle)
        z = z.translate(center_x, center_y)
        edges.append(z)

    edges = union(edges)
    edges = unify(edges)

    polygon_face = polysegment(polygon_points, closed=True)
    polygon_face = polygon_face.fill()
    z = polygon_face - edges
    z = z.extrude(height + tech * 2).down(tech)

    z = z.solids()
    z = union([offset(solid, fillet_radius) for solid in z])
    return z


def create_chamfer_fillet_dimensions(height: float):
    z = height / (1 + sqrt(2))
    fillet_radius = (height - z) / tan(pi / 8)
    return z, fillet_radius


def gen_maze(width, length, height, cell_size, tube_percentage, seed=0):
    cells_count = width * length * height

    ready_cells = [False] * cells_count
    queue = set([])

    def index(x, y, z):
        return (z * length + y) * width + x

    random = Random(seed)

    def get_neighbours(x, y, z):
        res = []
        if x > 0:
            res.append((x - 1, y, z))
        if x < width - 1:
            res.append((x + 1, y, z))
        if y > 0:
            res.append((x, y - 1, z))
        if y < length - 1:
            res.append((x, y + 1, z))
        if z > 0:
            res.append((x, y, z - 1))
        if z < height - 1:
            res.append((x, y, z + 1))
        return res

    def add_ready_cell(x, y, z):
        ready_cells[index(x, y, z)] = True
        for neighbour in get_neighbours(x, y, z):
            nx, ny, nz = neighbour
            if not ready_cells[index(nx, ny, nz)]:
                queue.add(neighbour)

    add_ready_cell(int(width / 2), int(length / 2), int(height / 2))

    tube_radius = cell_size * tube_percentage / 2
    res = []
    cell_cap = sphere(tube_radius)
    for ix in range(width):
        for iy in range(length):
            for iz in range(height):
                res.append(
                    cell_cap.translate(
                        ix * cell_size,
                        iy * cell_size,
                        iz * cell_size,
                    )
                )

    hole = cylinder(tube_radius, cell_size)

    def add_hole(x1, y1, z1, x2, y2, z2):

        def add_hole(hx, hy, hz, direction):
            match direction:
                case 0:
                    h = hole.rotateY(pi / 2)
                case 1:
                    h = hole.rotateX(-pi / 2)
                case _:
                    h = hole
            h = h.translate(hx * cell_size, hy * cell_size, hz * cell_size)
            nonlocal res
            res.append(h)
            # i = index(x, y, res)
            # hx, hy, hz = holes[i]
            # holes[i] = (
            #     hx or direction == 0,
            #     hy or direction == 1,
            #     hz or direction == 2,
            # )

        if x1 < x2:
            add_hole(x1, y1, z1, 0)
            return

        if x2 < x1:
            add_hole(x2, y1, z1, 0)
            return

        if y1 < y2:
            add_hole(x1, y1, z1, 1)
            return

        if y2 < y1:
            add_hole(x1, y2, z1, 1)
            return

        if z1 < z2:
            add_hole(x1, y1, z1, 2)
            return

        if z2 < z1:
            add_hole(x1, y1, z2, 2)
            return

    while len(queue) > 0:
        queue_list = list(queue)
        next_cell = queue_list[random.randint(0, len(queue_list) - 1)]
        queue.remove(next_cell)
        x, y, z = next_cell
        neighbours = get_neighbours(x, y, z)
        random.shuffle(neighbours)
        added = False
        i = 0
        while not added and i < len(neighbours):
            nx, ny, nz = neighbours[i]
            if ready_cells[index(nx, ny, nz)]:
                added = True
                add_hole(x, y, z, nx, ny, nz)
                add_ready_cell(x, y, z)
            i += 1
    return union(res)


display_colors = [
    Color(0xF4 / 255, 0x43 / 255, 0x36 / 255),
    Color(0xE9 / 255, 0x1E / 255, 0x63 / 255),
    Color(0x9C / 255, 0x27 / 255, 0xB0 / 255),
    Color(0x67 / 255, 0x3A / 255, 0xB7 / 255),
    Color(0x3F / 255, 0x51 / 255, 0xB5 / 255),
    Color(0x21 / 255, 0x96 / 255, 0xF3 / 255),
    Color(0x03 / 255, 0xA9 / 255, 0xF4 / 255),
    Color(0x00 / 255, 0xBC / 255, 0xD4 / 255),
    Color(0x00 / 255, 0x96 / 255, 0x88 / 255),
    Color(0x4C / 255, 0xAF / 255, 0x50 / 255),
    Color(0x8B / 255, 0xC3 / 255, 0x4A / 255),
    Color(0xCD / 255, 0xDC / 255, 0x39 / 255),
    Color(0xFF / 255, 0xEB / 255, 0x3B / 255),
    Color(0xFF / 255, 0xC1 / 255, 0x07 / 255),
    Color(0xFF / 255, 0x98 / 255, 0x00 / 255),
    Color(0xFF / 255, 0x57 / 255, 0x22 / 255),
    Color(0x79 / 255, 0x55 / 255, 0x48 / 255),
]


def choose_color_to_display(
        index: int,
) -> Color:
    return display_colors[index % len(display_colors)]
