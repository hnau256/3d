from utils import *

random = Random(4)

max_cell_size: float = 12.0
line_width: float = 1.2
thickness: float = 0.2


def create_test(
        width: float,
        height: float,
) -> Shape:
    x_count: int = int(ceil((width - line_width) / max_cell_size))
    x_step: float = (width - line_width) / x_count
    y_count: int = int(ceil((height - line_width) / max_cell_size))
    y_step: float = (height - line_width) / y_count

    min_x = -(width - line_width) / 2
    min_y = -(height - line_width) / 2

    def calc_x(ix: int) -> float:
        return min_x + ix * x_step

    def calc_y(iy: int) -> float:
        return min_y + iy * y_step

    z: [Shape] = []
    cross_circle = circle(line_width / 2)

    for ix in range(x_count + 1):
        for iy in range(y_count + 1):
            z.append(
                cross_circle.translate(
                    calc_x(ix),
                    calc_y(iy),
                )
            )

    def calc_maze() -> [(int, int, bool)]:
        w = x_count + 1
        h = y_count + 1
        DIRECTIONS = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        def is_valid(nx, ny):
            return 0 <= nx < w and 0 <= ny < h and not visited[nx][ny]

        def shuffle_directions():
            random.shuffle(DIRECTIONS)

        visited = [[False] * h for _ in range(w)]
        walls = []

        def dfs(x, y):
            visited[x][y] = True
            shuffle_directions()

            for dx, dy in DIRECTIONS:
                nx, ny = x + dx, y + dy
                if is_valid(nx, ny):
                    if dx == 0:
                        walls.append((x, min(y, ny), False))
                    else:
                        walls.append((min(x, nx), y, True))
                    dfs(nx, ny)

        dfs(
            random.randint(0, w),
            random.randint(0, h)
        )

        return walls

    maze = calc_maze()
    for segment in maze:
        ix, iy, horizontal = segment
        x = calc_x(ix)
        y = calc_y(iy)
        z.append(
            rectangle(
                x_step if horizontal else line_width,
                line_width if horizontal else y_step
            ).translate(
                x - (0 if horizontal else line_width / 2),
                y - (line_width / 2 if horizontal else 0),
            )
        )

    z = union(z)
    z = unify(z)
    z = z.extrude(thickness)
    return z


test = create_test(
    width=205,
    height=215,
)
disp(test)
export_to_stl(
    shapes={
        "bed_level_test": test
    }
)

show()
