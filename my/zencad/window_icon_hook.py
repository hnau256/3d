from utils import *

beam_depth = 23
beam_height = 35

wall = 4
width = 12
shelf_length = 32
shelf_hook_length = 6.4
hook_length = 1.2


def create_window_icon_hook():
    wall2_radius = wall / 2 - tech
    deltas_with_radiuses = [
        (shelf_length, 0, 0),
        (0, shelf_hook_length, wall2_radius),
        (wall, 0, wall2_radius),
        (0, -shelf_hook_length - wall, wall),
        (-wall - shelf_length + beam_depth + wall, 0, wall),
        (0, -beam_height - wall, wall),
        (-wall - beam_depth, 0, 0),
        (0, wall + hook_length, 0),
        (wall, 0, 0),
        (0, -hook_length, hook_length - tech),
        (beam_depth - wall, 0, wall/2),
        (0, beam_height, wall/2),
        (-beam_depth + wall, 0, hook_length - tech),
        (0, -hook_length, 0),
        (-wall, 0, 0),
    ]
    current = (0, 0, 0)
    points_with_radiuses = [current]
    for delta in deltas_with_radiuses:
        dx, dy, radius = delta
        x, y, r = current
        x += dx
        y += dy
        current = (x, y, radius)
        points_with_radiuses.append(current)

    points = []
    for point_with_radius in points_with_radiuses:
        x, y, r = point_with_radius
        points.append((x, y))

    z = polygon(
        pnts=points,
        wire=True
    )
    z = z.fill()
    z = z.extrude(width)
    for point_with_radius in points_with_radiuses:
        x, y, radius = point_with_radius
        if radius > 0:
            z = z.fillet(radius, [(x, y, width / 2)])
    return z


window_icon_hook = create_window_icon_hook()
display(window_icon_hook)
show()
