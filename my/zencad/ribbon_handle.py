from utils import *

ribbon_width = 31
ribbon_height = 3
ribbon_length = 32
wall = 2.4
screw_min_radius = 2.5
screw_max_radius = 5
hole_min_radius = 1
hole_max_radius = 2
holes_separation = 5
tech = 0.001


def create_handle():
    length = ribbon_length + wall
    screw_x_offset = ribbon_width / 2 + wall + screw_min_radius
    screw_y_offset = wall + screw_max_radius
    width = (screw_x_offset + screw_max_radius + wall) * 2
    height = ribbon_height + wall
    z = box(width, length, height)
    z = z.left(width / 2)
    z = z.fillet(
            screw_y_offset,
            [
                (width / 2, 0, height / 2),
                (width / 2, length, height / 2),
                (-width / 2, 0, height / 2),
                (-width / 2, length, height / 2)
            ]
    )
    ribbon_sbtn = box(ribbon_width, ribbon_length, ribbon_height)
    ribbon_sbtn = ribbon_sbtn.left(ribbon_width / 2)
    z -= ribbon_sbtn
    screw_sbtn = cylinder(screw_min_radius, height)
    screw_sbtn += cone(0, screw_max_radius, screw_max_radius).up(height - screw_max_radius)
    screw_sbtn = screw_sbtn.right(screw_x_offset)
    screw_sbtn += screw_sbtn.rotateZ(pi)
    screw_sbtn = screw_sbtn.forw(screw_y_offset) + screw_sbtn.forw(length - screw_y_offset)
    z -= screw_sbtn
    hole = cylinder(hole_min_radius, wall)
    hole_cap = cone(hole_max_radius, 0, hole_max_radius)
    hole += hole_cap
    hole += hole_cap.rotateX(pi).up(wall)
    holes_x_count = int(floor(ribbon_width / holes_separation))
    holes_x_separation = ribbon_width / holes_x_count
    holes_line = union([hole.right(i * holes_x_separation) for i in range(holes_x_count)])
    holes_y_count = int(floor(ribbon_length / holes_separation))
    holes_y_separation = ribbon_width / holes_y_count
    holes = union([holes_line.forw(i * holes_y_separation) for i in range(holes_y_count)])
    holes = holes.translate(
            -(holes_x_count / 2 - 0.5) * holes_x_separation,
            holes_y_separation / 2,
            ribbon_height
    )
    z -= holes
    return z


handle = create_handle()

export_to_stl(
        {
            "handle": handle
        }
)

display(handle)
show()
