#!/usr/bin/env python3
# coding: utf-8

from utils import *

clearance: float = 0.2
size: float = 32.5
wall: float = 2.4
screw_height: float = 7.2
screw_depth: float = 1.2
screw_shrinkage: float = 1.2
hole_radius: float = 1.5

# calculated
screw_radius = size / 2 - wall - clearance * sqrt(2)


def create_half() -> Shape:
    z = sphere(size / 2)
    z -= halfspace()
    z -= cylinder(hole_radius, size / 2)

    def create_screw():
        out_radius = screw_radius + clearance * sqrt(2)
        in_radius = screw_radius - wall - clearance
        height = screw_height / 2 + clearance
        z = screw(
            r=out_radius,
            h=height,
            depth=screw_depth,
        )
        z += cone(
            r1=out_radius,
            r2=out_radius - screw_depth,
            h=screw_depth,
        )

        z += cone(
            r1 = out_radius,
            r2 = 0,
            h = out_radius,
        ).up(height)

        z -= cylinder(
            r=in_radius,
            h=height+out_radius,
        )
        return z

    z -= create_screw()
    return z


def create_screw() -> Shape:
    out_radius = screw_radius
    in_radius = screw_radius - wall
    height = screw_height - screw_shrinkage
    z = screw(
        r=out_radius,
        h=height,
        depth=screw_depth,
    )
    z -= cylinder(
        r=in_radius,
        h=height,
    )
    z = z.down(height / 2)
    istn_radius = out_radius + height / 2 - screw_depth
    istn = cone(istn_radius, 0, istn_radius)
    istn += istn.mirrorXY()
    z ^= istn
    return z


half = create_half()
screw = create_screw()

disp_sbtn = halfspace().rotateX(pi / 2).rotateZ(pi / 8)

disp(half - disp_sbtn)
#disp(half.rotateX(pi) - disp_sbtn)
#disp(screw - disp_sbtn)

export_to_stl(
    shapes={
        "half": half,
        "screw": screw,
    }
)

show()
