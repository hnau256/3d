#!/usr/bin/env python3
# coding: utf-8

from utils import *
from zencad import *

friction = 0.2
tech = 0.01
wall = 1.6
strong_wall = 2.0

star_radius = 80
star_convexity = star_radius / 4
slot_radius = 5
center_hole_radius = slot_radius * 2

cavity_length_factor = 0.55
segment_rays_length_factors = [1, 0.8]
segment_rays_count = len(segment_rays_length_factors)
segments_count = 4
rays_count = segments_count * segment_rays_count

segment_angle = pi * 2 / segments_count
ray_angle = segment_angle / segment_rays_count

inserts_handle_length = 4.8

handle_length = star_radius * 0.25


def slot(offset: float = 0):
    width = (slot_radius + offset) * 2
    length = (star_radius + tech) * 2
    line = rectangle(width, length, center=True)
    result = union([line.rotateZ(i * ray_angle) for i in range(rays_count)])
    result = unify(result)
    result = extrude(result, star_convexity + tech)
    return result


def star(offset: float = 0):
    thickness = star_radius * star_convexity / (sqrt(star_radius ** 2 + star_convexity ** 2))
    scale_factor = (thickness + offset) / thickness
    radius = star_radius * scale_factor
    height = star_convexity * scale_factor

    base_points = []
    cavity_point = point3(radius * cavity_length_factor, 0, 0)
    for segment in range(segments_count):
        angle_offset = segment_angle * segment
        for ray in range(segment_rays_count):
            angle = ray_angle * ray
            segment_rays_length_factor = segment_rays_length_factors[ray]
            point_radius = radius * segment_rays_length_factor
            x = cos(angle) * point_radius
            y = sin(angle) * point_radius
            ray_point = point3(x, y, 0)
            base_points.append(ray_point.rotateZ(angle_offset))
            base_points.append(cavity_point.rotateZ(angle + angle_offset + ray_angle / 2))

    base_points_count = segment_rays_count * segments_count * 2
    base_points.append(point3(0, 0, 0))
    center_base_point_index = base_points_count
    base_points.append(point3(0, 0, height))
    center_convexity_point_index = base_points_count + 1

    faces = []
    for i in range(base_points_count):
        next_i = (i + 1) % base_points_count
        faces.append((center_base_point_index, i, next_i))
        faces.append((center_convexity_point_index, i, next_i))

    result = polyhedron(base_points, faces)

    result = result.rotateZ(-pi / 2)
    return result


center_radius = slot_radius / sin(ray_angle / 2)


def center_colon(offset: float = 0):
    radius = center_radius / cos(ray_angle / 2)
    result = ngon(radius + offset, rays_count)
    result = result.extrude(star_convexity + tech)
    return result


def lamp():
    result = star()

    full_handle_length = segment_rays_length_factors[0] * star_radius + handle_length
    handle = cylinder(slot_radius - tech, full_handle_length)
    handle = handle.rotateX(pi / 2)
    handle -= halfspace()
    handle_sbtn_radius = slot_radius - wall - tech
    handle_sbtn = box(handle_sbtn_radius, full_handle_length, handle_sbtn_radius)
    handle_sbtn = handle_sbtn.translate(-handle_sbtn_radius / 2, -full_handle_length, -handle_sbtn_radius / 2)
    handle_sbtn = handle_sbtn.rotateY(pi / 4)

    result ^= slot()
    
    result += handle
    sbtn = star(-wall)
    sbtn ^= slot(-wall)
    result -= sbtn

    result -= handle_sbtn

    inserts_handle_offset = friction + inserts_handle_length + friction + strong_wall
    inserts_handle = center_colon(inserts_handle_offset)
    inserts_handle ^= star(-strong_wall - friction)
    inserts_handle -= slot(-tech)
    inserts_handle -= center_colon(inserts_handle_offset - strong_wall)

    inserts_handle_center_sbtn_height = star_convexity - wall + tech
    inserts_handle_center_sbtn = cone(inserts_handle_center_sbtn_height, 0, inserts_handle_center_sbtn_height)
    inserts_handle_center_sbtn = inserts_handle_center_sbtn.down(tech)
    inserts_handle -= inserts_handle_center_sbtn

    result += inserts_handle

    def lines():
        line = box(center_radius - tech + strong_wall, strong_wall, star_convexity - wall + tech * 2)
        line = line.forw(-strong_wall / 2)
        line = line.rotateZ(ray_angle / 2)
        line ^= slot()
        result = union([line.rotateZ(ray_angle * i) for i in range(rays_count)])
        result ^= star(-wall + tech)
        return result

    result += lines()

    center_sbtn_height = star_convexity - wall - strong_wall
    center_sbtn = cone(center_sbtn_height, 0, center_sbtn_height)
    center_sbtn = center_sbtn.up(strong_wall)
    result -= center_sbtn

    result += cylinder(center_hole_radius + strong_wall, strong_wall)
    result -= cylinder(center_hole_radius, strong_wall)

    result = unify(result)
    return result




shapes = {}
def fill_inserts():
    all_inserts = star()
    all_inserts -= star(-wall)
    all_inserts -= slot(friction)

    handle_offset = friction + inserts_handle_length
    handle = center_colon(handle_offset)
    handle ^= star(-tech)
    handle -= star(-wall - friction - strong_wall)
    handle -= slot(friction)
    all_inserts += handle
    all_inserts += all_inserts.mirrorXY()

    insert_istn = polygon([
        point3(0, 0, 0),
        point3(0, -star_radius, 0),
        point3(0, -star_radius, 0).rotateZ(ray_angle)
    ])
    insert_istn = insert_istn.extrude((star_convexity + tech) * 2)
    insert_istn = insert_istn.down(star_convexity + tech)

    for i in range(segment_rays_count):
        insert = all_inserts ^ insert_istn.rotateZ(i * ray_angle)
        shapes[f'insert_{i + 1}_x{segments_count}'] = insert
        for segment in range(segments_count):
            display(insert.rotateZ(segment * segment_angle))

fill_inserts()


lamp = lamp()
shapes["side_x2"] = lamp
display(lamp)
display(lamp.mirrorXY())

export_to_stl(
    shapes=shapes,
)

show()
