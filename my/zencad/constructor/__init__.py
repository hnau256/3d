from zencad import *

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from utils import *

def build_box(
        min_x: float,
        max_x: float,
        min_y: float,
        max_y: float,
        min_z: float,
        max_z: float,
) -> Shape:
    z = box(
        max_x-min_x,
        max_y-min_y,
        max_z-min_z,
    )
    z = z.translate(
        min_x,
        min_y,
        min_z,
    )
    return z


def build_cylinder(
        min_z: float,
        max_z: float,
        radius: float,
) -> Shape:
    z = cylinder(
        r=radius,
        h=max_z - min_z,
    )
    z = z.up(min_z)
    return z


def build_screw(
        r: float,
        min_z: float,
        max_z: float,
        depth: float = 1.2,
        angle: float = pi / 2,
        entries: int = 1,
        top_side: ScrewSide = ScrewSide.DEFAULT,
        bottom_side: ScrewSide = ScrewSide.DEFAULT,
) -> Shape:
    return screw(
        r=r,
        h=max_z - min_z,
        depth=depth,
        angle=angle,
        entries=entries,
        top_side=top_side,
        bottom_side=bottom_side,
    ).up(min_z)
