#!/usr/bin/env python3
#coding: utf-8

from math import *
from zencad import *

tech = 0.1
friction = 0.25
nutDepth = 0.8
nutHeight = 5
arrayColors = [
    Color(1, 0, 0),
    Color(0, 1, 0),
    Color(0, 0, 1),
    Color(0, 1, 1),
    Color(1, 0, 1),
    Color(1, 1, 0),
    Color(1, 1, 1)
]

trunkWall = 0.8
levelBranchesHeight = 1.6
levelLineWidth = 3.2
levelSegmentsSeparation = 3.2
levelBranchesSeparation = 7.6
levelBranchAngle = deg(60)
levelTrunkRadiusByRadius = 0.16
nextLevelRotationAngle = deg(73)

levelsSeparation = 12
minLevelRadius = 15
maxLevelRadius = 35
levelsSegments = [7, 7, 6, 6, 5, 5]
baseRadiusByMaxRadius = 0.75
baseHeight = 2.4
nextLevelRadiusSubstraction = (maxLevelRadius - minLevelRadius) / (len(levelsSegments) - 1)

starWall = 1.2
starRaysCount = 5
starRayAngle = deg(45)
starRayLength = 12

maskSize = 1000
mask = box(maskSize, maskSize, maskSize)
mask = mask.translate(-maskSize/2, 0, -maskSize/2)
useMask = True

exportToStlDelta = 0.01
useExportToStl = True


def createNut(radius, height):
    pattern = polysegment(
        points([
            (0, 0, nutDepth),
            (nutDepth, 0, 0),
            (0, 0, -nutDepth)
        ]),
        closed=True
    )
    pattern = pattern.right(radius - tech)
    zOffset = nutDepth * 2
    extendedHeight = height + zOffset * 2
    path = helix(radius, extendedHeight, nutDepth * 2 + tech)
    nut = pipe_shell(spine=path, arr=[pattern], frenet=True)
    nut += cylinder(radius, extendedHeight)
    nut = nut.down(zOffset)
    nut ^= cylinder(radius + nutDepth, height)
    cap = cone(radius, 0, radius)
    cap = cap.up(height)
    nut += cap
    return nut


def calcTrunkRadius(levelRadius):
    radius = levelRadius * levelTrunkRadiusByRadius
    levelTrunkMinRadius = trunkWall + nutDepth + friction + trunkWall + nextLevelRadiusSubstraction * levelTrunkRadiusByRadius
    if radius < levelTrunkMinRadius:
        return levelTrunkMinRadius
    return radius

def calInnerNutRadius(trunkRadius):
    return trunkRadius - nutDepth - trunkWall


def createStar():

    angleInterRays = 2 * pi / starRaysCount
    connectorRadius = calcTrunkRadius(minLevelRadius)
    starDepth = (connectorRadius + starWall) * 2


    def createRays():

        def createRay():
            offsetX = tan(starRayAngle / 2) * starRayLength
            ray = polysegment(
                [
                    (-offsetX, 0),
                    (0, starRayLength),
                    (offsetX, 0)
                ],
                closed=True
            )
            ray = ray.fill()
            ray = ray.extrude(starDepth)
            return ray

        ray = createRay()
        rays = union([ray.rotateZ(angleInterRays * i) for i in range(0, starRaysCount)])
        rays = rays.down(starDepth / 2)
        rays = rays.rotateX(deg(90))

        return unify(rays)

    star = createRays()


    def calcBaseHeight():
        a = angleInterRays / 2
        b = starRayAngle / 2
        l = starRayLength
        c = pi - a - b
        adapterHeight = l * sin(b) / sin(c)
        return adapterHeight

    baseHeight = calcBaseHeight()

    def createAdapterNut():
        nutRadius = calInnerNutRadius(connectorRadius)
        nut = createNut(nutRadius, nutHeight)
        nut = nut.down(baseHeight)
        return nut

    star -= createAdapterNut()

    def createConnectorSbtn():
        radius = connectorRadius + friction
        height = 100
        sbtn = cylinder(radius, height)
        sbtn = sbtn.down(height + baseHeight)
        return sbtn

    star -= createConnectorSbtn()


    star = star.up(baseHeight)
    star = star.up(levelsSeparation * len(levelsSegments))

    return star



def createNextLevelConnector(
        thisConnectorRadius,
        nextConnectorRadius
):
    connector = None

    if thisConnectorRadius > nextConnectorRadius:
        connector = cone(thisConnectorRadius, nextConnectorRadius, levelsSeparation)
    else:
        connector = cylinder(thisConnectorRadius, levelsSeparation)

    nutRadius = nextConnectorRadius - trunkWall - nutDepth - friction
    nut = createNut(nutRadius, nutHeight)
    nut = nut.up(levelsSeparation)
    connector += nut
    return connector


def createLevel(radius, segmentsCount, zAngle):

    def createBase():

        def createBaseShape():

            segmentAngle = 2 * pi / segmentsCount

            def createSegment():

                def calcMaxBranchMountRadius(levelRadius):
                    lineRadius = levelLineWidth / 2
                    separationHalfRadius = levelSegmentsSeparation / 2
                    segmentHalfAngle = segmentAngle / 2
                    distanceToSeparation = lineRadius + separationHalfRadius
                    distanceToLevelRadius = levelRadius - lineRadius
                    angleFromSeparation = asin(distanceToSeparation / distanceToLevelRadius)
                    centerAngle = segmentHalfAngle - angleFromSeparation
                    branchAngle = pi - levelBranchAngle
                    omega = levelBranchAngle - centerAngle
                    mountRadius = (levelRadius - lineRadius)*sin(omega)/sin(branchAngle)
                    return mountRadius

                def calcBranchLength(branchMountRadius):
                    lineRadius = levelLineWidth / 2
                    separationHalfRadius = levelSegmentsSeparation / 2 + lineRadius
                    segmentHalfAngle = segmentAngle / 2
                    separationOffset = separationHalfRadius / sin(segmentHalfAngle)
                    offset = branchMountRadius - separationOffset
                    if offset <= 0:
                        return None
                    omega = levelBranchAngle - segmentHalfAngle
                    branchLength = offset * sin(segmentHalfAngle) / sin(omega)
                    return branchLength

                maxBranchMountRadius = calcMaxBranchMountRadius(radius)

                def createLine(start, end):

                    def createStroke(start, end, width):
                        radius = width / 2
                        x1, y1 = start
                        x2, y2 = end
                        dx = x2 - x1
                        dy = y2 - y1
                        if dx == 0 and dy == 0:
                            return nullshape()
                        nx, ny = (-dy / dx, 1) if dx != 0 else (1, -dx / dy)
                        nLength = sqrt(nx*nx + ny * ny)
                        nx = nx * radius / nLength
                        ny = ny * radius / nLength
                        stroke = polysegment(
                            [
                                (x1 + nx, y1 + ny),
                                (x2 + nx, y2 + ny),
                                (x2 - nx, y2 - ny),
                                (x1 - nx, y1 - ny)
                            ],
                            closed=True
                        )
                        stroke = stroke.fill()
                        return stroke

                    result = createStroke(start, end, levelLineWidth)
                    endX, endY = end
                    result += circle(levelLineWidth/2).translate(endX, endY)
                    return result

                result = createLine((0, 0), (0, radius - levelLineWidth / 2))

                branchesCount = int(maxBranchMountRadius / levelBranchesSeparation)
                for i in range(0, branchesCount):
                    mountRadius = maxBranchMountRadius - i * levelBranchesSeparation
                    length = calcBranchLength(mountRadius)
                    if length:
                        angle = pi / 2 - levelBranchAngle
                        offsetX = cos(angle) * length
                        offsetY = sin(angle) * length
                        start = (0, mountRadius)
                        result += createLine(start, (offsetX, mountRadius + offsetY))
                        result += createLine(start, (-offsetX, mountRadius + offsetY))

                return result

            levelSegment = createSegment()

            result = union([levelSegment.rotateZ(i * segmentAngle) for i in range(0, segmentsCount)])
            result = result

            return result

        base = createBaseShape()
        base = base.extrude(levelBranchesHeight)
        return base

    level = createBase()
    level = level.rotateZ(zAngle)

    trunkRadius = calcTrunkRadius(radius)
    nextLevelTrunkRadius = calcTrunkRadius(radius - nextLevelRadiusSubstraction)
    level += createNextLevelConnector(trunkRadius, nextLevelTrunkRadius)

    nutRadius = calInnerNutRadius(trunkRadius)
    nut = createNut(nutRadius, nutHeight + friction)
    level -= nut

    return level


def createBase():
    radius = maxLevelRadius * baseRadiusByMaxRadius
    height = baseHeight
    base = cylinder(radius, height)
    base = base.fillet(height - tech, [(0, radius, height)])
    fakeLevelRadius = maxLevelRadius + nextLevelRadiusSubstraction
    fakeLevelTrunkRadius = calcTrunkRadius(fakeLevelRadius)
    maxLevelTrunkRadius = calcTrunkRadius(maxLevelRadius)
    base += createNextLevelConnector(fakeLevelTrunkRadius, maxLevelTrunkRadius)
    base = base.fillet(levelsSeparation - height - tech, [(fakeLevelTrunkRadius, 0, height)])
    base = base.down(levelsSeparation)
    return base


def use(
        model,
        exportToStlName,
        color=Color(1, 1, 1)
):

    if useExportToStl:
        name = exportToStlName + ".stl"
        to_stl(model, name, exportToStlDelta)

    if useMask:
        model ^= mask

    disp(model, color)

def useLevels():
    for i in range(0, len(levelsSegments)):
        segmentsCount = levelsSegments[i]
        radius =  maxLevelRadius - nextLevelRadiusSubstraction * i
        level = createLevel(radius, segmentsCount, nextLevelRotationAngle * i)
        level = level.up(levelsSeparation * i)
        color = arrayColors[i % len(arrayColors)]
        use(level, "level_" + str(i+1), color)


useLevels()
use(createBase(), "base", Color(1, 1, 1))
use(createStar(), "star", Color(1, 1, 1))



show()
