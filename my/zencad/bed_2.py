from zencad import *
from utils import *
from dataclasses import *
from enum import *
from typing import *
from collections import *

mattress_width = 1700
mattress_length = 800
mattress_height = 120
mattress_compressed_height = 80
mattress_corner_fillet = 100
mattress_fillet = 10

material_thickness = 16
furnishing_material_thickness = 0.5
clearance = 4

mattress_top = 500
boardside_height = 250
boardside_opening_width = 500
boardside_fillet = 200

wheel_height = 36

# Calculated
mattress_bottom = mattress_top - mattress_height
mattress_base_bottom = mattress_bottom - material_thickness
total_height = mattress_top + boardside_height
total_length = material_thickness + mattress_length + material_thickness
total_width = material_thickness + mattress_width + material_thickness


class Orientation(Enum):
    X = 0
    Y = 1
    Z = 2


class Side(Enum):
    LEFT = 0
    FAR = 1
    RIGHT = 2
    NEAR = 3


class Corner(Enum):
    LEFT_FAR = 0
    FAR_RIGHT = 1
    RIGHT_NEAR = 2
    NEAR_LEFT = 3


@dataclass
class Position:
    orientation: Orientation
    x: float
    y: float
    z: float


@dataclass
class Part:
    name: str
    width: float
    length: float
    positions: List[Position]
    furnished_sides: List[Side] = None
    round_corners: Dict[Corner, float] = None

    def print_specification(self):

        print(self.name + ":")

        x_furnishings = 0
        y_furnishings = 0
        if self.furnished_sides:
            for side in self.furnished_sides:
                match side:
                    case Side.LEFT:
                        y_furnishings += 1
                    case Side.RIGHT:
                        y_furnishings += 1
                    case Side.NEAR:
                        x_furnishings += 1
                    case Side.FAR:
                        x_furnishings += 1
        width = int(self.width - y_furnishings * furnishing_material_thickness)
        length = int(self.length - x_furnishings * furnishing_material_thickness)
        print("\tSize:" + str(width) + "x" + str(length))

        if self.furnished_sides:
            sides = ""
            sides_count = len(self.furnished_sides)
            for i in range(sides_count):
                side = self.furnished_sides[i]
                match side:
                    case Side.LEFT:
                        sides += "left"
                    case Side.RIGHT:
                        sides += "right"
                    case Side.NEAR:
                        sides += "bottom"
                    case Side.FAR:
                        sides += "top"
                if i < sides_count - 1:
                    sides += ", "
            print("\tFurnishing sides: " + sides)

        if self.round_corners:
            corners = ""
            corners_count = len(self.round_corners)
            for i, corner in enumerate(self.round_corners):
                radius = self.round_corners[corner]
                match corner:
                    case Corner.LEFT_FAR:
                        corners += "left_top"
                    case Corner.NEAR_LEFT:
                        corners += "left_bottom"
                    case Corner.RIGHT_NEAR:
                        corners += "right_bottom"
                    case Corner.FAR_RIGHT:
                        corners += "right_top"
                corners += "(" + str(radius) + ")"
                if i < corners_count - 1:
                    corners += ", "
            print("\tRound corners: " + corners)

        print(" ")

    def display(self):
        z = box(
            self.width,
            self.length,
            material_thickness,
        )

        if self.round_corners:
            for corner in self.round_corners:
                is_left = corner == Corner.LEFT_FAR or corner == Corner.NEAR_LEFT
                is_near = corner == Corner.NEAR_LEFT or corner == Corner.RIGHT_NEAR
                z = z.fillet(
                    self.round_corners[corner],
                    [
                        point3(
                            0 if is_left else self.width,
                            0 if is_near else self.length,
                            material_thickness / 2,
                        )
                    ],
                )

        furnishing = None
        if self.furnished_sides:
            for side in self.furnished_sides:
                is_horizontal = side == Side.NEAR or side == Side.FAR
                line = box(
                    self.width if is_horizontal else furnishing_material_thickness,
                    furnishing_material_thickness if is_horizontal else self.length,
                    material_thickness,
                ).translate(
                    (self.width - furnishing_material_thickness) if side == Side.RIGHT else 0,
                    (self.length - furnishing_material_thickness) if side == Side.FAR else 0
                )
                furnishing = (furnishing + line) if furnishing else line

        for position in self.positions:
            positioned_z = z

            def transform_shape(shape):
                match position.orientation:
                    case Orientation.X:
                        shape = shape.rotateY(-pi / 2).right(material_thickness)
                    case Orientation.Y:
                        shape = shape.rotateX(pi / 2).forw(material_thickness)

                shape = shape.translate(
                    position.x,
                    position.y,
                    position.z,
                )

                return shape

            positioned_z = transform_shape(positioned_z)

            if furnishing:
                positioned_furnishing = unify(furnishing)
                positioned_furnishing = transform_shape(positioned_furnishing)
                positioned_z -= positioned_furnishing
                display(
                    positioned_furnishing,
                    Color(1, 0.1, 0),
                )

            display(positioned_z, Color(1, 1, 1, 0.75))


mattress_base = Part(
    name="Mattress base",
    width=mattress_width,
    length=mattress_length,
    positions=[
        Position(
            orientation=Orientation.Z,
            x=material_thickness,
            y=material_thickness,
            z=mattress_base_bottom,
        )
    ]
)

back_side = Part(
    name="Back side",
    width=mattress_width,
    length=total_height,
    furnished_sides=[Side.FAR, Side.NEAR],
    positions=[
        Position(
            orientation=Orientation.Y,
            x=material_thickness,
            y=material_thickness + mattress_length,
            z=0,
        )
    ]
)

front_side = Part(
    name="Front side",
    width=mattress_width,
    length=material_thickness + mattress_compressed_height,
    furnished_sides=[Side.FAR, Side.NEAR],
    positions=[
        Position(
            orientation=Orientation.Y,
            x=material_thickness,
            y=0,
            z=mattress_base_bottom,
        )
    ]
)

folding_boardside = Part(
    name="Folding boardside",
    width=mattress_width - clearance - boardside_opening_width,
    length=total_height - (mattress_bottom + mattress_compressed_height + clearance),
    furnished_sides=[Side.FAR, Side.NEAR, Side.LEFT, Side.RIGHT],
    round_corners={Corner.LEFT_FAR: boardside_fillet},
    positions=[
        Position(
            orientation=Orientation.Y,
            x=material_thickness + boardside_opening_width,
            y=0,
            z=mattress_bottom + mattress_compressed_height + clearance,
        )
    ]
)

left_side = Part(
    name="Left side",
    width=total_height,
    length=total_length,
    furnished_sides=[Side.FAR, Side.NEAR, Side.LEFT, Side.RIGHT],
    round_corners={Corner.RIGHT_NEAR: boardside_fillet},
    positions=[
        Position(
            orientation=Orientation.X,
            x=0,
            y=0,
            z=0,
        )
    ]
)

right_side = Part(
    name="Right side",
    width=total_height,
    length=total_length,
    furnished_sides=[Side.FAR, Side.NEAR, Side.LEFT, Side.RIGHT],
    positions=[
        Position(
            orientation=Orientation.X,
            x=total_width - material_thickness,
            y=0,
            z=0,
        )
    ]
)

chest_hole_width = mattress_width / 2 - material_thickness / 2

mattress_base_support = Part(
    name="Mattress base support",
    width=mattress_base_bottom,
    length=mattress_length - clearance,
    furnished_sides=[Side.NEAR, Side.LEFT],
    positions=[
        Position(
            orientation=Orientation.X,
            x=material_thickness + chest_hole_width,
            y=material_thickness + clearance,
            z=0,
        )
    ]
)

chest_facade = Part(
    name="Chest facade",
    width=chest_hole_width - clearance + material_thickness / 2 - clearance / 2,
    length=mattress_base_bottom - clearance - clearance,
    furnished_sides=[Side.RIGHT, Side.NEAR, Side.LEFT, Side.FAR],
    positions=[
        Position(
            orientation=Orientation.Y,
            x=material_thickness + clearance,
            y=0,
            z=clearance,
        ),
        Position(
            orientation=Orientation.Y,
            x=material_thickness + chest_hole_width + material_thickness / 2 + clearance / 2,
            y=0,
            z=clearance,
        )
    ]
)

chest_base = Part(
    name="Chest base",
    width=chest_hole_width - clearance - clearance,
    length=mattress_length - clearance,
    furnished_sides=[Side.RIGHT, Side.LEFT, Side.FAR],
    positions=[
        Position(
            orientation=Orientation.Z,
            x=material_thickness + clearance,
            y=material_thickness,
            z=wheel_height,
        ),
        Position(
            orientation=Orientation.Z,
            x=material_thickness + chest_hole_width + material_thickness + clearance,
            y=material_thickness,
            z=wheel_height,
        )
    ]
)

chest_back_side = Part(
    name="Chest back side",
    width=chest_hole_width - clearance - clearance,
    length=mattress_base_bottom - wheel_height - material_thickness - clearance,
    furnished_sides=[Side.RIGHT, Side.LEFT, Side.FAR],
    positions=[
        Position(
            orientation=Orientation.Y,
            x=material_thickness + clearance,
            y=material_thickness + mattress_length - material_thickness - clearance,
            z=wheel_height + material_thickness,
        ),
        Position(
            orientation=Orientation.Y,
            x=material_thickness + chest_hole_width + material_thickness + clearance,
            y=material_thickness + mattress_length - material_thickness - clearance,
            z=wheel_height + material_thickness,
        ),
    ]
)

chest_side = Part(
    name="Chest side",
    width=mattress_base_bottom - wheel_height - material_thickness - clearance,
    length=mattress_length - clearance - material_thickness,
    furnished_sides=[Side.RIGHT],
    positions=[
        Position(
            orientation=Orientation.X,
            x=material_thickness + clearance,
            y=material_thickness,
            z=wheel_height + material_thickness,
        ),
        Position(
            orientation=Orientation.X,
            x=material_thickness + chest_hole_width - clearance - material_thickness,
            y=material_thickness,
            z=wheel_height + material_thickness,
        ),
        Position(
            orientation=Orientation.X,
            x=material_thickness + chest_hole_width + material_thickness + clearance,
            y=material_thickness,
            z=wheel_height + material_thickness,
        ),
        Position(
            orientation=Orientation.X,
            x=material_thickness + mattress_width - clearance - material_thickness,
            y=material_thickness,
            z=wheel_height + material_thickness,
        )
    ]
)

parts = [
    mattress_base,
    back_side,
    front_side,
    right_side,
    mattress_base_support,
    chest_facade,
    chest_base,
    chest_back_side,
    chest_side,
    left_side,
    folding_boardside,
]


def create_mattress():
    z = rectangle(mattress_width, mattress_length)
    z = z.fillet2d(mattress_corner_fillet)
    z = z.extrude(mattress_height)
    z = z.fillet(mattress_fillet)
    z = z.translate(
        material_thickness,
        material_thickness,
        mattress_bottom,
    )
    return z


display(create_mattress(), Color(0.5, 1, 0, 0.5))

for part in parts:
    part.print_specification()
    part.display()

show()
