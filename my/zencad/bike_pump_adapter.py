#!/usr/bin/env python3
# coding: utf-8

from utils import *

# defaults
clearance: float = 0.2
pump_radius_shrinkage: float = 0.4
pump_holder_wall: float = 3.2
length: float = 24.0
width: float = 24.0
bolt_pump_distance: float = 0.4
reference_length: float = 64.0
base_to_pump_holder_fillet_radius: float = 16.0
pump_holder_window_angle: float = pi / 2

# measurements
bike_tube_radius: float = 34.4 / 2
bike_tube_hill_radius: float = 10.3 / 2
bike_tube_hill_height: float = 2.3
bolt_screw_radius: float = 4.8 / 2
bolt_cap_radius: float = 8.4 / 2
bolt_cap_height: float = 5.0
real_pump_radius: float = 32.5 / 2

# calculated
pump_radius = real_pump_radius - pump_radius_shrinkage

bike_tube_center_z = -(bike_tube_radius + clearance)
bike_tube_hill_max_z = bike_tube_hill_height - clearance
bike_tube_hill_sbtn_max_z = bike_tube_hill_max_z + clearance
bolt_cap_sbtn_min_z = bike_tube_hill_sbtn_max_z + pump_holder_wall
bolt_cap_min_z = bolt_cap_sbtn_min_z + clearance
bolt_cap_max_z = bolt_cap_min_z + bolt_cap_height
pump_sbtn_min_z = bolt_cap_max_z + bolt_pump_distance
pump_min_z = pump_sbtn_min_z + clearance
pump_center_z = pump_min_z + pump_radius


def create_bike_tube(
        expansion: float = 0,
) -> Shape:
    def tube() -> Shape:
        z = cylinder(
            r=bike_tube_radius + expansion,
            h=reference_length,
        )
        z = z.rotateX(pi / 2)
        z = z.forw(reference_length / 2)
        z = z.up(bike_tube_center_z)
        return z

    z = tube()

    def hill() -> Shape:
        height = bike_tube_hill_max_z - bike_tube_center_z + expansion
        z = cylinder(
            r=bike_tube_hill_radius + expansion,
            h=height,
        )
        z = z.up(bike_tube_center_z)
        return z

    z += hill()

    return z


def create_bike_tube_and_pump() -> Shape:
    z = create_bike_tube()

    def bolt_screw() -> Shape:
        height = bolt_cap_min_z - bike_tube_center_z
        z = cylinder(
            r=bolt_screw_radius,
            h=height,
        )
        z = z.up(bike_tube_center_z)
        return z

    z += bolt_screw()

    def bolt_cap() -> Shape:
        height = bolt_cap_max_z - bolt_cap_min_z
        z = cylinder(
            r=bolt_cap_radius,
            h=height,
        )
        z = z.up(bolt_cap_min_z)
        return z

    z += bolt_cap()

    def pump() -> Shape:
        z = cylinder(
            r=pump_radius,
            h=reference_length,
        )
        z = z.rotateX(pi / 2)
        z = z.forw(reference_length / 2)
        z = z.up(pump_center_z)
        return z

    z += pump()

    return z


def create_adapter() -> Shape:
    def base() -> Shape:
        height = pump_center_z - bike_tube_center_z
        z = box(width, length, height)
        z = z.translate(
            -width / 2,
            -length / 2,
            bike_tube_center_z,
        )
        z -= create_bike_tube(
            expansion=clearance,
        )
        return z

    z = base()

    def pump_holder() -> Shape:
        z = cylinder(
            r=pump_radius + clearance + pump_holder_wall,
            h=length,
        )
        z = z.rotateX(pi / 2)
        z = z.forw(length / 2)
        z = z.up(pump_center_z)
        return z

    z += pump_holder()

    z = fillet(
        shp=z,
        r=base_to_pump_holder_fillet_radius,
        refs=[
            (-width / 2, 0, pump_sbtn_min_z),
            (width / 2, 0, pump_sbtn_min_z),
        ]
    )

    def pump_sbtn() -> Shape:
        z = cylinder(
            r=pump_radius + clearance,
            h=length + tech * 2,
        )
        z = z.rotateX(pi / 2)
        z = z.forw(length / 2 + tech)
        z = z.up(pump_center_z)
        return z

    z -= pump_sbtn()

    def bolt_sbtn() -> Shape:
        def part_sbtn(
                min_z: float,
                max_z: float,
                radius: float,
        ) -> Shape:
            z = cylinder(
                r=radius,
                h=max_z - min_z,
            )
            z = z.up(min_z)
            return z

        z = part_sbtn(
            min_z=bike_tube_center_z,
            max_z=bolt_cap_min_z,
            radius=bolt_screw_radius + clearance,
        )
        z += part_sbtn(
            min_z=bolt_cap_sbtn_min_z,
            max_z=pump_center_z,
            radius=bolt_cap_radius + clearance,
        )
        return z

    z -= bolt_sbtn()

    def pump_holder_window_sbtn() -> Shape:
        z = cylinder(
            r=pump_radius + clearance + pump_holder_wall + tech,
            h=length + tech * 2,
            yaw=pump_holder_window_angle,
        )
        z = z.rotateZ(pi / 2 - pump_holder_window_angle / 2)
        z = z.rotateX(pi / 2)
        z = z.forw(length / 2)
        z = z.up(pump_center_z)
        return z

    z -= pump_holder_window_sbtn()

    def pump_holder_caps() -> Shape:
        z = cylinder(
            r=pump_holder_wall / 2,
            h=length,
        )
        z = z.rotateX(pi / 2)
        z = z.forw(length / 2)
        z = z.up(pump_radius + clearance + pump_holder_wall / 2)
        z = z.rotateY(pump_holder_window_angle/2)
        z = z.up(pump_center_z)
        z += z.mirrorYZ()
        return z

    z += pump_holder_caps()

    return z


adapter: Shape = create_adapter()

sbtn: Shape = nullshape()#halfspace().rotateX(pi / 2)

disp(
    create_bike_tube_and_pump() - sbtn,
    Color(1, 1, 1, 0.25)
)
disp(
    adapter - sbtn,
    Color(1, 0, 1)
)

export_to_stl(
    shapes={
        "bike_pump_adapter": adapter.rotateX(pi / 2),
    }
)

show()
