from utils import *

wall = 6.4
content_width = 55
content_length = 40
base_chamfer = 4.8
content_chamfer = 7.2
clearance = 0.4
tech = 0.001
screw_radius = wall
screw_depth = 1.2
nut_length = wall * 2
nut_notch_radius = 2
nut_notches_separation = nut_notch_radius * 3
base_height = 22
hanging_angle = pi / 5
handle_hole_radius = 4.8
handle_x_offset = -6.4
handle_wall = 3.2

# Calculated
screw_hole_radius = screw_radius + clearance
base_length = content_length + (wall + clearance) * 2
piston_width = wall + wall
base_width = wall + content_width + piston_width + clearance + nut_length + clearance + wall


def create_base():
    def chamfer_inner(shape, x, y):
        shape = shape.chamfer(
                base_chamfer - tech,
                [
                    (x, y, base_height / 2),
                    (x, y, -base_height / 2)
                ]
        )
        shape = shape.fillet(
                base_chamfer * sqrt(2) * 2 - tech,
                [
                    (x, y, base_height / 2 - base_chamfer * sqrt(2)),
                    (x, y, -base_height / 2 + base_chamfer * sqrt(2))
                ]
        )
        return shape

    z = box(base_width, base_length, base_height)
    z = z.fillet(
            base_chamfer,
            [
                (base_width, 0, base_height / 2),
                (base_width, base_length, base_height / 2)
            ]
    )
    z = z.fillet(
            content_chamfer + wall,
            [
                (0, 0, base_height / 2),
                (0, base_length, base_height / 2)
            ]
    )
    z = z.translate(-base_width, -base_length / 2, -base_height / 2)
    handle_radius = handle_hole_radius + handle_wall
    handle = cylinder(handle_radius, base_height)
    handle += box(handle_radius * 2, handle_hole_radius, base_height).left(handle_radius)
    handle -= cylinder(handle_hole_radius, base_height)
    handle -= halfspace().rotateX(pi/2).forw(handle_hole_radius+tech)
    handle_x = -base_width + wall + content_width / 2 + handle_x_offset
    handle = handle.translate(handle_x, -base_length / 2-handle_hole_radius, -base_height / 2)
    z += handle
    z += handle.rotateX(pi)
    z = z.fillet(
            handle_hole_radius-tech,
            [
                (handle_x-handle_radius, -base_length / 2, 0),
                (handle_x+handle_radius, -base_length / 2, 0),
                (handle_x-handle_radius, base_length / 2, 0),
                (handle_x+handle_radius, base_length / 2, 0)
            ]
    )
    sbtn = box(base_width - wall * 2, base_length - wall * 2, base_height)
    sbtn = sbtn.fillet(
            content_chamfer,
            [
                (0, 0, base_height / 2),
                (0, base_length - wall * 2, base_height / 2)
            ]
    )
    sbtn = sbtn.translate(-base_width + wall, -base_length / 2 + wall, -base_height / 2)
    z -= sbtn
    z = chamfer_inner(z, -base_width / 2, base_length / 2 - wall)
    screw_hole_size = (screw_hole_radius + clearance) * 2
    screw_hole = box(wall, screw_hole_size, screw_hole_size)
    screw_hole = screw_hole.translate(-wall, -screw_hole_size / 2, -screw_hole_size / 2)
    z -= screw_hole
    piston_additional_length = wall + clearance - base_chamfer
    piston_length = base_length - piston_additional_length * 2
    piston_extended_width = piston_width
    piston = box(piston_extended_width, piston_length, base_height)
    piston = piston.translate(- piston_extended_width, -piston_length / 2, -base_height / 2)
    piston = chamfer_inner(piston, - piston_width - 1.5, 0)
    piston_side_sbtn_length = wall + clearance
    piston_side_sbtn_height = base_height + clearance * 2
    piston_side_sbtn = box(piston_extended_width, piston_side_sbtn_length, piston_side_sbtn_height)
    piston_side_sbtn = piston_side_sbtn.translate(0, 0, -piston_side_sbtn_height / 2)
    piston_side_sbtn = piston_side_sbtn.chamfer(
            base_chamfer + clearance - tech,
            [
                (piston_extended_width / 2, piston_side_sbtn_length, piston_side_sbtn_height / 2),
                (piston_extended_width / 2, piston_side_sbtn_length, -piston_side_sbtn_height / 2)
            ]
    )
    piston_side_sbtn = piston_side_sbtn.fillet(
            (base_chamfer + clearance) * sqrt(2) * 2 - tech,
            [
                (piston_extended_width / 2, piston_side_sbtn_length,
                 piston_side_sbtn_height / 2 - (base_chamfer + clearance) * sqrt(2)),
                (piston_extended_width / 2, piston_side_sbtn_length,
                 -piston_side_sbtn_height / 2 + (base_chamfer + clearance) * sqrt(2))
            ]
    )
    piston_side_sbtn = piston_side_sbtn.translate(-piston_extended_width, -base_length / 2, 0)
    piston -= piston_side_sbtn
    piston -= piston_side_sbtn.mirrorXZ()
    screw_handle_sbtn_size = wall + clearance * 2
    screw_handle_sbtn = box(wall + clearance, screw_handle_sbtn_size, screw_handle_sbtn_size)
    screw_handle_sbtn = screw_handle_sbtn.translate(-wall - clearance, -screw_handle_sbtn_size / 2,
                                                    -screw_handle_sbtn_size / 2)
    piston -= screw_handle_sbtn
    piston = piston.left(wall + clearance + clearance + nut_length)
    z += piston
    # z -= halfspace().rotateY(pi/2).left(30)
    return z


def create_screw():
    z = box(wall, wall, wall)
    z = z.translate(-wall / 2, -wall / 2)
    screw_height = content_width + nut_length + wall
    screw_shape = screw(screw_radius, screw_height, screw_depth)
    screw_shape_istn_radius = screw_hole_radius + screw_height - screw_depth
    screw_shape ^= cone(screw_shape_istn_radius, 0, screw_shape_istn_radius)
    z += screw_shape.up(wall)
    z = z.rotateY(pi / 2)
    z = z.left(wall + clearance + nut_length + clearance + wall)
    safe_handle_radius = (screw_radius - screw_depth) * sin(hanging_angle)
    z -= halfspace().down(safe_handle_radius)
    return z


def create_nut():
    radius = content_length / 2 - clearance
    z = cylinder(radius, nut_length)
    z = z.chamfer(nut_length / 8, [(0, radius, 0), (0, radius, nut_length)])
    screw_radius_local = screw_radius + clearance * sqrt(2)
    z -= screw(screw_radius_local, nut_length, screw_depth)
    chamfer_cone = cone(screw_radius_local, 0, screw_radius_local)
    z -= chamfer_cone
    z -= chamfer_cone.mirrorXY().up(nut_length)
    notch = cylinder(nut_notch_radius, nut_length)
    notch = notch.right(radius)
    notches_count = floor(radius * 2 * pi / nut_notches_separation)
    notch_step_angle = pi * 2 / notches_count
    notches = union([notch.rotateZ(i * notch_step_angle) for i in range(notches_count)])
    z -= notches
    z = z.rotateY(pi / 2)
    z = z.left(wall + clearance + nut_length)
    return z


base = create_base()
screw_shape = create_screw()
nut = create_nut()

export_to_stl(
        shapes={
            "base": base,
            "screw": screw_shape,
            "nut": nut.rotateY(pi/2)
        }
)

display(base, Color(1, 1, 0))
display(screw_shape, Color(1, 0, 1))
display(nut, Color(0, 1, 1))
show()
