#!/usr/bin/env python3
# coding: utf-8

from enum import Enum
from utils import *

# invented
in_size: float = 600.0
edge_wall: float = 12.0
edge_corner_radius: float = 8.0

# calculated
step: float = edge_wall / 2 + in_size + edge_wall / 2


class Axis(Enum):
    X = left(step / 2) * rotateY(-pi / 2)
    Y = back(step / 2) * rotateX(pi / 2)
    Z = down(step / 2)


def calc_coordinate(index: int) -> float:
    return (index + 0.5) * step


def create_edge() -> Shape:
    z = box(
        x=in_size,
        y=in_size,
        z=edge_wall,
        center=True,
    )
    r = in_size / 2
    z = z.fillet(
        r=edge_corner_radius,
        refs=[
            (r, r),
            (r, -r),
            (-r, r),
            (-r, -r),
        ]
    )
    z = z.fillet(
        r=edge_wall / 3,
        refs=[],
    )
    return z


edge = create_edge()


def place_edge(
        coordinates: (int, int, int),
        axis: Axis,
        sbtn: Shape = None,
):
    z = edge
    if sbtn:
        sbtn_height = tech + edge_wall + tech
        z -= sbtn.extrude(sbtn_height).down(sbtn_height / 2)
    z = z.transform(axis.value)
    z = z.translate(
        calc_coordinate(coordinates[0]),
        calc_coordinate(coordinates[1]),
        calc_coordinate(coordinates[2]),
    )
    disp(z)

place_edge(
    coordinates=(0, 0, 0),
    axis=Axis.X,
)
show()
