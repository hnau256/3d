from utils import *
import svgpathtools as svg

wall = 2.4
furrow = 0.8
voronoi_fillet = 2.0
radius = 50
height = 8
circle_segment_length = 2


def create_stand():
    voronoi_radius = radius - wall - furrow
    segments_count = floor((voronoi_radius * 2 * pi) / circle_segment_length)
    z = voronoi_sbtn(
            seed=1,
            polygon_points=[
                (
                    cos(2 * pi * i / segments_count) * voronoi_radius,
                    sin(2 * pi * i / segments_count) * voronoi_radius
                )
                for i in range(segments_count)
            ],
            height=tech * 2,
            separation=furrow,
            fillet_radius=voronoi_fillet
    )
    z -= halfspace().down(tech)
    z = z.up(height - voronoi_fillet)
    z += cylinder(radius, height - voronoi_fillet)
    border = cylinder(radius, height)
    border -= cylinder(radius - wall, height)
    z += border
    return z


stand = create_stand()
export_to_stl({"stand": stand})
display(create_stand())
show()
