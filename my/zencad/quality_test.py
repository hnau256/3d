#!/usr/bin/env python3
# coding: utf-8

from utils import *

font_name = "roboto"
register_font(f"utils/{font_name}.ttf")

# invented
size: float = 20.0
column: float = 1.6
wall: float = 0.8
overhang_angle: float = 50 / 180 * pi
overhang_trunk_radius: float = 4.8
overhang_top_separation: float = 0.4
z_depth: float = 0.8
z_size_by_size: float = 0.8
xy_depth: float = 0.8
xy_size_by_size: float = 0.5
resonance_gap_width: float = 0.2
resonance_gap_depth: float = 2.4
resonance_layer_height: float = 2.4
resonance_x_offset_by_layer_height: float = 1
clearance: float = 0.2
clearance_rays_count: int = 5
clearance_inner_radius_factor: float = 0.6
clearance_chamfer: float = 0.4
clearance_radius_by_size: float = 0.3
clearance_depth: float = 1.2
clearance_sbtn_additional_depth: float = 0.8
circle_radius_by_size: float = 0.25
circle_depth: float = 1.2
circle_back_gap: float = 0.4
circle_back_gap_margin: float = 0.4


def create_test():
    top_rect_min_z = size - wall
    overhang_max_z = top_rect_min_z - overhang_top_separation
    overhang_radius = size / 2
    overhang_radius_delta = overhang_radius - overhang_trunk_radius
    overhang_height = 1 / tan(overhang_angle) * overhang_radius_delta
    overhang_min_z = overhang_max_z - overhang_height
    bottom_content_min_z = wall
    bottom_content_max_z = overhang_min_z - wall
    bottom_content_height = bottom_content_max_z - bottom_content_min_z

    z = box(size, size, overhang_min_z)
    z = z.translate(
        -size / 2,
        -size / 2,
    )

    def create_sign(
            sign: str,
            size_by_size: float,
            thickness: float,
    ) -> Shape:
        z = textshape(
            text=sign,
            fontname=font_name,
            size=size * size_by_size
        )
        z = z.extrude(thickness)
        bbox = z.bbox()
        z = z.translate(
            -bbox.xmin - (bbox.xmax - bbox.xmin) / 2,
            -bbox.ymin - (bbox.ymax - bbox.ymin) / 2,
        )
        return z

    def create_top():
        z = box(size, size, z_depth + wall)
        z = z.translate(
            -size / 2,
            -size / 2,
        )
        z -= create_sign(
            sign="Z",
            size_by_size=z_size_by_size,
            thickness=z_depth,
        ).up(wall)
        z = z.up(wall)
        rect = box(size, size, wall)
        rect = rect.translate(
            -size / 2,
            -size / 2,
        )
        rect_sbtn = box(size - column * 2, size - column * 2, wall)
        rect_sbtn = rect_sbtn.translate(
            -size / 2 + column,
            -size / 2 + column,
        )
        rect -= rect_sbtn
        z += rect
        z = z.up(top_rect_min_z)
        return z

    z += create_top()

    def create_columns():
        z = box(
            column,
            column,
            size - overhang_min_z,
        )
        z = z.translate(
            size / 2 - column,
            size / 2 - column,
        )
        z += z.mirrorXZ()
        z += z.mirrorYZ()
        z = z.up(overhang_min_z)
        return z

    z += create_columns()

    def create_overhang():
        z = cone(
            overhang_trunk_radius,
            overhang_radius,
            overhang_height,
        )
        z = thicksolid(
            proto=z,
            t=-wall,
            refs=[
                (0, 0, overhang_height),
            ]
        )
        z = z.up(overhang_min_z)
        return z

    z += create_overhang()

    def create_resonance_sbtn():
        layers_count = int(bottom_content_height / resonance_layer_height)
        layer_height = bottom_content_height / layers_count
        x_offset = layer_height * resonance_x_offset_by_layer_height
        gap = resonance_gap_width
        z = polysegment(
            pnts=[
                (0, 0, 0),
                (gap, 0, 0),
                (gap + x_offset, layer_height, 0),
                (x_offset, layer_height, 0),
            ],
            closed=True,
        )
        z = z.fill()

        def create_layer(
                z: Shape,
                i: int,
        ) -> Shape:
            if i % 2 == 1:
                z = z.mirrorYZ()
                z = z.right(gap + x_offset)
            z = z.forw(i * layer_height)
            return z

        z = union([create_layer(z, i) for i in range(layers_count)])
        z = z.extrude(resonance_gap_depth)
        z = z.rotateX(pi / 2)
        z = z.translate(
            clearance_depth + clearance_sbtn_additional_depth + wall / 2 - size / 2,
            -size / 2 + resonance_gap_depth,
            wall,
        )
        z += z.rotateZ(-pi / 2)
        return z

    z -= create_resonance_sbtn()

    def create_xy_sbtn():
        center_z = (bottom_content_max_z + bottom_content_min_z) / 2

        def create_x_or_y(
                sign: str,
                z_angle: float,
        ) -> Shape:
            z = create_sign(
                sign=sign,
                size_by_size=xy_size_by_size,
                thickness=xy_depth,
            )
            z = z.rotateX(pi / 2)
            z = z.translate(
                0,
                -size / 2 + xy_depth + circle_back_gap + circle_depth,
                center_z,
            )
            z = z.rotateZ(z_angle)
            return z

        z = create_x_or_y("X", pi / 2)
        z += create_x_or_y("Y", pi)
        return z

    z -= create_xy_sbtn()

    def create_circle_sbtn():
        circle_radius = size * circle_radius_by_size
        gap_radius = circle_radius + circle_back_gap_margin
        center_x = 0
        center_z = (bottom_content_max_z + bottom_content_min_z) / 2

        def create_cylinder(
                radius: float,
                height: float,
                depth: float,
        ) -> Shape:
            z = cylinder(radius, height)
            z = z.rotateX(-pi / 2)
            z = z.translate(
                center_x,
                depth - size / 2,
                center_z
            )
            return z

        z = create_cylinder(
            radius=gap_radius,
            height=circle_back_gap,
            depth=circle_depth,
        )
        z += create_cylinder(
            radius=circle_radius,
            height=circle_depth,
            depth=0,
        )
        z = z.rotateZ(pi / 2)
        z += z.rotateZ(pi / 2)
        return z

    z -= create_circle_sbtn()

    def create_clearance():
        radius = size * clearance_radius_by_size

        def create_outline() -> Shape:
            points: list[tuple[float, float]] = []
            ray_angle = pi / clearance_rays_count
            for i in range(clearance_rays_count * 2):
                angle = i * ray_angle - pi / 2
                r = 1 if i % 2 == 0 else clearance_inner_radius_factor
                x = cos(angle) * r * radius
                y = sin(angle) * r * radius
                points.append((x, y))
            z = polygon(
                pnts=points,
                wire=True,
            )
            return z

        outline = create_outline()

        def get_outline(
                expansion: float,
                z: float,
        ) -> Shape:
            factor = (radius + expansion) / radius
            return outline \
                .scaleXYZ(factor, factor, 1) \
                .up(z)

        z = loft(
            arr=[
                get_outline(
                    expansion=-clearance_chamfer - clearance,
                    z=0,
                ),
                get_outline(
                    expansion=-clearance,
                    z=clearance_chamfer,
                ),
                get_outline(
                    expansion=-clearance,
                    z=clearance_depth,
                ),
            ],
        )

        sbtn = loft(
            arr=[
                get_outline(
                    expansion=clearance_chamfer,
                    z=0,
                ),
                get_outline(
                    expansion=0,
                    z=clearance_chamfer,
                ),
                get_outline(
                    expansion=0,
                    z=clearance_depth + clearance_sbtn_additional_depth,
                ),
            ],
        )

        sbtn = sbtn.rotateX(-pi / 2)
        sbtn = sbtn.translate(
            size / 2 - wall - radius,
            -size / 2,
            (bottom_content_min_z + bottom_content_max_z) / 2,
        )
        sbtn += sbtn.rotateZ(-pi / 2)

        sbtn += loft(
            arr=[
                get_outline(
                    expansion=clearance_chamfer,
                    z=0,
                ),
                get_outline(
                    expansion=0,
                    z=clearance_chamfer,
                ),
                get_outline(
                    expansion=0,
                    z=clearance_depth + clearance_sbtn_additional_depth,
                ),
                get_outline(
                    expansion=-radius + tech,
                    z=clearance_depth + clearance_sbtn_additional_depth + radius,
                )
            ],
        )

        return z, sbtn

    clearance_shape, clearance_sbtn = create_clearance()
    z -= clearance_sbtn
    z += clearance_shape

    z = unify(z)
    return z


test = create_test()
export_to_stl(
    shapes={
        "quality_test": test,
    }
)

disp(test)

show()
