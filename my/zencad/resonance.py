#!/usr/bin/env python3
# coding: utf-8

from utils import *

size = 30
levels_count = 16
level_corner_radius = size * 0.15
level_height = 3
hole_width = 1
hole_depth = 8
hole_x_percentage = 0.2
hole_y_angle = pi / 6


def create_levels():

    def create_holes_level(
            reverse: bool,
    ) -> Shape:

        def create_hole(
                reverse: bool,
        ) -> Shape:
            dx = hole_width / 2
            z = wire_builder(start=(-dx, 0), defrel=True) \
                .segment(hole_width, 0) \
                .segment(0, hole_depth - dx) \
                .arc_by_points(
                (-dx, dx),
                (-hole_width, 0),
            ) \
                .close() \
                .doit()

            x_offset = tan(hole_y_angle) * level_height / 2
            if reverse:
                x_offset = -x_offset

            z = loft(
                arr=[
                    z.left(x_offset),
                    z.right(x_offset).up(level_height)
                ]
            )
            z = z.translate(
                -size / 2 + size * hole_x_percentage,
                -size / 2 - tech,
            )
            return z

        z = create_hole(
            reverse=reverse,
        )

        z += z.rotateZ(pi)
        z += z.rotateZ(pi / 2)
        return z

    even_holes = create_holes_level(
        reverse=False,
    )

    odd_holes = create_holes_level(
        reverse=True,
    )

    def create_holes():

        def create_level_holes(
                level: int,
        ) -> Shape:
            z = odd_holes if level % 2 == 0 else even_holes
            z = z.up(level * level_height)
            return z

        z = union([create_level_holes(i) for i in range(levels_count)])
        z = unify(z)

        return z

    z = box(size, size, level_height * levels_count)
    z = z.fillet(
        level_corner_radius,
        [
            (0, 0, level_height / 2),
            (size, 0, level_height / 2),
            (0, size, level_height / 2),
            (size, size, level_height / 2),
        ]
    )
    z = z.translate(-size/2, -size/2)
    z -= create_holes()
    return z


levels = create_levels()
disp(levels)
export_to_stl(
    shapes={
        "resonance": levels,
    },
)

show()
