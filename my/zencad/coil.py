from utils import *

wall = 2.4
clearance = 0.4
screw_depth = 1.2
content_radius_min = 16
content_radius_max = 28
content_width = 16
side_hole_max_length = 8
side_hole_fillet = wall
screw_length = 6.4

# Calculated
out_screw_radius = content_radius_min - wall
in_screw_radius = out_screw_radius - clearance * sqrt(2)
hole_radius = in_screw_radius - screw_depth - wall
height = wall + content_width + wall
radius = content_radius_max
out_screw_length = screw_length
in_screw_length = out_screw_length + clearance

side_hole_radius_max = radius - wall
side_hole_radius_min = content_radius_min
side_holes_count = floor(side_hole_radius_max * 2 * pi / (side_hole_max_length + wall))

wall_fillet_z, wall_fillet_radius = create_chamfer_fillet_dimensions(wall)


def create_side():
    z = cylinder(radius, wall)
    z = z.chamfer(wall_fillet_z, [(0, radius, 0)])
    z = z.fillet(wall_fillet_radius - tech, [(0, radius, wall_fillet_z)])

    def create_holes():
        hole_height = wall + tech * 2
        z = cylinder(side_hole_radius_max - side_hole_fillet, hole_height)
        z -= cylinder(side_hole_radius_min + side_hole_fillet, hole_height)
        sbtn_width = side_hole_fillet + wall + side_hole_fillet
        sbtn = box(sbtn_width, side_hole_radius_max, hole_height)
        sbtn = sbtn.left(sbtn_width / 2)
        sbtn_angle = pi * 2 / side_holes_count
        sbtn = union([sbtn.rotateZ(i * sbtn_angle) for i in range(side_holes_count)])
        z -= sbtn
        z = z.solids()
        z = union([offset(solid, side_hole_fillet) for solid in z])
        z = z.down(tech)
        return z

    z -= create_holes()
    return z


side = create_side()


def create_base():
    z = side
    under_screw_height = height - in_screw_length
    z += cylinder(content_radius_min, under_screw_height - wall).up(wall)
    in_screw = screw(in_screw_radius, in_screw_length, screw_depth)
    in_screw ^= cone(in_screw_radius - screw_depth + in_screw_length, in_screw_radius - screw_depth, in_screw_length)
    in_screw = in_screw.up(under_screw_height)
    z += in_screw
    z -= cylinder(hole_radius, height)
    z = z.chamfer(wall_fillet_z, [(0, hole_radius, 0), (0, hole_radius, height)])
    z = z.fillet(wall_fillet_radius - tech, [(0, hole_radius, wall_fillet_z), (0, hole_radius, height - wall_fillet_z)])
    return z


base = create_base()


def create_cap():
    z = side
    z += cylinder(content_radius_min, out_screw_length)
    z -= screw(out_screw_radius, out_screw_length, screw_depth)
    sbtn_radius = out_screw_radius
    sbtn = cone(0, sbtn_radius, sbtn_radius)
    sbtn = sbtn.up(out_screw_length - sbtn_radius)
    z -= sbtn
    z = z.rotateX(pi)
    z = z.up(height)
    return z


cap = create_cap()

export_to_stl(
        {
            "base": base,
            "cap": cap.rotateX(pi)
        }
)

display(base, Color(0, 1, 1))
display(cap.up(10), Color(1, 0, 1))
show()
