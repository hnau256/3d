#!/usr/bin/env python3
# coding: utf-8

from utils import *


class ConnectorInfo:

    def __init__(
            self,
            clearance: float = 0.2,
            wall: float = 0.8,
            depth: float = 16.0,
            width: float = 8.0,
            thickness: float = 2.0,
            tooth_depth: float = 1.2,
            tooth_length: float = 3.2,
            length_shrinkage: float = 0.2,
    ):
        self.clearance = clearance
        self.wall = wall
        self.depth = depth
        self.thickness = thickness
        self.width = width
        self.tooth_depth = tooth_depth
        self.tooth_length = tooth_length
        self.length_shrinkage = length_shrinkage

    def _shape(
            self,
            sbtn: bool,
            expansion: float,
            tooth_offset: float,
    ) -> Shape:
        thickness = self.thickness + expansion * 2
        width = self.width + expansion * 2
        depth = self.depth + expansion

        z = box(depth, width, thickness)
        z = z.back(width / 2)
        z = z.down(thickness / 2)

        def teeth():
            dx = self.tooth_length / 2
            cx = self.depth / 2 + tooth_offset
            min_y = width / 2

            z = polysegment(
                pnts=[
                    (cx + dx, min_y, 0),
                    (cx, min_y + self.tooth_depth, 0),
                    (cx - dx, min_y, 0),
                ],
                closed=True,
            )
            z = z.fill()
            z = z.extrude(thickness)
            z = z.down(thickness / 2)
            z += z.mirrorXZ()
            return z

        z += teeth()

        def sbtns():
            w = depth - self.wall * 2
            min_x = self.wall
            l = self.tooth_depth * 2
            min_y = width / 2 - self.wall - l
            r = l / 2
            cy = min_y + r
            z = rectangle(w - r * 2, l)
            z = z.translate(min_x + r, min_y)
            z += circle(r).translate(min_x + r, cy)
            z += circle(r).translate(min_x + w - r, cy)
            z = z.extrude(thickness)
            z = z.down(thickness / 2)
            z += z.mirrorXZ()
            return z

        if sbtn:
            z -= sbtns()

        return z

    def connector(self):
        return self._shape(
            sbtn=True,
            expansion=0,
            tooth_offset=0,
        )

    def connector2(self):
        z = self.connector()
        z += z.mirrorYZ()
        return z

    def sbtn(self):
        return self._shape(
            sbtn=False,
            expansion=self.clearance,
            tooth_offset=self.length_shrinkage,
        )

    def sbtn_size(self) -> (float, float, float):
        return (
            self.depth + self.clearance,
            self.width + self.clearance * 2 + self.tooth_depth * 2,
            self.thickness + self.clearance * 2,
        )


info = ConnectorInfo()

sbtn_width, sbtn_length, sbtn_height = info.sbtn_size()
wall = 0.8
base_width = sbtn_width + wall
base_length = sbtn_length + wall * 2
base_height = sbtn_height + wall * 2
base = box(
    base_width,
    base_length,
    base_height,
).translate(
    0,
    -base_length / 2,
    -base_height / 2,
)
base -= info.sbtn()
disp(base, Color(1, 1, 1, 0.75))

connector = info.connector2()
disp(connector, Color(1, 1, 0))

export_to_stl(
    {
        "base": base.rotateX(pi / 2),
        "connector": connector,
    }
)

show()
