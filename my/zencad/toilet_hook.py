from utils import *

wall = 6.4
bar_width = 20
bar_height = 20
bar_length = 46.5 - 10.1
clearance = 0.2

back_height = 72
front_height = 8
hook_radius = 24

def create_hook():
    width = bar_width + (clearance + wall) * 2
    bar_handle_length = bar_length - clearance * 2

    def create_bar_handle():
        length = bar_handle_length
        height = bar_height + (clearance + wall) * 2
        z = box(width, length, height)
        z = z.translate(-width/2, -length)
        sbtn = box(width-wall*2, length, height-wall*2)
        sbtn = sbtn.translate(-width/2+wall, -length, wall)
        z -= sbtn
        return z

    z = create_bar_handle()

    def create_back_side():
        z = box(width, wall, back_height)
        z = z.translate(-width/2, -wall, -back_height)
        return z

    z += create_back_side()
    z = z.fillet(
        bar_handle_length - wall - tech,
        [
            (0, -wall, 0)
        ]
    )

    def create_circle_side():
        z = cylinder(hook_radius + wall, width)
        z += box(
            hook_radius + wall,
            hook_radius + wall,
            width,
        )
        z -= cylinder(hook_radius, width)
        z = z.rotateY(pi/2)
        z -= halfspace().rotateY(pi)
        z = z.translate(
            -width/2,
            -hook_radius - wall,
            -back_height,
        )
        return z

    z += create_circle_side()

    def create_front_side():
        z = box(width, wall, front_height)
        z = z.fillet(
            wall / 2 - tech,
            [
                (width/2, 0, front_height),
                (width/2, wall, front_height),
            ]
        )
        z = z.translate(-width/2, -wall*2-hook_radius*2, -back_height)
        return z

    z += create_front_side()


    z = unify(z)

    return z


display(create_hook())
show()
