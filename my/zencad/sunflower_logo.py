#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from math import *
from utils import *

petal_width = 8
petal_length = 12
petal_offset_from_center = 3
petals_side_count = 6
petals_skip_count = 0
petal_rotate_angle = pi / 5
petal_thickness = 1
petal_edge_fillet = 1
petal_fillet = 0.3
petal_curve_radius = 12
petals_z = 4.5

text_sbtn_center_radius = 8
text_thickness = 2
text_fillet = 0.17
text_border = 1

seeds_radius = 9
seeds_in_circle = 24
seeds_circles_count = 16
seeds_next_circle_radius_factor = 0.85
seed_depth = 8
seed_height_by_circle_radius = 0.1


def create_seeds():
    seed_in_circle_angle = pi * 2 / seeds_in_circle

    radius = seeds_radius

    def create_circle(
            angle_delta: float,
            circle_radius: float,
    ):
        side_radius = circle_radius * seeds_next_circle_radius_factor
        side_angle = seed_in_circle_angle / 2
        side_y = cos(side_angle) * side_radius
        side_x = sin(side_angle) * side_radius
        base_radius = side_radius * seeds_next_circle_radius_factor

        def create_shape(
                dx: float,
                dy_far: float,
                dy_near: float,
        ):
            b1 = 0.4
            b2 = 0.1
            shape = sew(
                lst=[
                    bezier(
                        pnts=[
                            (-dx, 0),
                            (-dx + b2 * dx, b1 * dy_far),
                            (-b1 * dx, dy_far - b2 * dy_far),
                            (0, dy_far),
                        ],
                    ),
                    bezier(
                        pnts=[
                            (0, dy_far),
                            (b1 * dx, dy_far - b2 * dy_far),
                            (dx - b2 * dx, b1 * dy_far),
                            (dx, 0),
                        ],
                    ),
                    bezier(
                        pnts=[
                            (dx, 0),
                            (dx - b2 * dx, -b1 * dy_near),
                            (b1 * dx, -dy_near + b2 * dy_near),
                            (0, -dy_near),
                        ],
                    ),
                    bezier(
                        pnts=[
                            (0, -dy_near),
                            (-b1 * dx, -dy_near + b2 * dy_near),
                            (-dx + b2 * dx, -b1 * dy_near),
                            (-dx, 0),
                        ],
                    ),
                ],
            )
            return shape

        shape = create_shape(
            side_x,
            circle_radius - side_y,
            side_y - base_radius
        )

        z = shape.fill().extrude(seed_depth)

        def create_cap():
            height = circle_radius * seed_height_by_circle_radius
            levels_count = 4
            levels = []
            for i in range(levels_count):
                h = i / (levels_count - 1)
                h = sqrt(h)
                factor = max(sqrt(1 - h ** 2), tech)
                levels.append(shape.scale(factor).up(h * height))

            z = loft(
                arr=levels,
                smooth=True,
            )
            z = z.up(seed_depth)
            return z

        z += create_cap()
        z = z.forw(side_y)
        z = z.rotateZ(angle_delta)
        z = union([z.rotateZ(seed_in_circle_angle * i) for i in range(seeds_in_circle)])
        return z

    z = nullshape()
    for i in range(seeds_circles_count):
        angle_offset = 0 if i % 2 == 0 else seed_in_circle_angle / 2
        z += create_circle(angle_offset, radius)
        radius *= seeds_next_circle_radius_factor

    return z


seeds = create_seeds()


def create_text():
    register_font('utils/roboto.ttf')
    z = textshape("ПОДСОЛНУХ", "roboto", 64)
    size = z.boundbox()
    width = petal_length * 2 + petal_offset_from_center * 2
    z = z.back(size.ymin + size.ylength() / 2)
    z = z.scale(width / size.xlength())
    z = z.left(width / 2)
    z = z.extrude(text_thickness)
    z = z.fillet(text_fillet)
    return z


text = create_text()

text_sbtn_length = text.boundbox().ylength() / 2 + text_border


def create_petals():
    def create_main_shape():
        length = petal_length / 2
        width = petal_width / 2
        radius = (length ** 2 + width ** 2) / (2 * width)
        z = circle(radius)
        z = z.translate(
            radius - width,
            length,
        )
        z ^= z.mirrorYZ()
        return z

    curve_length = petal_length + petal_offset_from_center + tech

    def create_curve():
        radius = petal_curve_radius
        cone_height = sqrt(curve_length ** 2 - radius ** 2)
        z = cone(0, radius, cone_height)
        z = thicksolid(z, petal_thickness, [(0, 0, cone_height)])
        z = z.rotateX(-pi / 2)
        z = z.rotateX(atan(radius / cone_height))
        z = z.up(petal_thickness)
        return z

    z = create_main_shape()
    extrude_height = curve_length + tech
    z = z.extrude(extrude_height)
    z = z.down(tech)
    z = z.fillet(
        petal_edge_fillet,
        [(0, petal_length, 0)]
    )
    z = z.forw(petal_offset_from_center)
    z ^= create_curve()
    z = z.rotateY(petal_rotate_angle)
    angle_step = pi / (petals_side_count + petals_skip_count)
    z = union([z.rotateZ(i * angle_step) for i in range(petals_side_count)])
    z = z.rotateZ(-pi / 2 + angle_step * petals_skip_count)
    z += z.rotateZ(pi)
    z = z.fillet(petal_fillet)
    z = z.up(petals_z)

    return z


petals = create_petals()

export_to_stl(
    shapes={
        "petals": petals,
        "seeds": seeds,
    },
    delta=0.001,
)


display(seeds, Color(0.03, 0.03, 0.03))
display(petals, Color(1, 0.75, 0))
# display(text, Color(0, 0, 0))
show()
