from utils import *

bowl_out_radius = 92 / 2
bowl_wall = 2
clearance = 0.8
height_bottom = 12
height_top = 8
spoon_hole_radius = 10
spoon_hole_angle = pi / 6

# Calculated
radius_bottom = bowl_out_radius - bowl_wall - clearance
radius_top = bowl_out_radius + clearance


def create_cap():

    def create_bottom():
        z = cylinder(radius_bottom, height_bottom)
        fillet_z, fillet_radius = create_chamfer_fillet_dimensions(height_bottom/2)
        z = z.chamfer(fillet_z, [(0, radius_bottom, 0)])
        z = z.fillet(fillet_radius-tech, [(0, radius_bottom, fillet_z)])
        z = z.down(height_bottom)
        return z

    z = create_bottom()

    def create_top():
        fillet_z, fillet_radius = create_chamfer_fillet_dimensions(height_top/2)
        radius = radius_top + fillet_z
        z = cylinder(radius, height_top)
        z = z.chamfer(fillet_z, [(0, radius, 0), (0, radius, height_top)])
        z = z.fillet(fillet_radius-tech, [(0, radius, fillet_z), (0, radius, height_top-fillet_z)])
        return z

    z += create_top()

    spoon_sbtn = cylinder(spoon_hole_radius, radius_top, center=True)
    spoon_sbtn = spoon_sbtn.rotateY(spoon_hole_angle)
    spoon_sbtn = spoon_sbtn.right(radius_bottom)
    z -= spoon_sbtn

    return z


cap = create_cap()

export_to_stl(
        {
            "cap": cap.rotateX(pi)
        }
)

display(cap)
show()
