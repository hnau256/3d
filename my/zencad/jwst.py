from zencad import *
from utils import *

mirror_side_length = 50
mirror_height = 2

divider_width = 1.6
border_width = 16
mirror_support_width = 3.6
mirror_support_height = 6.4
wall = 1.2
screw_depth = 1.2
pin_radius = 1.75 / 2
nut_hole_length = 4.8
nut_hole_depth = 2.4
handle_hole_radius = 3.6

mirror_clearance_xy = 0.3
mirror_clearance_z = 0.3
plastic_clearance = 0.3
tech = 0.001

# Calculated
cell_side_length = mirror_side_length + (mirror_clearance_xy + divider_width / 2) * tan(pi / 6) * 2
cell_side_part_length_decrease = plastic_clearance / cos(pi / 6)
cell_side_part_length = cell_side_length - cell_side_part_length_decrease

divider_offset = divider_width / 2
border_offset = border_width - divider_offset
mirror_support_offset = divider_offset + mirror_clearance_xy + mirror_support_width

top_height = mirror_clearance_z + mirror_height
total_height = mirror_support_height + top_height

nut_sbtn_height = mirror_support_height - wall
nut_height = nut_sbtn_height - plastic_clearance * 2 * sqrt(2)
nut_sbtn_radius = border_offset - wall * 2
nut_radius = nut_sbtn_radius - plastic_clearance
nut_screw_radius = nut_radius - wall - screw_depth
screw_radius = nut_screw_radius - plastic_clearance * sqrt(2)


def create_pin_sbtn():
    pin_sbtn_radius = pin_radius + plastic_clearance
    z = torus(screw_radius - screw_depth - wall - pin_sbtn_radius, pin_sbtn_radius)
    z = z.up(mirror_support_height / 2)
    return z


pin_sbtn = create_pin_sbtn()


def create_cell_side(
        keep_right_side_for_border: bool,
        keep_near_right_corner: bool,
        keep_far_right_corner: bool,
        handle_hole: bool = False
):
    center_y_offset = cell_side_part_length / 2
    min_max_y_offset_delta = border_offset * tan(pi / 6)
    min_y_offset = center_y_offset - min_max_y_offset_delta
    max_y_offset = center_y_offset + min_max_y_offset_delta
    near_right_y_offset = max_y_offset if keep_near_right_corner else min_y_offset
    far_right_y_offset = max_y_offset if keep_far_right_corner else min_y_offset
    z = polygon(
            pnts=[
                (-border_offset, min_y_offset),
                (0, center_y_offset),
                (border_offset, far_right_y_offset),
                (border_offset, -near_right_y_offset),
                (0, -center_y_offset),
                (-border_offset, -min_y_offset)
            ],
            wire=True
    )
    z = z.fill()
    z = z.extrude(total_height)
    if handle_hole:
        min_x = -mirror_support_offset + wall + handle_hole_radius
        max_x = (border_offset if keep_right_side_for_border else mirror_support_width) - wall - handle_hole_radius
        max_z = mirror_support_height - wall
        hole_sbtn = cylinder(handle_hole_radius, max_z)\
            .left(min_x)
        hole_sbtn += cylinder(handle_hole_radius, max_z-wall)\
            .right(max_x)\
            .up(wall)
        hole_sbtn += box(max_x-min_x, handle_hole_radius*2, max_z-wall)\
            .translate(min_x, -handle_hole_radius, wall)
        z -= hole_sbtn
    mirror_sbtn = box(
            border_offset - divider_offset,
            max_y_offset * 2,
            total_height
    )
    mirror_sbtn_sbtn = box(
            mirror_support_width + mirror_clearance_xy,
            max_y_offset * 2,
            mirror_support_height
    )
    mirror_sbtn_sbtn = mirror_sbtn_sbtn.right(
            border_offset - divider_offset - mirror_support_width - mirror_clearance_xy
    )
    mirror_sbtn -= mirror_sbtn_sbtn
    mirror_sbtn = mirror_sbtn.translate(
            -border_offset,
            -max_y_offset,
            0
    )
    mirror_sbtn_screw_sbtn = cylinder(screw_radius, mirror_support_height).forw(cell_side_length / 2)
    mirror_sbtn -= mirror_sbtn_screw_sbtn
    mirror_sbtn -= mirror_sbtn_screw_sbtn.rotateZ(pi)
    z -= mirror_sbtn
    if not keep_right_side_for_border:
        z -= mirror_sbtn.rotateZ(pi)
    nut_sbtn = cylinder(nut_sbtn_radius, nut_sbtn_height)
    nut_sbtn_screw_blank = cylinder(screw_radius, nut_sbtn_height)
    nut_sbtn_screw_blank = nut_sbtn_screw_blank.chamfer(screw_depth, [(0, screw_radius, 0)])
    nut_sbtn -= screw(screw_radius, nut_sbtn_height, entries=3, blank=nut_sbtn_screw_blank)
    nut_sbtn -= cone(screw_radius - screw_depth, screw_radius, screw_depth).up(nut_sbtn_height - screw_depth)
    nut_sbtn = nut_sbtn.forw(cell_side_length / 2)
    z -= nut_sbtn
    z -= nut_sbtn.rotateZ(pi)
    nut_chamfer_point = point3(0, cell_side_length / 2 - nut_sbtn_radius, 0)
    z = z.chamfer(wall, [nut_chamfer_point, nut_chamfer_point.rotateZ(pi)])
    pin_sbtn_local = pin_sbtn.forw(cell_side_length / 2)
    pin_sbtn_local += pin_sbtn_local.rotateZ(pi)
    z -= pin_sbtn_local
    return z


def create_nut():
    z = cylinder(nut_radius, nut_height)
    z = z.chamfer(wall, [(0, nut_radius, nut_height)])
    z -= screw(nut_screw_radius, nut_height, entries=3)
    chamfer_sbtn = cone(nut_screw_radius, nut_screw_radius - screw_depth, screw_depth)
    z -= chamfer_sbtn
    z -= chamfer_sbtn.mirrorXY().up(nut_height)
    holes_count = floor(pi * (nut_screw_radius + screw_depth) / nut_hole_length / 2)
    hole_angle = pi / holes_count
    hole = box(nut_hole_length, nut_radius * 2, nut_hole_depth)
    hole = hole.translate(-nut_hole_length / 2, -nut_radius)
    hole = union([hole.rotateZ(i * hole_angle) for i in range(holes_count)])
    hole -= cylinder(nut_screw_radius + wall / 2, nut_hole_depth)
    z -= hole
    return z


def copy6(
        shape
):
    return union([shape.rotateZ(i * pi / 3) for i in range(6)])


def create_hexagon():
    min_radius = cell_side_length / 2 * sqrt(3)
    top_radius = (min_radius + divider_offset) * 2 / sqrt(3)
    bottom_radius = (min_radius + mirror_support_offset) * 2 / sqrt(3)
    z = ngon(bottom_radius, 6, wire=True).fill().extrude(mirror_support_height)
    z += ngon(top_radius, 6, wire=True).fill().extrude(top_height).up(mirror_support_height)
    z -= copy6(cylinder(nut_sbtn_radius, nut_sbtn_height).right(cell_side_length))
    nut_chamfer_point = point3(0, cell_side_length / 2 - nut_sbtn_radius, 0)
    z = z.chamfer(wall, [nut_chamfer_point.rotateZ(i * pi / 3) for i in range(6)])
    nut_screw = screw(
            screw_radius,
            nut_sbtn_height,
            entries=3,
            blank=cylinder(screw_radius, nut_sbtn_height).chamfer(screw_depth, [(0, screw_radius, 0)])
    )
    nut_screw = nut_screw.rotateZ(-pi / 6)
    nut_screw += cone(screw_radius - screw_depth, screw_radius, screw_depth).up(nut_sbtn_height - screw_depth)
    nut_screw += cylinder(screw_radius, screw_depth).up(nut_sbtn_height)
    nut_screw = nut_screw.right(cell_side_length)
    z += copy6(nut_screw)
    corner_sbtn_radius = cell_side_part_length_decrease * 2 + screw_radius * 2
    corner_sbtn = ngon(corner_sbtn_radius, 6, wire=True).rotateZ(pi).fill().extrude(total_height)
    corner_sbtn = corner_sbtn.right(cell_side_part_length + corner_sbtn_radius + cell_side_part_length_decrease / 2)
    z -= copy6(corner_sbtn)
    z -= copy6(pin_sbtn.right(cell_side_length))
    z = z.rotateZ(pi / 6)
    return z


def create_mirror():
    z = ngon(mirror_side_length, 6, wire=True)
    z = z.rotateZ(pi / 6)
    z = z.fill()
    z = z.extrude(mirror_height)
    z = z.up(total_height - mirror_height)
    return z


inner_cell_side = create_cell_side(
        keep_right_side_for_border=False,
        keep_far_right_corner=False,
        keep_near_right_corner=False
)

border_out_cell_side = create_cell_side(
        keep_right_side_for_border=True,
        keep_far_right_corner=True,
        keep_near_right_corner=True,
        handle_hole=True
)

border_in_to_out_cell_side = create_cell_side(
        keep_right_side_for_border=True,
        keep_far_right_corner=True,
        keep_near_right_corner=False
)

border_out_to_in_cell_side = create_cell_side(
        keep_right_side_for_border=True,
        keep_far_right_corner=False,
        keep_near_right_corner=True
)

nut = create_nut()

mirror = create_mirror()

hexagon = create_hexagon()


def show_hex(
        object,
        color
):
    for i in range(6):
        display(object.rotateZ(i * pi / 3), color)


dy = cell_side_length / 2
dx = dy * sqrt(3)
plastic_color = Color(0.2, 0.2, 0.2)
display(hexagon, plastic_color)
show_hex(inner_cell_side.forw(dy * 3), plastic_color)
show_hex(inner_cell_side.right(dx * 3), plastic_color)
show_hex(border_out_cell_side.right(dx * 5), plastic_color)
show_hex(inner_cell_side.forw(dy).rotateZ(pi / 3).forw(dy).right(dx * 3), plastic_color)
show_hex(inner_cell_side.forw(dy).rotateZ(-pi / 3).forw(dy).right(dx * 3), plastic_color)
show_hex(inner_cell_side.forw(-dy).rotateZ(pi / 3).forw(-dy).right(dx * 3), plastic_color)
show_hex(inner_cell_side.forw(-dy).rotateZ(-pi / 3).forw(-dy).right(dx * 3), plastic_color)
show_hex(border_out_to_in_cell_side.right(dx).rotateZ(pi / 3).right(dx * 4), plastic_color)
show_hex(border_in_to_out_cell_side.right(dx).rotateZ(-pi / 3).right(dx * 4), plastic_color)
show_hex(border_out_to_in_cell_side.right(dx).rotateZ(pi / 6).right(dy * 6).rotateZ(pi / 6), plastic_color)
show_hex(border_in_to_out_cell_side.right(dx).rotateZ(-pi / 6).right(dy * 6).rotateZ(pi / 6), plastic_color)
show_hex(nut.forw(dy * 2), plastic_color)
show_hex(nut.forw(dy * 4), plastic_color)
show_hex(nut.forw(dy * 2).rotateZ(pi / 3).forw(dy * 4), plastic_color)
show_hex(nut.forw(dy * 2).rotateZ(-pi / 3).forw(dy * 4), plastic_color)
show_hex(nut.forw(dy * 8), plastic_color)
show_hex(nut.forw(dy * 6).rotateZ(pi / 3).forw(dy * 2), plastic_color)
show_hex(nut.forw(dy * 6).rotateZ(pi / 3).forw(dy * 4), plastic_color)
show_hex(nut.forw(dy * 4).rotateZ(pi / 3).forw(dy * 6), plastic_color)
show_hex(nut.forw(dy * 6).rotateZ(-pi / 3).forw(dy * 2), plastic_color)

mirror_color = Color(0.96, 0.83, 0.43)
show_hex(mirror.right(dx * 2), mirror_color)
show_hex(mirror.right(dx * 4), mirror_color)
show_hex(mirror.rotateZ(pi / 6).right(dy * 6).rotateZ(pi / 6), mirror_color)

export_to_stl(
        shapes={
            'nut(x54)': nut.rotateX(pi),
            'hexagon(x1)': hexagon,
            'inner_cell_side(x36)': inner_cell_side,
            'border_out_cell_side(x6)': border_out_cell_side,
            'border_out_to_in_cell_side(x12)': border_out_to_in_cell_side,
            'border_in_to_out_cell_side(x12)': border_in_to_out_cell_side
        }
)

show()
