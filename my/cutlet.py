#!/usr/bin/env python3
# coding: utf-8

from math import *
from zencad import *
from screw import *
from mechanism import *

tech = 0.001
wall = 1.6
friction = 0.4
figure_key_points_count = 100
cutlet_radius = 40
cutlet_height = 16
grip_sbtn_radius = 2.4
grip_sbtns_distance = grip_sbtn_radius * 4
pads_count = 4

screw_style = ScrewStyle()


def create_grip(radius, height):
    radius += grip_sbtn_radius
    result = cylinder(radius, height)
    result = result.chamfer(
        grip_sbtn_radius,
        [(0, radius, 0), (0, radius, height)]
    )
    fillet_radius = grip_sbtn_radius * sqrt(2) * 2
    fillet_radius = min(fillet_radius, height / 2 - grip_sbtn_radius - tech)
    result = result.fillet(
        fillet_radius,
        [(0, radius, grip_sbtn_radius), (0, radius, height - grip_sbtn_radius)]
    )
    sbtns_count = int(radius * 2 * pi / grip_sbtns_distance)
    sbtn_angle = pi * 2 / sbtns_count
    sbtn = cylinder(grip_sbtn_radius, height).right(radius)
    sbtns = union([sbtn.rotateZ(sbtn_angle * i) for i in range(sbtns_count)])
    result -= sbtns
    return result


def create_squircle(s_factor):
    def calc_radius(a, s):
        result = abs(cos(a)) ** s
        result += abs(sin(a)) ** s
        result = result ** (1 / s)
        result = 1 / result
        return result

    a_step = 2 * pi / figure_key_points_count
    result = interpolate(
        pnts=[point3(calc_radius(a_step * i, s_factor), 0).rotateZ(a_step * i) for i in range(figure_key_points_count)],
        closed=True
    )
    result = fill(result)
    max_radius = max(
        calc_radius(0, s_factor),
        calc_radius(pi / 4, s_factor)
    )
    result = result.scale(1 / max_radius)
    if s_factor > 2:
        result = result.rotateZ(pi / 4)
    return result


height = cutlet_height + wall * 2

pads_radius = cutlet_radius + wall
ring_min_radius = pads_radius + friction
screw_radius = ring_min_radius + wall
screw_sbtn_radius = screw_radius + friction * sqrt(2)
screw_out_radius = screw_sbtn_radius + screw_style.depth
out_radius = screw_out_radius + wall


def create_cap():
    cap_z = friction * 2
    cap_height = height / 2 - cap_z
    result = create_grip(out_radius, cap_height)
    result -= screw_style.create_shape(screw_sbtn_radius, cap_height - wall)
    result -= cone(screw_sbtn_radius + screw_style.depth, screw_sbtn_radius, screw_style.depth)
    result = result.up(cap_z)
    return result


cap = create_cap()


def create_ring():
    ring_height = height - (wall + friction) * 2
    result = screw_style.create_shape(screw_radius, ring_height)
    istn_radius = screw_out_radius + tech
    istn = cylinder(istn_radius, ring_height)
    istn = istn.chamfer(
        screw_style.depth,
        [(0, istn_radius, 0), (0, istn_radius, ring_height)]
    )
    result ^= istn
    result -= cylinder(ring_min_radius, ring_height)
    result = result.down(ring_height / 2)
    return result


def create_pads():
    result = cylinder(pads_radius, cutlet_height)
    sbtn = create_squircle(1.4)
    sbtn = sbtn.scale(cutlet_radius)
    sbtn = sbtn.extrude(cutlet_height)
    result -= sbtn
    pad_angle = pi * 2 / pads_count
    ray = box(pads_radius + tech, friction, cutlet_height).back(friction / 2)
    rays = union([ray.rotateZ(pad_angle * i) for i in range(pads_count)])
    result -= rays
    result = result.down(cutlet_height / 2)
    istn = cylinder(pads_radius + tech, cutlet_height + tech * 2, yaw=pad_angle, center=True).down(tech)
    return [result ^ istn.rotateZ(i * pad_angle) for i in range(pads_count)]


objects = [
    MechanismObject(
        model=create_ring(),
        to_stl_name="ring",
        color=Color(0, 1, 1)
    ),
    MechanismObject(
        model=cap,
        color=Color(1, 0, 1, 0.75)
    ),
    MechanismObject(
        model=cap.rotateX(pi),
        to_stl_name="cap",
        color=Color(1, 0, 1)
    )
]

pads = create_pads()
for i in range(pads_count):
    objects.append(
        MechanismObject(
            model=pads[i],
            to_stl_name="pad_" + str(i),
            color=Color(1, 1, 0)
        )
    )

mechanism(
    objects=objects
)
