#!/usr/bin/env python3
# coding: utf-8

from mechanism import *


segment_length = 40
wall = 2.0
friction = 0.2
tech = 0.01
height = 10
separator_radius = 3.6

base_bevel_angle = 0.615
side_bevel_angle = 0.955

segments_count = 5


side_length = segments_count * segment_length
radius_factor = tan(pi/6) / 2
radius = side_length * radius_factor



def side_triangle(offset: float = 0):
    result = halfspace()
    result = result.up(height)
    result ^= halfspace().mirrorXY()
    result = cylinder(side_length+offset, height)
    sbtn_offset = radius + offset * radius_factor
    sbtn = halfspace()
    sbtn = sbtn.rotateX(-pi/2-base_bevel_angle)
    sbtn = sbtn.back(sbtn_offset)
    result -= sbtn
    sbtn = halfspace()
    sbtn = sbtn.rotateX(-pi/2-side_bevel_angle)
    sbtn = sbtn.back(sbtn_offset)
    sbtn = sbtn.rotateZ(-pi*2/3)
    result -= sbtn
    sbtn = sbtn.mirrorYZ()
    result -= sbtn
    return result

def side():
    result = cylinder(side_length + wall, height)
    out_triangle_sbtn = halfspace().rotateX(-pi/2).back(radius+wall)
    result -= out_triangle_sbtn
    result -= out_triangle_sbtn.rotateZ(pi*2/3)
    result -= out_triangle_sbtn.rotateZ(-pi*2/3)
    result -= out_triangle_sbtn
    result -= side_triangle(0)
    return result


def grid():
    size = separator_radius * sqrt(2)
    line = box((side_length + tech) * 2, size, separator_radius, center=True)
    line = line.rotateX(pi/4)
    step = sqrt(3)/2 * segment_length
    line = line.back(radius-step)
    line -= halfspace()
    lines = union([line.forw(step*i) for i in range(segments_count-1)])
    result = lines
    result += lines.rotateZ(pi/3*2)
    result += lines.rotateZ(-pi/3*2)
    result ^= side_triangle(-friction)
    return result

mechanism(
    use_mask=False,
    objects=[
        MechanismObject(
            model=side(),
            color=color(1, 0, 1),
            to_stl_name="pyramid_side"
        ),
        MechanismObject(
            model=grid(),
            color=color(1, 1, 0),
            to_stl_name="grid"
        )
    ]
)