package vase

import vase.utils.OpenSimplexNoise
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


fun interface PatternRadiusPercentageCalculator {

    data class Coordinates(
        val pointCoordinates: PointCoordinates,
        val formRadiusPercentage: Double
    )

    companion object {

        val min = PatternRadiusPercentageCalculator { 0.0 }
        val max = PatternRadiusPercentageCalculator { 1.0 }

    }

    fun calcRadiusPercentage(
        coordinates: Coordinates
    ): Double

}

inline fun PatternRadiusPercentageCalculator.Companion.zeroSymmetric(
    crossinline calcRadiusPercentage: (coordinates: PatternRadiusPercentageCalculator.Coordinates) -> Double
) = PatternRadiusPercentageCalculator { coordinates ->
    (calcRadiusPercentage(coordinates) + 1) / 2
}

fun PatternRadiusPercentageCalculator.Companion.spiral(
    entersCount: Int = 10,
    turnoversCount: Int = entersCount
) = zeroSymmetric { (point) ->
    sin((point.anglePercentage * entersCount + point.zPercentage * turnoversCount) * 2 * PI)
}

inline fun PatternRadiusPercentageCalculator.Companion.combine(
    first: PatternRadiusPercentageCalculator,
    second: PatternRadiusPercentageCalculator,
    crossinline combinator: (Double, Double) -> Double
) = PatternRadiusPercentageCalculator { coordinates ->
    combinator(
        first.calcRadiusPercentage(coordinates),
        second.calcRadiusPercentage(coordinates)
    )
}

inline fun PatternRadiusPercentageCalculator.combineWith(
    other: PatternRadiusPercentageCalculator,
    crossinline combinator: (Double, Double) -> Double
) = PatternRadiusPercentageCalculator.combine(
    first = this,
    second = other,
    combinator = combinator
)

operator fun PatternRadiusPercentageCalculator.times(
    other: PatternRadiusPercentageCalculator
) = combineWith(
    other = other,
    combinator = Double::times
)

operator fun PatternRadiusPercentageCalculator.plus(
    other: PatternRadiusPercentageCalculator
) = combineWith(
    other = other,
    combinator = Double::plus
)

fun PatternRadiusPercentageCalculator.reverse() = PatternRadiusPercentageCalculator { coordinates ->
    this@reverse.calcRadiusPercentage(
        coordinates.copy(
            pointCoordinates = coordinates.pointCoordinates.copy(
                anglePercentage = 1.0 - coordinates.pointCoordinates.anglePercentage
            )
        )
    )
}

inline fun PatternRadiusPercentageCalculator.map(
    crossinline transform: (Double) -> Double
) = PatternRadiusPercentageCalculator { coordinates ->
    this@map.calcRadiusPercentage(coordinates).let(transform)
}

fun PatternRadiusPercentageCalculator.Companion.noise(
    height: Double,
    radius: Double,
    seed: Long = 0,
    scale: Double = 1.0
) = OpenSimplexNoise(seed).let { noise ->
    val scaledRadius = radius * scale
    val scaledHeight = height * scale
    PatternRadiusPercentageCalculator.zeroSymmetric { (point, formRadiusPercentage) ->
        val angle = point.anglePercentage * 2 * PI
        val z = scaledHeight * point.zPercentage
        val actualRadius = scaledRadius * formRadiusPercentage
        noise.eval(
            cos(angle) * actualRadius,
            sin(angle) * actualRadius,
            z
        )
    }
}

fun PatternRadiusPercentageCalculator.Companion.waves(
    height: Double,
    radius: Double,
    seed: Long = 6,
    scale: Double = 0.03,
    wavesCount: Int = 16,
    bridgeWidthPercentage: Double = 0.8
) = PatternRadiusPercentageCalculator
    .noise(height, radius, seed, scale)
    .map {
        val i = (it * wavesCount).toInt()
        val v = it * wavesCount - i
        val r = if (i % 2 == 0) v else 1 - v
        r
    }
    .map {
        if (it < bridgeWidthPercentage) {
            (it / bridgeWidthPercentage).let {
                1 - sqrt(1 - it * it)
            }
        } else {
            1.0
        }
    }
//.map { (sin(it * wavesCountFactor) + 1) / 2 }
//.map { sin(it * PI / 2) }
