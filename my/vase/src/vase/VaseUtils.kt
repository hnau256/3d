package vase

import stl.Point
import stl.Triangle
import kotlin.math.sqrt


object VaseUtils {

    fun createCap(
        centerPoint: Point,
        sidePoints: List<Point>
    ) = sidePoints.indices.map { iPoint ->
        val nextIAngle = (iPoint + 1) % sidePoints.size
        Triangle(
            centerPoint,
            sidePoints[nextIAngle],
            sidePoints[iPoint]
        )
    }

    fun offsetRadius(
        from: Double,
        to: Double,
        offsetPercentage: Double
    ): Double {
        val percentage = 1 - sqrt(1 - offsetPercentage * offsetPercentage)
        return from + (to - from) * percentage
    }

    data class OffsetTarget<P>(
        val minOffsetZ: Double,
        val maxOffsetZ: Double,
        val fromUp: Boolean = false,
        val calcToRadius: (P) -> Double
    )

    fun <P> offsetRadius(
        from: Double,
        z: Double,
        param: P,
        targets: List<OffsetTarget<P>>
    ) = targets
        .find { (minOffsetZ, maxOffsetZ) ->
            minOffsetZ <= z && z <= maxOffsetZ
        }
        ?.let { (minOffsetZ, maxOffsetZ, fromUp, calcToRadius) ->
            val to = calcToRadius(param)
            val fromZ = if (fromUp) maxOffsetZ else minOffsetZ
            val toZ = if (fromUp) minOffsetZ else maxOffsetZ
            val percentage = (z - fromZ) / (toZ - fromZ)
            offsetRadius(from, to, percentage)
        }
        ?: from

}