package vase


class FormWithPatternShape(
    override val height: Double,
    private val formRadiusPercentageCalculator: FormRadiusPercentageCalculator,
    private val patternRadiusPercentageCalculator: PatternRadiusPercentageCalculator,
    info: Info = Info()
) : Shape {

    data class Info(
        val baseRadiusByHeight: Double = 0.3,
        val formRadiusDeltaByFormBaseRadius: Double = 0.5,
        val patternRadiusDeltaByFormBaseRadius: Double = 0.05
    )


    override val baseRadius = height * info.baseRadiusByHeight
    private val formRadiusDelta = baseRadius * info.formRadiusDeltaByFormBaseRadius
    private val formMinRadius = baseRadius - formRadiusDelta

    override val maxAdditionalRadius = baseRadius * info.patternRadiusDeltaByFormBaseRadius

    fun calcMainRadius(formRadiusPercentage: Double) =
        formMinRadius + formRadiusDelta * formRadiusPercentage

    fun calcAdditionalRadius(patternRadiusPercentage: Double) =
        maxAdditionalRadius * patternRadiusPercentage

    override fun calcPoint(
        pointCoordinates: PointCoordinates
    ) = calcMainRadius(
        formRadiusPercentage = formRadiusPercentageCalculator.calcRadiusPercentage(pointCoordinates)
    ).let { mainRadius ->
        ShapePoint(
            mainRadius = mainRadius,
            additionalRadius = calcAdditionalRadius(
                patternRadiusPercentage = patternRadiusPercentageCalculator.calcRadiusPercentage(
                    PatternRadiusPercentageCalculator.Coordinates(
                        pointCoordinates, mainRadius / baseRadius
                    )
                )
            )
        )
    }


}