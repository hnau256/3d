package vase


data class PointCoordinates(
    val zPercentage: Double,
    val anglePercentage: Double
)