package vase


interface Shape {

    val height: Double

    val baseRadius: Double
    val maxAdditionalRadius: Double

    fun calcPoint(
        pointCoordinates: PointCoordinates
    ): ShapePoint

}