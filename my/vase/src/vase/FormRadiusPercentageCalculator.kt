package vase

import vase.utils.OpenSimplexNoise
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin


fun interface FormRadiusPercentageCalculator {

    companion object {

        val min = FormRadiusPercentageCalculator { 0.0 }
        val max = FormRadiusPercentageCalculator { 1.0 }

    }

    fun calcRadiusPercentage(
        coordinates: PointCoordinates
    ): Double

}

inline fun FormRadiusPercentageCalculator.Companion.zeroSymmetric(
    crossinline calcRadiusPercentage: (coordinates: PointCoordinates) -> Double
) = FormRadiusPercentageCalculator { coordinates ->
    (calcRadiusPercentage(coordinates) + 1) / 2
}

fun FormRadiusPercentageCalculator.Companion.classic() = zeroSymmetric { (zPercentage) ->
    sin(zPercentage * 5.4 - 0.25)
}

fun FormRadiusPercentageCalculator.Companion.pot() = zeroSymmetric { (zPercentage) ->
    sin(zPercentage * 1.5 + 0.25)
}

fun FormRadiusPercentageCalculator.Companion.oval() = zeroSymmetric { (zPercentage) ->
    cos(zPercentage * 4 + 4.8)
}

fun FormRadiusPercentageCalculator.Companion.milkHolder() =
    FormRadiusPercentageCalculator { (zPercentage, anglePercentage) ->
        val top = (cos(anglePercentage * 4 * PI) + 1) / 2
        val bottom = top * 0.7
        bottom + (top - bottom) * zPercentage
    }