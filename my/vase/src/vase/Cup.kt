package vase

import stl.Dimension
import stl.Point
import stl.Points
import stl.Triangle
import stl.toDimension
import kotlin.math.PI
import kotlin.math.ceil
import kotlin.math.cos
import kotlin.math.sin


data class Cup(
    val triangles: List<Triangle>,
    val topPoints: List<Point>
) {

    companion object {

        private enum class RowType {

            even, odd;

            companion object {

                fun determinte(
                    row: Int,
                    rowsCount: Int
                ) = when ((rowsCount - row) % 2 == 0) {
                    true -> even
                    false -> odd
                }

            }

            fun getIAngleOffset() = when (this) {
                even -> 0.0
                odd -> 0.5
            }

        }

        fun create(
            angleStepsCount: Int,
            minZ: Double,
            maxZ: Double,
            shape: Shape,
            toRadius: ShapePoint.(z: Double) -> Double,
            density: Double = 1.0
        ): Cup {

            val zDelta = maxZ - minZ
            val zStepsCount = ceil(zDelta * density).toInt()

            fun createPoint(
                z: Double,
                anglePercentage: Double,
                radius: Double
            ): Point {
                val angle = anglePercentage * 2 * PI
                return Point(
                    first = (cos(angle) * radius).toDimension(),
                    second = (sin(angle) * radius).toDimension(),
                    third = z.toDimension(),
                )
            }

            fun normalizeIAngle(iAngle: Int) = iAngle % angleStepsCount

            val points = (0 until zStepsCount).map { iZ ->
                val zPercentage = iZ.toDouble() / (zStepsCount - 1).toDouble()
                val z = minZ + zPercentage * zDelta
                val iAngleOffset = RowType.determinte(iZ, zStepsCount).getIAngleOffset()
                (0 until angleStepsCount).map { iAngle ->
                    val anglePercentage = (iAngle + iAngleOffset).toDouble() / angleStepsCount.toDouble()
                    val pointCoordinates = PointCoordinates(
                        zPercentage = zPercentage,
                        anglePercentage = anglePercentage
                    )
                    val radius = shape.calcPoint(pointCoordinates).run { toRadius(z) }
                    createPoint(
                        z = z,
                        radius = radius,
                        anglePercentage = anglePercentage
                    )
                }
            }

            fun createSide(
                points: List<List<Point>>
            ) = (0 until points.size - 1).flatMap { iZ ->
                (0 until angleStepsCount).flatMap { iAngle ->
                    val p00 = points[iZ][iAngle]
                    val nextIZ = iZ + 1
                    val p01 = points[nextIZ][iAngle]
                    val nextIAngle = normalizeIAngle(iAngle + 1)
                    val p10 = points[iZ][nextIAngle]
                    val p11 = points[nextIZ][nextIAngle]
                    when (RowType.determinte(iZ, zStepsCount)) {
                        RowType.even -> listOf(
                            Triangle(p00, p10, p01),
                            Triangle(p10, p11, p01)
                        )
                        RowType.odd -> listOf(
                            Triangle(p00, p10, p11),
                            Triangle(p00, p11, p01)
                        )
                    }
                }
            }

            val sideSurface = createSide(
                points = points
            )
            val bottomSurface = VaseUtils.createCap(
                centerPoint = Point(
                    Dimension.zero,
                    Dimension.zero,
                    minZ.toDimension()
                ),
                sidePoints = points.first()
            )

            return Cup(
                triangles = sideSurface + bottomSurface,
                topPoints = points.last()
            )

        }

    }


}