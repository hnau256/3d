package vase

import stl.Dimension
import stl.Point
import stl.Solid
import stl.toDimension
import stl.turn
import kotlin.math.PI
import kotlin.math.ceil


fun Vase(
    shape: Shape,
    density: Double = 1.0,
    wall: Double? = null,
    holeFilletByHeight: Double = 0.05,
    additionalRadiusFilletByHeight: Double = 0.2
): Solid {

    val circleLength = 2 * PI * shape.baseRadius
    val angleStepsCount = ceil(circleLength * density).toInt()
    val additionalRadiusFillet = shape.height * additionalRadiusFilletByHeight

    val outCup = run {

        val targets = listOf(
            VaseUtils.OffsetTarget<ShapePoint>(
                minOffsetZ = shape.height - additionalRadiusFillet,
                maxOffsetZ = shape.height
            ) {
                it.mainRadius + shape.maxAdditionalRadius
            },
            VaseUtils.OffsetTarget<ShapePoint>(
                minOffsetZ = 0.0,
                maxOffsetZ = additionalRadiusFillet,
                fromUp = true
            ) {
                it.mainRadius + shape.maxAdditionalRadius
            }
        )

        Cup.create(
            angleStepsCount = angleStepsCount,
            minZ = 0.0,
            maxZ = shape.height,
            density = density,
            shape = shape,
            toRadius = { z ->
                VaseUtils.offsetRadius(
                    param = this,
                    from = mainRadius + additionalRadius,
                    z = z,
                    targets = targets
                )
            }
        )
    }

    return Solid(
        name = "Vase",
        triangles = run {
            val topInnerSurfeces = wall
                ?.let { wallValue ->
                    val holeFillet = shape.height * holeFilletByHeight
                    val targets = listOf(
                        VaseUtils.OffsetTarget<ShapePoint>(
                            minOffsetZ = shape.height - holeFillet,
                            maxOffsetZ = shape.height
                        ) {
                            it.mainRadius + shape.maxAdditionalRadius
                        },
                        VaseUtils.OffsetTarget<ShapePoint>(
                            minOffsetZ = wall,
                            maxOffsetZ = wall + holeFillet,
                            fromUp = true
                        ) {
                            it.mainRadius - wall - holeFillet
                        }
                    )
                    val inCup = Cup.create(
                        angleStepsCount = angleStepsCount,
                        minZ = wallValue,
                        maxZ = shape.height,
                        density = density,
                        shape = shape,
                        toRadius = { z ->
                            VaseUtils.offsetRadius(
                                param = this,
                                from = mainRadius - wallValue,
                                z = z,
                                targets = targets
                            )
                        }
                    )
                    inCup.triangles.turn()
                }
                ?: VaseUtils.createCap(
                    centerPoint = Point(
                        Dimension.zero,
                        Dimension.zero,
                        shape.height.toDimension()
                    ),
                    sidePoints = outCup.topPoints
                )
            outCup.triangles + topInnerSurfeces
        }
    )

}