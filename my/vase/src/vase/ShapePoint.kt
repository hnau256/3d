package vase


data class ShapePoint(
    val mainRadius: Double,
    val additionalRadius: Double
)