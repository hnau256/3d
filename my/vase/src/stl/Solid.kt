package stl

import java.io.Writer

data class Solid(
        val name: String,
        val triangles: List<Triangle>
) {

    fun toStl(
            writer: Writer,
            precision: Int,
    ) {
        writer.appendLine("solid $name")
        triangles.forEach { triangle ->
            triangle.toStl(writer, precision)
        }
        writer.write("endsolid $name")
    }

    override fun toString() = "$name(triangles: ${triangles.size})"
}