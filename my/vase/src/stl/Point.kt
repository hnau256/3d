package stl

import java.io.Writer

typealias Point = Triple<Dimension, Dimension, Dimension>

object Points {

    val zero = Point(Dimension.zero, Dimension.zero, Dimension.zero)

}

fun Point.toStl(
        writer: Writer,
        precision: Int,
) {
    val dimensions = toList()
    val dimensionsCount = dimensions.size
    dimensions.forEachIndexed { index, dimension ->
        dimension.toStl(writer, precision)
        if (index < dimensionsCount - 1) {
            writer.write(" ")
        }
    }
}
