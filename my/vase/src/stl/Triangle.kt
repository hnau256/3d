package stl

import java.io.Writer

typealias Triangle = Triple<Point, Point, Point>

fun Triangle.toStl(
    writer: Writer,
    precision: Int,
) {
    writer.write("facet normal ")
    Points.zero.toStl(writer, precision)
    writer.appendLine()
    writer.appendLine("outer loop")
    toList().forEach { point ->
        writer.write("vertex ")
        point.toStl(writer, precision)
        writer.appendLine()
    }
    writer.appendLine("endloop")
    writer.appendLine("endfacet")
}

fun Triangle.turn() = Triple(first, third, second)

fun Iterable<Triangle>.turn() = map(Triangle::turn)