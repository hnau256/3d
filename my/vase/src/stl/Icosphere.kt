package stl

import kotlin.math.pow
import kotlin.math.sqrt
import stl.Point as StlPoint

class Icosphere private constructor(
    private val points: List<Point>,
    private val triangles: List<Triangle>,
) {

    private data class Point(
        val x: Double,
        val y: Double,
        val z: Double,
    ) {

        operator fun times(
            factor: Double,
        ): Point = Point(
            x = x * factor,
            y = y * factor,
            z = z * factor,
        )

        fun toStlPoint(): StlPoint = StlPoint(
            first = x.toDimension(),
            second = y.toDimension(),
            third = z.toDimension(),
        )

        companion object {

            val zero = Point(
                x = 0.0,
                y = 0.0,
                z = 0.0,
            )
        }
    }

    private data class Triangle(
        val a: Int,
        val b: Int,
        val c: Int,
    )

    private val edgeLength: Double
        get() {
            val firstTriangle = triangles[0]
            val a = points[firstTriangle.a]
            val b = points[firstTriangle.b]
            return a.distanceTo(b)
        }

    private fun improve(
        radius: Double,
    ): Icosphere {
        val points = points.toMutableList()
        val additionalPointsIndexes = mutableMapOf<Pair<Int, Int>, Int>()

        fun getAdditionalPointIndex(
            fromPointIndex: Int,
            toPointIndex: Int,
        ): Int {
            val indexes = when (fromPointIndex < toPointIndex) {
                true -> fromPointIndex to toPointIndex
                false -> toPointIndex to fromPointIndex
            }
            return additionalPointsIndexes.getOrPut(indexes) {
                val (indexMin, indexMax) = indexes
                val pointMin = points[indexMin]
                val pointMax = points[indexMax]
                val middlePoint = Point(
                    x = (pointMin.x + pointMax.x) / 2,
                    y = (pointMin.y + pointMax.y) / 2,
                    z = (pointMin.z + pointMax.z) / 2,
                )
                val point = middlePoint * (radius / middlePoint.distanceTo(Point.zero))
                points.add(point)
                points.size - 1
            }
        }

        val triangles = triangles.flatMap { (a, b, c) ->
            val ab = getAdditionalPointIndex(a, b)
            val bc = getAdditionalPointIndex(b, c)
            val ca = getAdditionalPointIndex(c, a)
            listOf(
                Triangle(a, ab, ca),
                Triangle(ab, bc, ca),
                Triangle(ab, b, bc),
                Triangle(bc, c, ca),
            )
        }

        return Icosphere(
            points = points,
            triangles = triangles,
        )
    }

    fun toSolid(
        name: String,
    ): Solid = Solid(
        name = name,
        triangles = points
            .map(Point::toStlPoint)
            .let { points ->
                triangles.map { (a, b, c) ->
                    Triangle(
                        first = points[a],
                        second = points[b],
                        third = points[c],
                    )
                }
            }
    )

    companion object {

        fun create(
            radius: Double,
            maxEdgeLength: Double,
        ): Icosphere {
            var result = Icosphere(
                triangles = base.triangles,
                points = base.points.map { point -> point * radius },
            )
            while (result.edgeLength > maxEdgeLength) {
                result = result.improve(radius)
            }
            return result
        }

        private fun Point.distanceTo(
            other: Point,
        ): Double = sqrt((x - other.x).pow(2) + (y - other.y).pow(2) + (z - other.z).pow(2))


        private val base: Icosphere = run {
            val x = 0.5257311121191336
            val z = 0.8506508083520399
            val n = 0.0
            Icosphere(
                points = listOf(
                    Point(-x, n, z),
                    Point(x, n, z),
                    Point(-x, n, -z),
                    Point(x, n, -z),
                    Point(n, z, x),
                    Point(n, z, -x),
                    Point(n, -z, x),
                    Point(n, -z, -x),
                    Point(z, x, n),
                    Point(-z, x, n),
                    Point(z, -x, n),
                    Point(-z, -x, n),
                ),

                triangles = listOf(
                    Triangle(4, 0, 1),
                    Triangle(9, 0, 4),
                    Triangle(5, 9, 4),
                    Triangle(5, 4, 8),
                    Triangle(8, 4, 1),
                    Triangle(10, 8, 1),
                    Triangle(3, 8, 10),
                    Triangle(3, 5, 8),
                    Triangle(2, 5, 3),
                    Triangle(7, 2, 3),
                    Triangle(10, 7, 3),
                    Triangle(6, 7, 10),
                    Triangle(11, 7, 6),
                    Triangle(0, 11, 6),
                    Triangle(1, 0, 6),
                    Triangle(1, 6, 10),
                    Triangle(0, 9, 11),
                    Triangle(11, 9, 2),
                    Triangle(2, 9, 5),
                    Triangle(2, 7, 11),

                    )
            )
        }
    }
}