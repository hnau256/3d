package stl

fun Cube() = Solid(
        name = "stl.Cube",
        triangles = (0..1).flatMap { x1 ->
            (0..1).flatMap { y1 ->
                (0..1).flatMap { z1 ->
                    val point1 = Triple(x1, y1, z1)
                    (x1..1).flatMap { x2 ->
                        (y1..1).flatMap { y2 ->
                            (z1..1).flatMap { z2 ->
                                val point2 = Triple(x2, y2, z2)
                                (x2..1).flatMap { x3 ->
                                    (y2..1).flatMap { y3 ->
                                        (z2..1).flatMap { z3 ->
                                            val point3 = Triple(x3, y3, z3)

                                            fun checkTriangleExists(): Boolean {

                                                fun calcChangedAxis(
                                                        point1: Triple<Int, Int, Int>,
                                                        point2: Triple<Int, Int, Int>
                                                ) = (point2.first - point1.first) +
                                                        (point2.second - point1.second) +
                                                        (point2.third - point1.third)

                                                if (calcChangedAxis(point1, point2) != 1) {
                                                    return false
                                                }

                                                if (calcChangedAxis(point2, point3) != 1) {
                                                    return false
                                                }

                                                return true
                                            }

                                            if (checkTriangleExists()) {
                                                listOf(
                                                        Triangle(
                                                                Point(x1.toDimension(), y1.toDimension(), z1.toDimension()),
                                                                Point(x2.toDimension(), y2.toDimension(), z2.toDimension()),
                                                                Point(x3.toDimension(), y3.toDimension(), z3.toDimension())
                                                        )
                                                )
                                            } else {
                                                emptyList<Triangle>()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
)