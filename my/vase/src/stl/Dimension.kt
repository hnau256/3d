package stl

import java.io.Writer
import java.math.BigDecimal
import java.math.RoundingMode

@JvmInline
value class Dimension(
    val value: Double,
) {

    fun toStl(
        writer: Writer,
        precision: Int,
    ) {
        writer.write(BigDecimal.valueOf(value).setScale(precision, RoundingMode.HALF_DOWN).toString())
    }

    companion object {

        val zero = 0.toDimension()

    }
}

fun Number.toDimension() = Dimension(toDouble())