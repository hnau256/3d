import stl.*
import utils.map
import vase.*
import java.io.File
import java.io.FileWriter
import kotlin.math.*
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@OptIn(ExperimentalTime::class)
fun main(args: Array<String>) {

    val rb = 0.02
    val rbf = 0.2
    val b = 0.055
    val r = 0.05
    val rcf = 0.4
    val rc = 0.22

    val xb_x2 = sqrt((rb + rbf).pow(2) - (r + rbf - b).pow(2))
    val x2 = xb_x2 + rb
    val xb_x1 = rb / (rb + rbf) * xb_x2
    val x1 = xb_x1 + rb

    val x5 = 1 - rc
    val x3_xc = sqrt((rc + rcf).pow(2) - (r + rcf).pow(2))
    val x3 = x5 - x3_xc
    val x3_x4 = rcf / (rc + rcf) * x3_xc
    val x4 = x3 + x3_x4

    val yb = b
    val ybf = r + rbf
    val ycf = r + rcf
    val yc = 0.0

    fun circle(
        cx: Double,
        cy: Double,
        r: Double,
        x: Double,
        bottom: Boolean,
    ): Double {
        val dy = sqrtOrZero(r.pow(2) - (x - cx).pow(2))
        return cy + when (bottom) {
            true -> -dy
            false -> dy
        }
    }

    val wall = 0.8

    val rattle: Solid
    val vaseGeneratingDuration = measureTime {
        val height = 150.0
        val info = FormWithPatternShape.Info(
            baseRadiusByHeight = rc,
            formRadiusDeltaByFormBaseRadius = 1.0,
            patternRadiusDeltaByFormBaseRadius = 0.015
        )
        val density = 6.0
        val out = Vase(
            density = density,
            wall = null,
            shape = FormWithPatternShape(
                height = height,
                formRadiusPercentageCalculator = { (x) ->
                    val y = when {
                        x < x1 -> circle(
                            cx = rb,
                            cy = yb,
                            r = rb,
                            x = x,
                            bottom = false,
                        )

                        x < x2 -> circle(
                            cx = x2,
                            cy = ybf,
                            r = rbf,
                            x = x,
                            bottom = true,
                        )

                        x < x3 -> r
                        x < x4 -> circle(
                            cx = x3,
                            cy = ycf,
                            r = rcf,
                            x = x,
                            bottom = true,
                        )

                        else -> circle(
                            cx = x5,
                            cy = yc,
                            r = rc,
                            x = x,
                            bottom = false,
                        )
                    }
                    y / rc
                },
                patternRadiusPercentageCalculator = PatternRadiusPercentageCalculator.zeroSymmetric { (coordinates) ->
                    val (z, angle) = coordinates
                    var zn = z - rb
                    zn = when {
                        zn <= 0 -> zn
                        else -> sqrt(zn)
                    }
                    sin((zn * 2 + angle) * 20 * PI)
                }.let { waves ->
                    PatternRadiusPercentageCalculator { coordinates ->
                        val wave = waves.calcRadiusPercentage(coordinates)
                        val x = coordinates.pointCoordinates.zPercentage
                        val factor = when {
                            x < rb -> 0.0
                            x < x2 -> {
                                val fraction = (x - rb) / (x2 - rb)
                                1 - (cos(fraction * PI) + 1) / 2
                            }

                            x < x3 -> 1.0
                            x < x5 -> {
                                val fraction = (x - x3) / (x5 - x3)
                                (cos(fraction * PI) + 1) / 2
                            }

                            else -> 0.0
                        }
                        wave * factor
                    }
                },
                info = info
            )
        )
        val sphereTriangles = generateSbtnTriangles(
            radius = rc * height - wall,
            maxEdgeLength = 1 / density,
        )
            .map { (a, b, c) ->
                Triple(a, c, b).map { (x, y, z) ->
                    Point(x, y, Dimension(z.value + x5 * height))
                }
            }
        rattle = out.copy(
            triangles = out.triangles + sphereTriangles,
        )
    }
    println("$rattle was generated in $vaseGeneratingDuration")
    val filename = "${rattle.name.toLowerCase()}.stl"
    val vaseSavingDuration = measureTime {
        val tempFile = File("${filename}.tmp")
        FileWriter(tempFile).use { rattle.toStl(it, 3) }
        tempFile.renameTo(File(filename))
    }
    println("$rattle was saved to $filename in $vaseSavingDuration")
}

private fun sqrtOrZero(
    x: Double,
): Double = when {
    x <= 0 -> 0.0
    else -> sqrt(x)
}

private fun generateSbtnTriangles(
    radius: Double,
    maxEdgeLength: Double,
): List<Triple<Point, Point, Point>> {
    val minTrunkRadius = 2.0
    val maxTrunkRadius = radius / 2
    val trunkRadiusesDelta = maxTrunkRadius - minTrunkRadius
    val trunkMaxZ = sqrt(radius.pow(2) - maxTrunkRadius.pow(2))
    val trunkCircleRadius = (trunkMaxZ.pow(2) + trunkRadiusesDelta.pow(2)) / (2 * trunkRadiusesDelta)
    val trunkCenterOffset = trunkCircleRadius + minTrunkRadius
    val trunkMaxAngle = atan(trunkMaxZ / (trunkCircleRadius - maxTrunkRadius + minTrunkRadius))
    val trunkSideLength = trunkMaxAngle * trunkCircleRadius
    val trunkSegments = ceil(trunkSideLength / maxEdgeLength).toInt()
    val trunkSegmentAngle = trunkMaxAngle / trunkSegments
    val sphereMaxAngle = atan(trunkMaxZ / maxTrunkRadius)
    val sphereSideLength = sphereMaxAngle * radius
    val sphereSegments = ceil(sphereSideLength / maxEdgeLength).toInt()
    val sphereSegmentAngle = sphereMaxAngle / sphereSegments


    val sectionXZs = buildList {
        addAll(
            (1..sphereSegments).map { i ->
                val angle = i * sphereSegmentAngle
                val x = cos(angle) * radius
                val z = sin(angle) * radius
                x to z
            }
        )
        addAll(
            ((trunkSegments - 1).downTo(1)).map { i ->
                val angle = PI - i * trunkSegmentAngle
                val x = trunkCenterOffset + cos(angle) * trunkCircleRadius
                val z = sin(angle) * trunkCircleRadius
                x to z
            }
        )
    }
        .let { top ->
            val bottom = top.reversed().map { (x, z) ->
                x to (-z)
            }
            buildList {
                add(radius to 0.0)
                addAll(top)
                add(minTrunkRadius to 0.0)
                addAll(bottom)
            }
        }

    val equatorLength = radius * PI * 2
    val segments = ceil(equatorLength / maxEdgeLength).toInt()
    val segmentAngle = PI * 2 / segments

    val sections = HashMap<Int, List<Point>>()

    fun getSection(
        index: Int,
    ): List<Point> {
        val indexNormalized = index % segments
        return sections.getOrPut(
            key = indexNormalized,
        ) {
            val angleZ = indexNormalized * segmentAngle
            val sinAngleZ = sin(angleZ)
            val cosAngleZ = cos(angleZ)
            sectionXZs.map { (x, z) ->
                Point(
                    Dimension(x * cosAngleZ),
                    Dimension(x * sinAngleZ),
                    Dimension(z),
                )
            }
        }
    }

    return (0 until segments).flatMap { i ->
        val thisSection = getSection(i)
        val nextSection = getSection(i + 1)

        fun List<Point>.getCircled(i: Int) =
            get(i % size)

        thisSection.indices.flatMap { j ->
            listOf(
                Triangle(
                    nextSection.getCircled(j + 1),
                    thisSection.getCircled(j + 1),
                    thisSection.getCircled(j),
                ),
                Triangle(
                    nextSection.getCircled(j),
                    nextSection.getCircled(j + 1),
                    thisSection.getCircled(j),
                )
            )
        }
    }
}