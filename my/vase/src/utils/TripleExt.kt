package utils

inline fun <I, O> Triple<I, I, I>.map(
    transform: (I) -> O,
): Triple<O, O, O> = Triple(
    first = transform(first),
    second = transform(second),
    third = transform(third),
)