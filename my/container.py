#!/usr/bin/env python3
# coding: utf-8

from screw import *
from grip import *
from mechanism import *

tech = 0.01
wall = 2.4
friction = 0.2

content_size = 50
content_height = 50
content_fillet = content_size * 0.1
screw_height = wall * 3

hole_tube_angle = pi / 8

scew_style = ScrewStyle()
grip_style = GripStyle(step_length=4.8)

hole_tube_additional_angle = (pi / 2 - hole_tube_angle) / 2
total_size = content_size + (friction + wall) * 2
hole_radius = total_size / 2 - grip_style.depth - wall - scew_style.depth - friction - wall
hole_tube_height = (total_size / 2 * sqrt(2) - hole_radius) * tan(hole_tube_angle)
neck_length = tan(hole_tube_additional_angle) * (friction + wall + tech)
content_wrapper_max_y = content_height + hole_tube_height + neck_length


def base():
    def body(offset: float = 0):
        z_offset = tan(hole_tube_additional_angle) * offset

        size_div2 = content_size / 2 + offset
        square_projection = rounded_polysegment(
            [
                (-size_div2, -size_div2),
                (size_div2, -size_div2),
                (size_div2, size_div2),
                (-size_div2, size_div2)
            ],
            content_fillet + offset,
            closed=True
        )
        circle_projection = circle(hole_radius + offset, wire=True)
        result = loft([
            square_projection.down(offset),
            square_projection.up(content_height + z_offset),
            circle_projection.up(content_height + z_offset + hole_tube_height),
            circle_projection.up(content_height + hole_tube_height + neck_length)
        ])
        return result

    result = body(friction + wall)

    screw_radius = hole_radius + friction + wall
    screw = scew_style.create_shape(screw_radius, screw_height)
    screw_max_radius = screw_radius + scew_style.depth
    screw_cone = cone(screw_max_radius, 0, screw_max_radius)
    screw_cone = screw_cone.mirrorXY()
    screw += screw_cone
    screw = screw.up(content_wrapper_max_y)
    result += screw

    sbtn = body(friction)
    sbtn_size_div2 = content_size / 2 + friction
    sbtn = sbtn.fillet(content_fillet + friction, [
        (0, -sbtn_size_div2, -friction)
    ])
    #sbtn = sbtn.fillet(neck_length - tech, [(0, -hole_radius-friction, content_wrapper_max_y - neck_length)])
    sbtn += cylinder(hole_radius + friction, screw_height).up(content_wrapper_max_y)
    result -= sbtn

    return result


def cap():
    screw_radius = hole_radius + friction + wall + friction
    radius = screw_radius + scew_style.depth + wall + grip_style.depth
    result = grip_style.create_shape(radius, screw_height + wall)
    result -= scew_style.create_shape(screw_radius, screw_height)
    screw_cone_radius = screw_radius + scew_style.depth
    sbtn = cone(screw_cone_radius, 0, screw_cone_radius)
    sbtn ^= halfspace().up(screw_height - tech)
    result -= sbtn
    result = result.up(content_wrapper_max_y)
    return result


def content():
    result = box(content_size, content_size, content_height)
    result = result.fillet(content_fillet)
    result = result.translate(-content_size / 2, -content_size / 2)
    return result


mechanism(
    use_mask=True,
    export_to_stl=True,
    objects=[
        MechanismObject(
            model=content(),
            color=color(1, 1, 1)
        ),
        MechanismObject(
            model=base(),
            color=color(1, 0, 1),
            to_stl_name="base"
        ),
        MechanismObject(
            model=cap(),
            color=color(1, 1, 0),
            to_stl_name="cap"
        )
    ]
)
