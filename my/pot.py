#!/usr/bin/env python3
# coding: utf-8

from mechanism import *

scale = 100

wall = 2.0 / scale
tech = 0.01 / scale

base_radius_x = 100 / scale
base_radius_y = 90 / scale
base_radius_z = 110 / scale
base_offset_z = 30 / scale

spout_offset_x = base_radius_x / 2
spout_offset_z = -base_radius_z/4
spout_angle = pi / 4
spout_start_radius = base_radius_y / 2
spout_end_radius = base_radius_y / 8
spout_length = base_radius_x * 2

cap_depth = base_radius_z / 4
cap_radius = base_radius_x / 2

handle_clearance = 30 / scale
handle_radius = 13 / scale


def pot():

    center_z = base_radius_z - base_offset_z

    def pot_proto(
            offset: float = 0
    ):
        
        radius_x = base_radius_x + offset
        radius_y = base_radius_y + offset
        radius_z = base_radius_z + offset

        def base():
            result = sphere(radius_x)
            result = result.scaleXYZ(1, radius_y / radius_x, radius_z / radius_x)
            result = result.up(center_z)
            return result

        result = base()

        def spout():
            result = cone(spout_start_radius+offset, spout_end_radius+offset, spout_length)
            result = result.rotateY(spout_angle)
            result = result.translate(spout_offset_x, 0, center_z+spout_offset_z)
            return result

        result += spout()

        fillet_point = near_vertex(result, point3(radius_x, 0, center_z))
        result = result.fillet(radius_x/2, [fillet_point])

        return result

    result = pot_proto()

    def handle():
        max_z = center_z + base_radius_z - cap_depth + tech
        radius_offset = handle_clearance + handle_radius
        radius_x = base_radius_y + radius_offset
        radius_z = base_radius_z + radius_offset
        line = box(radius_x + handle_radius, handle_radius*2, handle_radius*2)
        line = line.translate(-radius_x - handle_radius, -handle_radius)
        result = line.down(tech) + line.up(max_z-handle_radius*2)

        lines_istn = cylinder(radius_x, (handle_radius+tech)*2, center=True)
        lines_istn = lines_istn.rotateX(pi/2)
        lines_istn = lines_istn.scaleZ(radius_z/radius_x)
        lines_istn = lines_istn.up(center_z)

        result ^= lines_istn

        handle = torus(radius_x, handle_radius)
        handle = handle.rotateX(pi/2)
        handle = handle.scaleZ(radius_z/radius_x)
        handle = handle.up(center_z)
        handle_istn = box(radius_x+handle_radius+tech, (handle_radius+tech)*2, max_z+tech)
        handle_istn = handle_istn.translate(-radius_x-handle_radius-tech, -handle_radius-tech, -tech)
        handle ^= handle_istn
        result += handle

        return result

    result += handle()

    def cap_sbtn():
        result = box(base_radius_x+tech, (base_radius_y+tech)*2, cap_depth + tech)
        result = result.translate(-base_radius_x-tech, -base_radius_y-tech)
        rounding = cylinder(cap_radius, (base_radius_y+tech)*2, center=True)
        rounding = rounding.rotateX(pi/2)
        rounding = rounding.up(cap_radius)
        result += rounding
        result = result.up(center_z + base_radius_z - cap_depth)
        return result

    result -= cap_sbtn()
    result -= halfspace()

    inner_sbtn = pot_proto(-wall)
    inner_sbtn -= halfspace().up(wall)
    result -= inner_sbtn

    return result


mechanism(
    use_mask=False,
    export_to_stl=False,
    objects=[
        MechanismObject(
            model=pot(),
            color=color(1, 1, 1),
            to_stl_name="pot"
        )
    ]
)
