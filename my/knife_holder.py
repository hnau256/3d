#!/usr/bin/env python3
# coding: utf-8

from zencad import *
from mechanism import *

tech = 0.0001
friction = 0.5
wall = 6.4

blade_width = 2
blade_length = 50
blade_height = 210
blades_count = 5
blade_fillet = 2.0
blades_separation = 20
blades_padding = 6.4
base_width_expansion = 16
base_length_expansion = 24
top_edges_fillet_radius = 16
tank_fillet_radius = 16

height = wall + friction + blade_height + friction + wall
blade_hole_width = friction + blade_width + friction
blade_hole_length = friction + blade_length + blade_width + friction
blades_width = blades_padding * 2 + (blades_count - 1) * blades_separation + blade_hole_width + top_edges_fillet_radius * 2
blades_length = (blades_padding+wall) * 2 + blade_hole_length
base_width = blades_width + base_width_expansion * 2
base_length = blades_length + base_length_expansion * 2


def holder():

    def base(compression: float = 0):
        def base_x(compression: float):
            bx = base_width / 2 - compression
            tx = blades_width / 2 - compression
            z_min = compression
            z_max = height - compression
            result = polysegment(
                pnts=[
                    (-bx, z_min),
                    (-tx, z_max),
                    (tx, z_max),
                    (bx, z_min)
                ],
                closed=True
            )
            result = result.fill()
            result = result.fillet2d(
                top_edges_fillet_radius-compression,
                [(-tx, height), (tx, height)]
            )
            result = result.extrude(base_length + tech * 2)
            result = result.down(base_length / 2 + tech)
            result = result.rotateX(pi / 2)
            return result

        def base_y():
            by = base_length / 2
            ty = blades_length / 2
            result = polysegment(
                pnts=[
                    (0, -by),
                    (height, -ty),
                    (height, ty),
                    (0, by)
                ],
                closed=True
            )
            result = result.fill()
            result = result.extrude(base_width + tech * 2)
            result = result.down(base_width / 2 + tech)
            result = result.rotateY(-pi / 2)
            return result

        result = base_x(compression) ^ base_y()
        return result

    result = base()
    base_sbtn = base(wall)
    base_sbtn = base_sbtn.chamfer(
        tank_fillet_radius,
        [
            (0, base_length/2, wall),
            (0, -base_length/2, wall)
        ]
    )
    base_sbtn = base_sbtn.fillet(
        tank_fillet_radius,
        [
            (base_width/2-wall, 0, wall),
            (-base_width/2+wall, 0, wall)
        ]
    )
    result -= base_sbtn

    first_blade_x = -(blades_count-1)*blades_separation/2

    def blades_sbtn():
        sbtn_height = wall+top_edges_fillet_radius
        length = friction + blade_length + friction
        result = box(blade_hole_width, length, sbtn_height)
        result = result.translate(-blade_hole_width/2, -length/2)
        side_cylinder = cylinder(blade_hole_width/2, sbtn_height)
        side_cylinder = side_cylinder.back(length/2)
        result += side_cylinder
        result += side_cylinder.rotateZ(pi)
        result = union([result.right(i*blades_separation) for i in range(blades_count)])
        result = result.right(first_blade_x)
        result = result.up(height-sbtn_height)
        return result

    result -= blades_sbtn()
    result = result.fillet(
        blade_fillet,
        [point3(first_blade_x+i*blades_separation, 0, height) for i in range(blades_count)]
    )
    result = result.fillet(
        blade_fillet,
        [point3(first_blade_x+i*blades_separation, 0, height-wall) for i in range(blades_count)]
    )

    return result


mechanism(
    use_mask=False,
    objects=[
        MechanismObject(
            model=holder(),
            to_stl_name="knife_holder"
        )
    ]
)
